import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MfsboxTestModule } from '../../../test.module';
import { RemittanceTransactionStatusDetailComponent } from 'app/entities/remittance-transaction-status/remittance-transaction-status-detail.component';
import { RemittanceTransactionStatus } from 'app/shared/model/remittance-transaction-status.model';

describe('Component Tests', () => {
  describe('RemittanceTransactionStatus Management Detail Component', () => {
    let comp: RemittanceTransactionStatusDetailComponent;
    let fixture: ComponentFixture<RemittanceTransactionStatusDetailComponent>;
    const route = ({ data: of({ remittanceTransactionStatus: new RemittanceTransactionStatus(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MfsboxTestModule],
        declarations: [RemittanceTransactionStatusDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(RemittanceTransactionStatusDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(RemittanceTransactionStatusDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load remittanceTransactionStatus on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.remittanceTransactionStatus).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
