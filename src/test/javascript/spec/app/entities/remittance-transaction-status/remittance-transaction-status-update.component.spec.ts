import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MfsboxTestModule } from '../../../test.module';
import { RemittanceTransactionStatusUpdateComponent } from 'app/entities/remittance-transaction-status/remittance-transaction-status-update.component';
import { RemittanceTransactionStatusService } from 'app/entities/remittance-transaction-status/remittance-transaction-status.service';
import { RemittanceTransactionStatus } from 'app/shared/model/remittance-transaction-status.model';

describe('Component Tests', () => {
  describe('RemittanceTransactionStatus Management Update Component', () => {
    let comp: RemittanceTransactionStatusUpdateComponent;
    let fixture: ComponentFixture<RemittanceTransactionStatusUpdateComponent>;
    let service: RemittanceTransactionStatusService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MfsboxTestModule],
        declarations: [RemittanceTransactionStatusUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(RemittanceTransactionStatusUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(RemittanceTransactionStatusUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(RemittanceTransactionStatusService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new RemittanceTransactionStatus(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new RemittanceTransactionStatus();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
