import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { RemittanceTransactionStatusService } from 'app/entities/remittance-transaction-status/remittance-transaction-status.service';
import { IRemittanceTransactionStatus, RemittanceTransactionStatus } from 'app/shared/model/remittance-transaction-status.model';

describe('Service Tests', () => {
  describe('RemittanceTransactionStatus Service', () => {
    let injector: TestBed;
    let service: RemittanceTransactionStatusService;
    let httpMock: HttpTestingController;
    let elemDefault: IRemittanceTransactionStatus;
    let expectedResult: IRemittanceTransactionStatus | IRemittanceTransactionStatus[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(RemittanceTransactionStatusService);
      httpMock = injector.get(HttpTestingController);

      elemDefault = new RemittanceTransactionStatus(0, 0, 'AAAAAAA', 'AAAAAAA', 'AAAAAAA');
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a RemittanceTransactionStatus', () => {
        const returnedFromService = Object.assign(
          {
            id: 0
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new RemittanceTransactionStatus()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a RemittanceTransactionStatus', () => {
        const returnedFromService = Object.assign(
          {
            domaineId: 1,
            type: 'BBBBBB',
            label: 'BBBBBB',
            description: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of RemittanceTransactionStatus', () => {
        const returnedFromService = Object.assign(
          {
            domaineId: 1,
            type: 'BBBBBB',
            label: 'BBBBBB',
            description: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a RemittanceTransactionStatus', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
