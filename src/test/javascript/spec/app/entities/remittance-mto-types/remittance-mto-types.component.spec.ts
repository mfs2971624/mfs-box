import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Data } from '@angular/router';

import { MfsboxTestModule } from '../../../test.module';
import { RemittanceMtoTypesComponent } from 'app/entities/remittance-mto-types/remittance-mto-types.component';
import { RemittanceMtoTypesService } from 'app/entities/remittance-mto-types/remittance-mto-types.service';
import { RemittanceMtoTypes } from 'app/shared/model/remittance-mto-types.model';

describe('Component Tests', () => {
  describe('RemittanceMtoTypes Management Component', () => {
    let comp: RemittanceMtoTypesComponent;
    let fixture: ComponentFixture<RemittanceMtoTypesComponent>;
    let service: RemittanceMtoTypesService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MfsboxTestModule],
        declarations: [RemittanceMtoTypesComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: {
              data: {
                subscribe: (fn: (value: Data) => void) =>
                  fn({
                    pagingParams: {
                      predicate: 'id',
                      reverse: false,
                      page: 0
                    }
                  })
              }
            }
          }
        ]
      })
        .overrideTemplate(RemittanceMtoTypesComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(RemittanceMtoTypesComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(RemittanceMtoTypesService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new RemittanceMtoTypes(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.remittanceMtoTypes && comp.remittanceMtoTypes[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });

    it('should load a page', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new RemittanceMtoTypes(123)],
            headers
          })
        )
      );

      // WHEN
      comp.loadPage(1);

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.remittanceMtoTypes && comp.remittanceMtoTypes[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });

    it('should calculate the sort attribute for an id', () => {
      // WHEN
      comp.ngOnInit();
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['id,desc']);
    });

    it('should calculate the sort attribute for a non-id attribute', () => {
      // INIT
      comp.ngOnInit();

      // GIVEN
      comp.predicate = 'name';

      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['name,desc', 'id']);
    });
  });
});
