import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MfsboxTestModule } from '../../../test.module';
import { RemittanceMtoTypesUpdateComponent } from 'app/entities/remittance-mto-types/remittance-mto-types-update.component';
import { RemittanceMtoTypesService } from 'app/entities/remittance-mto-types/remittance-mto-types.service';
import { RemittanceMtoTypes } from 'app/shared/model/remittance-mto-types.model';

describe('Component Tests', () => {
  describe('RemittanceMtoTypes Management Update Component', () => {
    let comp: RemittanceMtoTypesUpdateComponent;
    let fixture: ComponentFixture<RemittanceMtoTypesUpdateComponent>;
    let service: RemittanceMtoTypesService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MfsboxTestModule],
        declarations: [RemittanceMtoTypesUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(RemittanceMtoTypesUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(RemittanceMtoTypesUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(RemittanceMtoTypesService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new RemittanceMtoTypes(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new RemittanceMtoTypes();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
