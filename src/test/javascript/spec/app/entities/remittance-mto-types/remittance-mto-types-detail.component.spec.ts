import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MfsboxTestModule } from '../../../test.module';
import { RemittanceMtoTypesDetailComponent } from 'app/entities/remittance-mto-types/remittance-mto-types-detail.component';
import { RemittanceMtoTypes } from 'app/shared/model/remittance-mto-types.model';

describe('Component Tests', () => {
  describe('RemittanceMtoTypes Management Detail Component', () => {
    let comp: RemittanceMtoTypesDetailComponent;
    let fixture: ComponentFixture<RemittanceMtoTypesDetailComponent>;
    const route = ({ data: of({ remittanceMtoTypes: new RemittanceMtoTypes(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MfsboxTestModule],
        declarations: [RemittanceMtoTypesDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(RemittanceMtoTypesDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(RemittanceMtoTypesDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load remittanceMtoTypes on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.remittanceMtoTypes).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
