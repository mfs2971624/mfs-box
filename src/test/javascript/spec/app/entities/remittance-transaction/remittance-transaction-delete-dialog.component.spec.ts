import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { MfsboxTestModule } from '../../../test.module';
import { MockEventManager } from '../../../helpers/mock-event-manager.service';
import { MockActiveModal } from '../../../helpers/mock-active-modal.service';
import { RemittanceTransactionDeleteDialogComponent } from 'app/entities/remittance-transaction/remittance-transaction-delete-dialog.component';
import { RemittanceTransactionService } from 'app/entities/remittance-transaction/remittance-transaction.service';

describe('Component Tests', () => {
  describe('RemittanceTransaction Management Delete Component', () => {
    let comp: RemittanceTransactionDeleteDialogComponent;
    let fixture: ComponentFixture<RemittanceTransactionDeleteDialogComponent>;
    let service: RemittanceTransactionService;
    let mockEventManager: MockEventManager;
    let mockActiveModal: MockActiveModal;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MfsboxTestModule],
        declarations: [RemittanceTransactionDeleteDialogComponent]
      })
        .overrideTemplate(RemittanceTransactionDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(RemittanceTransactionDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(RemittanceTransactionService);
      mockEventManager = TestBed.get(JhiEventManager);
      mockActiveModal = TestBed.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          // comp.confirmCancel(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.closeSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));

      it('Should not call delete service on clear', () => {
        // GIVEN
        spyOn(service, 'delete');

        // WHEN
        comp.cancel();

        // THEN
        expect(service.delete).not.toHaveBeenCalled();
        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
      });
    });
  });
});
