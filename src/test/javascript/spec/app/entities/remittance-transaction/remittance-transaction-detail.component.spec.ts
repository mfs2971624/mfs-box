import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MfsboxTestModule } from '../../../test.module';
import { RemittanceTransactionDetailComponent } from 'app/entities/remittance-transaction/remittance-transaction-detail.component';
import { RemittanceTransaction } from 'app/shared/model/remittance-transaction.model';

describe('Component Tests', () => {
  describe('RemittanceTransaction Management Detail Component', () => {
    let comp: RemittanceTransactionDetailComponent;
    let fixture: ComponentFixture<RemittanceTransactionDetailComponent>;
    const route = ({ data: of({ remittanceTransaction: new RemittanceTransaction(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MfsboxTestModule],
        declarations: [RemittanceTransactionDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(RemittanceTransactionDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(RemittanceTransactionDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load remittanceTransaction on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.remittanceTransaction).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
