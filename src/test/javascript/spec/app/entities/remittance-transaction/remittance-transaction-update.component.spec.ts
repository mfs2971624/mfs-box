import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MfsboxTestModule } from '../../../test.module';
import { RemittanceTransactionUpdateComponent } from 'app/entities/remittance-transaction/remittance-transaction-update.component';
import { RemittanceTransactionService } from 'app/entities/remittance-transaction/remittance-transaction.service';
import { RemittanceTransaction } from 'app/shared/model/remittance-transaction.model';

describe('Component Tests', () => {
  describe('RemittanceTransaction Management Update Component', () => {
    let comp: RemittanceTransactionUpdateComponent;
    let fixture: ComponentFixture<RemittanceTransactionUpdateComponent>;
    let service: RemittanceTransactionService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MfsboxTestModule],
        declarations: [RemittanceTransactionUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(RemittanceTransactionUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(RemittanceTransactionUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(RemittanceTransactionService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new RemittanceTransaction(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new RemittanceTransaction();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
