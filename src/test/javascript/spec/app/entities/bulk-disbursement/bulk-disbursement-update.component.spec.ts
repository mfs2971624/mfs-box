import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MfsboxTestModule } from '../../../test.module';
import { BulkDisbursementUpdateComponent } from 'app/entities/bulk-disbursement/bulk-disbursement-update.component';
import { BulkDisbursementService } from 'app/entities/bulk-disbursement/bulk-disbursement.service';
import { BulkDisbursement } from 'app/shared/model/bulk-disbursement.model';

describe('Component Tests', () => {
  describe('BulkDisbursement Management Update Component', () => {
    let comp: BulkDisbursementUpdateComponent;
    let fixture: ComponentFixture<BulkDisbursementUpdateComponent>;
    let service: BulkDisbursementService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MfsboxTestModule],
        declarations: [BulkDisbursementUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(BulkDisbursementUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(BulkDisbursementUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BulkDisbursementService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new BulkDisbursement(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new BulkDisbursement();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
