import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Data } from '@angular/router';

import { MfsboxTestModule } from '../../../test.module';
import { BulkDisbursementComponent } from 'app/entities/bulk-disbursement/bulk-disbursement.component';
import { BulkDisbursementService } from 'app/entities/bulk-disbursement/bulk-disbursement.service';
import { BulkDisbursement } from 'app/shared/model/bulk-disbursement.model';

describe('Component Tests', () => {
  describe('BulkDisbursement Management Component', () => {
    let comp: BulkDisbursementComponent;
    let fixture: ComponentFixture<BulkDisbursementComponent>;
    let service: BulkDisbursementService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MfsboxTestModule],
        declarations: [BulkDisbursementComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: {
              data: {
                subscribe: (fn: (value: Data) => void) =>
                  fn({
                    pagingParams: {
                      predicate: 'id',
                      reverse: false,
                      page: 0
                    }
                  })
              }
            }
          }
        ]
      })
        .overrideTemplate(BulkDisbursementComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(BulkDisbursementComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BulkDisbursementService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new BulkDisbursement(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.bulkDisbursements && comp.bulkDisbursements[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });

    it('should load a page', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new BulkDisbursement(123)],
            headers
          })
        )
      );

      // WHEN
      comp.loadPage(1);

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.bulkDisbursements && comp.bulkDisbursements[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });

    it('should calculate the sort attribute for an id', () => {
      // WHEN
      comp.ngOnInit();
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['id,desc']);
    });

    it('should calculate the sort attribute for a non-id attribute', () => {
      // INIT
      comp.ngOnInit();

      // GIVEN
      comp.predicate = 'name';

      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['name,desc', 'id']);
    });
  });
});
