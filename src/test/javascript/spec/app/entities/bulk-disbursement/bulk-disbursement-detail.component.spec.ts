import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MfsboxTestModule } from '../../../test.module';
import { BulkDisbursementDetailComponent } from 'app/entities/bulk-disbursement/bulk-disbursement-detail.component';
import { BulkDisbursement } from 'app/shared/model/bulk-disbursement.model';

describe('Component Tests', () => {
  describe('BulkDisbursement Management Detail Component', () => {
    let comp: BulkDisbursementDetailComponent;
    let fixture: ComponentFixture<BulkDisbursementDetailComponent>;
    const route = ({ data: of({ bulkDisbursement: new BulkDisbursement(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MfsboxTestModule],
        declarations: [BulkDisbursementDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(BulkDisbursementDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(BulkDisbursementDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load bulkDisbursement on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.bulkDisbursement).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
