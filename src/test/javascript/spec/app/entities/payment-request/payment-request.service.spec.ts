import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { PaymentRequestService } from 'app/entities/payment-request/payment-request.service';
import { IPaymentRequest, PaymentRequest } from 'app/shared/model/payment-request.model';
import { PaymentState } from 'app/shared/model/enumerations/payment-state.model';

describe('Service Tests', () => {
  describe('PaymentRequest Service', () => {
    let injector: TestBed;
    let service: PaymentRequestService;
    let httpMock: HttpTestingController;
    let elemDefault: IPaymentRequest;
    let expectedResult: IPaymentRequest | IPaymentRequest[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(PaymentRequestService);
      httpMock = injector.get(HttpTestingController);

      elemDefault = new PaymentRequest(
        0,
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        PaymentState.PENDING,
        'AAAAAAA',
        'AAAAAAA'
      );
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a PaymentRequest', () => {
        const returnedFromService = Object.assign(
          {
            id: 0
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new PaymentRequest()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a PaymentRequest', () => {
        const returnedFromService = Object.assign(
          {
            msisdn: 'BBBBBB',
            amount: 'BBBBBB',
            identificationNo: 'BBBBBB',
            firstName: 'BBBBBB',
            lastName: 'BBBBBB',
            comment: 'BBBBBB',
            paymentState: 'BBBBBB',
            paymentTxId: 'BBBBBB',
            paymentMessage: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of PaymentRequest', () => {
        const returnedFromService = Object.assign(
          {
            msisdn: 'BBBBBB',
            amount: 'BBBBBB',
            identificationNo: 'BBBBBB',
            firstName: 'BBBBBB',
            lastName: 'BBBBBB',
            comment: 'BBBBBB',
            paymentState: 'BBBBBB',
            paymentTxId: 'BBBBBB',
            paymentMessage: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a PaymentRequest', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
