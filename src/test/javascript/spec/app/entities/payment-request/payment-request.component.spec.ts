import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Data } from '@angular/router';

import { MfsboxTestModule } from '../../../test.module';
import { PaymentRequestComponent } from 'app/entities/payment-request/payment-request.component';
import { PaymentRequestService } from 'app/entities/payment-request/payment-request.service';
import { PaymentRequest } from 'app/shared/model/payment-request.model';

describe('Component Tests', () => {
  describe('PaymentRequest Management Component', () => {
    let comp: PaymentRequestComponent;
    let fixture: ComponentFixture<PaymentRequestComponent>;
    let service: PaymentRequestService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MfsboxTestModule],
        declarations: [PaymentRequestComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: {
              data: {
                subscribe: (fn: (value: Data) => void) =>
                  fn({
                    pagingParams: {
                      predicate: 'id',
                      reverse: false,
                      page: 0
                    }
                  })
              }
            }
          }
        ]
      })
        .overrideTemplate(PaymentRequestComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(PaymentRequestComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(PaymentRequestService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new PaymentRequest(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.paymentRequests && comp.paymentRequests[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });

    it('should load a page', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new PaymentRequest(123)],
            headers
          })
        )
      );

      // WHEN
      comp.loadPage(1);

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.paymentRequests && comp.paymentRequests[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });

    it('should calculate the sort attribute for an id', () => {
      // WHEN
      comp.ngOnInit();
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['id,desc']);
    });

    it('should calculate the sort attribute for a non-id attribute', () => {
      // INIT
      comp.ngOnInit();

      // GIVEN
      comp.predicate = 'name';

      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['name,desc', 'id']);
    });
  });
});
