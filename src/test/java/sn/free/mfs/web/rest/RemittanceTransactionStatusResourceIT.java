package sn.free.mfs.web.rest;

import sn.free.mfs.MfsboxApp;
import sn.free.mfs.domain.RemittanceTransactionStatus;
import sn.free.mfs.repository.RemittanceTransactionStatusRepository;
import sn.free.mfs.service.RemittanceTransactionStatusService;
import sn.free.mfs.service.dto.RemittanceTransactionStatusDTO;
import sn.free.mfs.service.mapper.RemittanceTransactionStatusMapper;
import sn.free.mfs.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static sn.free.mfs.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link RemittanceTransactionStatusResource} REST controller.
 */
@SpringBootTest(classes = MfsboxApp.class)
public class RemittanceTransactionStatusResourceIT {

    private static final Integer DEFAULT_DOMAINE_ID = 1;
    private static final Integer UPDATED_DOMAINE_ID = 2;

    private static final String DEFAULT_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_LABEL = "AAAAAAAAAA";
    private static final String UPDATED_LABEL = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private RemittanceTransactionStatusRepository remittanceTransactionStatusRepository;

    @Autowired
    private RemittanceTransactionStatusMapper remittanceTransactionStatusMapper;

    @Autowired
    private RemittanceTransactionStatusService remittanceTransactionStatusService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restRemittanceTransactionStatusMockMvc;

    private RemittanceTransactionStatus remittanceTransactionStatus;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RemittanceTransactionStatusResource remittanceTransactionStatusResource = new RemittanceTransactionStatusResource(remittanceTransactionStatusService);
        this.restRemittanceTransactionStatusMockMvc = MockMvcBuilders.standaloneSetup(remittanceTransactionStatusResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RemittanceTransactionStatus createEntity(EntityManager em) {
        RemittanceTransactionStatus remittanceTransactionStatus = new RemittanceTransactionStatus()
            .domaineId(DEFAULT_DOMAINE_ID)
            .type(DEFAULT_TYPE)
            .label(DEFAULT_LABEL)
            .description(DEFAULT_DESCRIPTION);
        return remittanceTransactionStatus;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RemittanceTransactionStatus createUpdatedEntity(EntityManager em) {
        RemittanceTransactionStatus remittanceTransactionStatus = new RemittanceTransactionStatus()
            .domaineId(UPDATED_DOMAINE_ID)
            .type(UPDATED_TYPE)
            .label(UPDATED_LABEL)
            .description(UPDATED_DESCRIPTION);
        return remittanceTransactionStatus;
    }

    @BeforeEach
    public void initTest() {
        remittanceTransactionStatus = createEntity(em);
    }

    @Test
    @Transactional
    public void createRemittanceTransactionStatus() throws Exception {
        int databaseSizeBeforeCreate = remittanceTransactionStatusRepository.findAll().size();

        // Create the RemittanceTransactionStatus
        RemittanceTransactionStatusDTO remittanceTransactionStatusDTO = remittanceTransactionStatusMapper.toDto(remittanceTransactionStatus);
        restRemittanceTransactionStatusMockMvc.perform(post("/api/remittance-transaction-statuses")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(remittanceTransactionStatusDTO)))
            .andExpect(status().isCreated());

        // Validate the RemittanceTransactionStatus in the database
        List<RemittanceTransactionStatus> remittanceTransactionStatusList = remittanceTransactionStatusRepository.findAll();
        assertThat(remittanceTransactionStatusList).hasSize(databaseSizeBeforeCreate + 1);
        RemittanceTransactionStatus testRemittanceTransactionStatus = remittanceTransactionStatusList.get(remittanceTransactionStatusList.size() - 1);
        assertThat(testRemittanceTransactionStatus.getDomaineId()).isEqualTo(DEFAULT_DOMAINE_ID);
        assertThat(testRemittanceTransactionStatus.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(testRemittanceTransactionStatus.getLabel()).isEqualTo(DEFAULT_LABEL);
        assertThat(testRemittanceTransactionStatus.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createRemittanceTransactionStatusWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = remittanceTransactionStatusRepository.findAll().size();

        // Create the RemittanceTransactionStatus with an existing ID
        remittanceTransactionStatus.setId(1L);
        RemittanceTransactionStatusDTO remittanceTransactionStatusDTO = remittanceTransactionStatusMapper.toDto(remittanceTransactionStatus);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRemittanceTransactionStatusMockMvc.perform(post("/api/remittance-transaction-statuses")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(remittanceTransactionStatusDTO)))
            .andExpect(status().isBadRequest());

        // Validate the RemittanceTransactionStatus in the database
        List<RemittanceTransactionStatus> remittanceTransactionStatusList = remittanceTransactionStatusRepository.findAll();
        assertThat(remittanceTransactionStatusList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkDomaineIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = remittanceTransactionStatusRepository.findAll().size();
        // set the field null
        remittanceTransactionStatus.setDomaineId(null);

        // Create the RemittanceTransactionStatus, which fails.
        RemittanceTransactionStatusDTO remittanceTransactionStatusDTO = remittanceTransactionStatusMapper.toDto(remittanceTransactionStatus);

        restRemittanceTransactionStatusMockMvc.perform(post("/api/remittance-transaction-statuses")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(remittanceTransactionStatusDTO)))
            .andExpect(status().isBadRequest());

        List<RemittanceTransactionStatus> remittanceTransactionStatusList = remittanceTransactionStatusRepository.findAll();
        assertThat(remittanceTransactionStatusList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = remittanceTransactionStatusRepository.findAll().size();
        // set the field null
        remittanceTransactionStatus.setType(null);

        // Create the RemittanceTransactionStatus, which fails.
        RemittanceTransactionStatusDTO remittanceTransactionStatusDTO = remittanceTransactionStatusMapper.toDto(remittanceTransactionStatus);

        restRemittanceTransactionStatusMockMvc.perform(post("/api/remittance-transaction-statuses")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(remittanceTransactionStatusDTO)))
            .andExpect(status().isBadRequest());

        List<RemittanceTransactionStatus> remittanceTransactionStatusList = remittanceTransactionStatusRepository.findAll();
        assertThat(remittanceTransactionStatusList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLabelIsRequired() throws Exception {
        int databaseSizeBeforeTest = remittanceTransactionStatusRepository.findAll().size();
        // set the field null
        remittanceTransactionStatus.setLabel(null);

        // Create the RemittanceTransactionStatus, which fails.
        RemittanceTransactionStatusDTO remittanceTransactionStatusDTO = remittanceTransactionStatusMapper.toDto(remittanceTransactionStatus);

        restRemittanceTransactionStatusMockMvc.perform(post("/api/remittance-transaction-statuses")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(remittanceTransactionStatusDTO)))
            .andExpect(status().isBadRequest());

        List<RemittanceTransactionStatus> remittanceTransactionStatusList = remittanceTransactionStatusRepository.findAll();
        assertThat(remittanceTransactionStatusList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllRemittanceTransactionStatuses() throws Exception {
        // Initialize the database
        remittanceTransactionStatusRepository.saveAndFlush(remittanceTransactionStatus);

        // Get all the remittanceTransactionStatusList
        restRemittanceTransactionStatusMockMvc.perform(get("/api/remittance-transaction-statuses?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(remittanceTransactionStatus.getId().intValue())))
            .andExpect(jsonPath("$.[*].domaineId").value(hasItem(DEFAULT_DOMAINE_ID)))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE)))
            .andExpect(jsonPath("$.[*].label").value(hasItem(DEFAULT_LABEL)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)));
    }
    
    @Test
    @Transactional
    public void getRemittanceTransactionStatus() throws Exception {
        // Initialize the database
        remittanceTransactionStatusRepository.saveAndFlush(remittanceTransactionStatus);

        // Get the remittanceTransactionStatus
        restRemittanceTransactionStatusMockMvc.perform(get("/api/remittance-transaction-statuses/{id}", remittanceTransactionStatus.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(remittanceTransactionStatus.getId().intValue()))
            .andExpect(jsonPath("$.domaineId").value(DEFAULT_DOMAINE_ID))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE))
            .andExpect(jsonPath("$.label").value(DEFAULT_LABEL))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION));
    }

    @Test
    @Transactional
    public void getNonExistingRemittanceTransactionStatus() throws Exception {
        // Get the remittanceTransactionStatus
        restRemittanceTransactionStatusMockMvc.perform(get("/api/remittance-transaction-statuses/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRemittanceTransactionStatus() throws Exception {
        // Initialize the database
        remittanceTransactionStatusRepository.saveAndFlush(remittanceTransactionStatus);

        int databaseSizeBeforeUpdate = remittanceTransactionStatusRepository.findAll().size();

        // Update the remittanceTransactionStatus
        RemittanceTransactionStatus updatedRemittanceTransactionStatus = remittanceTransactionStatusRepository.findById(remittanceTransactionStatus.getId()).get();
        // Disconnect from session so that the updates on updatedRemittanceTransactionStatus are not directly saved in db
        em.detach(updatedRemittanceTransactionStatus);
        updatedRemittanceTransactionStatus
            .domaineId(UPDATED_DOMAINE_ID)
            .type(UPDATED_TYPE)
            .label(UPDATED_LABEL)
            .description(UPDATED_DESCRIPTION);
        RemittanceTransactionStatusDTO remittanceTransactionStatusDTO = remittanceTransactionStatusMapper.toDto(updatedRemittanceTransactionStatus);

        restRemittanceTransactionStatusMockMvc.perform(put("/api/remittance-transaction-statuses")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(remittanceTransactionStatusDTO)))
            .andExpect(status().isOk());

        // Validate the RemittanceTransactionStatus in the database
        List<RemittanceTransactionStatus> remittanceTransactionStatusList = remittanceTransactionStatusRepository.findAll();
        assertThat(remittanceTransactionStatusList).hasSize(databaseSizeBeforeUpdate);
        RemittanceTransactionStatus testRemittanceTransactionStatus = remittanceTransactionStatusList.get(remittanceTransactionStatusList.size() - 1);
        assertThat(testRemittanceTransactionStatus.getDomaineId()).isEqualTo(UPDATED_DOMAINE_ID);
        assertThat(testRemittanceTransactionStatus.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testRemittanceTransactionStatus.getLabel()).isEqualTo(UPDATED_LABEL);
        assertThat(testRemittanceTransactionStatus.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingRemittanceTransactionStatus() throws Exception {
        int databaseSizeBeforeUpdate = remittanceTransactionStatusRepository.findAll().size();

        // Create the RemittanceTransactionStatus
        RemittanceTransactionStatusDTO remittanceTransactionStatusDTO = remittanceTransactionStatusMapper.toDto(remittanceTransactionStatus);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRemittanceTransactionStatusMockMvc.perform(put("/api/remittance-transaction-statuses")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(remittanceTransactionStatusDTO)))
            .andExpect(status().isBadRequest());

        // Validate the RemittanceTransactionStatus in the database
        List<RemittanceTransactionStatus> remittanceTransactionStatusList = remittanceTransactionStatusRepository.findAll();
        assertThat(remittanceTransactionStatusList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRemittanceTransactionStatus() throws Exception {
        // Initialize the database
        remittanceTransactionStatusRepository.saveAndFlush(remittanceTransactionStatus);

        int databaseSizeBeforeDelete = remittanceTransactionStatusRepository.findAll().size();

        // Delete the remittanceTransactionStatus
        restRemittanceTransactionStatusMockMvc.perform(delete("/api/remittance-transaction-statuses/{id}", remittanceTransactionStatus.getId())
            .accept(TestUtil.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<RemittanceTransactionStatus> remittanceTransactionStatusList = remittanceTransactionStatusRepository.findAll();
        assertThat(remittanceTransactionStatusList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
