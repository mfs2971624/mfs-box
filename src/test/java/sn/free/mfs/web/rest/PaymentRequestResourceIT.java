//package sn.free.mfs.web.rest;
//
//import sn.free.mfs.MfsboxApp;
//import sn.free.mfs.domain.PaymentRequest;
//import sn.free.mfs.repository.PaymentRequestRepository;
//import sn.free.mfs.service.PaymentRequestService;
//import sn.free.mfs.service.dto.PaymentRequestDTO;
//import sn.free.mfs.service.mapper.PaymentRequestMapper;
//import sn.free.mfs.web.rest.errors.ExceptionTranslator;
//
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.mockito.MockitoAnnotations;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
//import org.springframework.http.MediaType;
//import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.validation.Validator;
//
//import javax.persistence.EntityManager;
//import java.util.List;
//
//import static sn.free.mfs.web.rest.TestUtil.createFormattingConversionService;
//import static org.assertj.core.api.Assertions.assertThat;
//import static org.hamcrest.Matchers.hasItem;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
//
//import sn.free.mfs.domain.enumeration.PaymentState;
///**
// * Integration tests for the {@link PaymentRequestResource} REST controller.
// */
//@SpringBootTest(classes = MfsboxApp.class)
//public class PaymentRequestResourceIT {
//
//    private static final String DEFAULT_MSISDN = "AAAAAAAAAA";
//    private static final String UPDATED_MSISDN = "BBBBBBBBBB";
//
//    private static final String DEFAULT_AMOUNT = "AAAAAAAAAA";
//    private static final String UPDATED_AMOUNT = "BBBBBBBBBB";
//
//    private static final String DEFAULT_IDENTIFICATION_NO = "AAAAAAAAAA";
//    private static final String UPDATED_IDENTIFICATION_NO = "BBBBBBBBBB";
//
//    private static final String DEFAULT_FIRST_NAME = "AAAAAAAAAA";
//    private static final String UPDATED_FIRST_NAME = "BBBBBBBBBB";
//
//    private static final String DEFAULT_LAST_NAME = "AAAAAAAAAA";
//    private static final String UPDATED_LAST_NAME = "BBBBBBBBBB";
//
//    private static final String DEFAULT_COMMENT = "AAAAAAAAAA";
//    private static final String UPDATED_COMMENT = "BBBBBBBBBB";
//
//    private static final PaymentState DEFAULT_PAYMENT_STATE = PaymentState.PENDING;
//    private static final PaymentState UPDATED_PAYMENT_STATE = PaymentState.SUCCESS;
//
//    private static final String DEFAULT_PAYMENT_TX_ID = "AAAAAAAAAA";
//    private static final String UPDATED_PAYMENT_TX_ID = "BBBBBBBBBB";
//
//    private static final String DEFAULT_PAYMENT_MESSAGE = "AAAAAAAAAA";
//    private static final String UPDATED_PAYMENT_MESSAGE = "BBBBBBBBBB";
//
//    @Autowired
//    private PaymentRequestRepository paymentRequestRepository;
//
//    @Autowired
//    private PaymentRequestMapper paymentRequestMapper;
//
//    @Autowired
//    private PaymentRequestService paymentRequestService;
//
//    @Autowired
//    private MappingJackson2HttpMessageConverter jacksonMessageConverter;
//
//    @Autowired
//    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;
//
//    @Autowired
//    private ExceptionTranslator exceptionTranslator;
//
//    @Autowired
//    private EntityManager em;
//
//    @Autowired
//    private Validator validator;
//
//    private MockMvc restPaymentRequestMockMvc;
//
//    private PaymentRequest paymentRequest;
//
//    @BeforeEach
//    public void setup() {
//        MockitoAnnotations.initMocks(this);
//        final PaymentRequestResource paymentRequestResource = new PaymentRequestResource(paymentRequestService);
//        this.restPaymentRequestMockMvc = MockMvcBuilders.standaloneSetup(paymentRequestResource)
//            .setCustomArgumentResolvers(pageableArgumentResolver)
//            .setControllerAdvice(exceptionTranslator)
//            .setConversionService(createFormattingConversionService())
//            .setMessageConverters(jacksonMessageConverter)
//            .setValidator(validator).build();
//    }
//
//    /**
//     * Create an entity for this test.
//     *
//     * This is a static method, as tests for other entities might also need it,
//     * if they test an entity which requires the current entity.
//     */
//    public static PaymentRequest createEntity(EntityManager em) {
//        PaymentRequest paymentRequest = new PaymentRequest()
//            .msisdn(DEFAULT_MSISDN)
//            .amount(DEFAULT_AMOUNT)
//            .identificationNo(DEFAULT_IDENTIFICATION_NO)
//            .firstName(DEFAULT_FIRST_NAME)
//            .lastName(DEFAULT_LAST_NAME)
//            .comment(DEFAULT_COMMENT)
//            .paymentState(DEFAULT_PAYMENT_STATE)
//            .paymentTxId(DEFAULT_PAYMENT_TX_ID)
//            .paymentMessage(DEFAULT_PAYMENT_MESSAGE);
//        return paymentRequest;
//    }
//    /**
//     * Create an updated entity for this test.
//     *
//     * This is a static method, as tests for other entities might also need it,
//     * if they test an entity which requires the current entity.
//     */
//    public static PaymentRequest createUpdatedEntity(EntityManager em) {
//        PaymentRequest paymentRequest = new PaymentRequest()
//            .msisdn(UPDATED_MSISDN)
//            .amount(UPDATED_AMOUNT)
//            .identificationNo(UPDATED_IDENTIFICATION_NO)
//            .firstName(UPDATED_FIRST_NAME)
//            .lastName(UPDATED_LAST_NAME)
//            .comment(UPDATED_COMMENT)
//            .paymentState(UPDATED_PAYMENT_STATE)
//            .paymentTxId(UPDATED_PAYMENT_TX_ID)
//            .paymentMessage(UPDATED_PAYMENT_MESSAGE);
//        return paymentRequest;
//    }
//
//    @BeforeEach
//    public void initTest() {
//        paymentRequest = createEntity(em);
//    }
//
//    @Test
//    @Transactional
//    public void createPaymentRequest() throws Exception {
//        int databaseSizeBeforeCreate = paymentRequestRepository.findAll().size();
//
//        // Create the PaymentRequest
//        PaymentRequestDTO paymentRequestDTO = paymentRequestMapper.toDto(paymentRequest);
//        restPaymentRequestMockMvc.perform(post("/api/payment-requests")
//            .contentType(TestUtil.APPLICATION_JSON)
//            .content(TestUtil.convertObjectToJsonBytes(paymentRequestDTO)))
//            .andExpect(status().isCreated());
//
//        // Validate the PaymentRequest in the database
//        List<PaymentRequest> paymentRequestList = paymentRequestRepository.findAll();
//        assertThat(paymentRequestList).hasSize(databaseSizeBeforeCreate + 1);
//        PaymentRequest testPaymentRequest = paymentRequestList.get(paymentRequestList.size() - 1);
//        assertThat(testPaymentRequest.getMsisdn()).isEqualTo(DEFAULT_MSISDN);
//        assertThat(testPaymentRequest.getAmount()).isEqualTo(DEFAULT_AMOUNT);
//        assertThat(testPaymentRequest.getIdentificationNo()).isEqualTo(DEFAULT_IDENTIFICATION_NO);
//        assertThat(testPaymentRequest.getFirstName()).isEqualTo(DEFAULT_FIRST_NAME);
//        assertThat(testPaymentRequest.getLastName()).isEqualTo(DEFAULT_LAST_NAME);
//        assertThat(testPaymentRequest.getComment()).isEqualTo(DEFAULT_COMMENT);
//        assertThat(testPaymentRequest.getPaymentState()).isEqualTo(DEFAULT_PAYMENT_STATE);
//        assertThat(testPaymentRequest.getPaymentTxId()).isEqualTo(DEFAULT_PAYMENT_TX_ID);
//        assertThat(testPaymentRequest.getPaymentMessage()).isEqualTo(DEFAULT_PAYMENT_MESSAGE);
//    }
//
//    @Test
//    @Transactional
//    public void createPaymentRequestWithExistingId() throws Exception {
//        int databaseSizeBeforeCreate = paymentRequestRepository.findAll().size();
//
//        // Create the PaymentRequest with an existing ID
//        paymentRequest.setId(1L);
//        PaymentRequestDTO paymentRequestDTO = paymentRequestMapper.toDto(paymentRequest);
//
//        // An entity with an existing ID cannot be created, so this API call must fail
//        restPaymentRequestMockMvc.perform(post("/api/payment-requests")
//            .contentType(TestUtil.APPLICATION_JSON)
//            .content(TestUtil.convertObjectToJsonBytes(paymentRequestDTO)))
//            .andExpect(status().isBadRequest());
//
//        // Validate the PaymentRequest in the database
//        List<PaymentRequest> paymentRequestList = paymentRequestRepository.findAll();
//        assertThat(paymentRequestList).hasSize(databaseSizeBeforeCreate);
//    }
//
//
//    @Test
//    @Transactional
//    public void checkMsisdnIsRequired() throws Exception {
//        int databaseSizeBeforeTest = paymentRequestRepository.findAll().size();
//        // set the field null
//        paymentRequest.setMsisdn(null);
//
//        // Create the PaymentRequest, which fails.
//        PaymentRequestDTO paymentRequestDTO = paymentRequestMapper.toDto(paymentRequest);
//
//        restPaymentRequestMockMvc.perform(post("/api/payment-requests")
//            .contentType(TestUtil.APPLICATION_JSON)
//            .content(TestUtil.convertObjectToJsonBytes(paymentRequestDTO)))
//            .andExpect(status().isBadRequest());
//
//        List<PaymentRequest> paymentRequestList = paymentRequestRepository.findAll();
//        assertThat(paymentRequestList).hasSize(databaseSizeBeforeTest);
//    }
//
//    @Test
//    @Transactional
//    public void checkAmountIsRequired() throws Exception {
//        int databaseSizeBeforeTest = paymentRequestRepository.findAll().size();
//        // set the field null
//        paymentRequest.setAmount(null);
//
//        // Create the PaymentRequest, which fails.
//        PaymentRequestDTO paymentRequestDTO = paymentRequestMapper.toDto(paymentRequest);
//
//        restPaymentRequestMockMvc.perform(post("/api/payment-requests")
//            .contentType(TestUtil.APPLICATION_JSON)
//            .content(TestUtil.convertObjectToJsonBytes(paymentRequestDTO)))
//            .andExpect(status().isBadRequest());
//
//        List<PaymentRequest> paymentRequestList = paymentRequestRepository.findAll();
//        assertThat(paymentRequestList).hasSize(databaseSizeBeforeTest);
//    }
//
//    @Test
//    @Transactional
//    public void getAllPaymentRequests() throws Exception {
//        // Initialize the database
//        paymentRequestRepository.saveAndFlush(paymentRequest);
//
//        // Get all the paymentRequestList
//        restPaymentRequestMockMvc.perform(get("/api/payment-requests?sort=id,desc"))
//            .andExpect(status().isOk())
//            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
//            .andExpect(jsonPath("$.[*].id").value(hasItem(paymentRequest.getId().intValue())))
//            .andExpect(jsonPath("$.[*].msisdn").value(hasItem(DEFAULT_MSISDN)))
//            .andExpect(jsonPath("$.[*].amount").value(hasItem(DEFAULT_AMOUNT)))
//            .andExpect(jsonPath("$.[*].identificationNo").value(hasItem(DEFAULT_IDENTIFICATION_NO)))
//            .andExpect(jsonPath("$.[*].firstName").value(hasItem(DEFAULT_FIRST_NAME)))
//            .andExpect(jsonPath("$.[*].lastName").value(hasItem(DEFAULT_LAST_NAME)))
//            .andExpect(jsonPath("$.[*].comment").value(hasItem(DEFAULT_COMMENT)))
//            .andExpect(jsonPath("$.[*].paymentState").value(hasItem(DEFAULT_PAYMENT_STATE.toString())))
//            .andExpect(jsonPath("$.[*].paymentTxId").value(hasItem(DEFAULT_PAYMENT_TX_ID)))
//            .andExpect(jsonPath("$.[*].paymentMessage").value(hasItem(DEFAULT_PAYMENT_MESSAGE)));
//    }
//
//    @Test
//    @Transactional
//    public void getPaymentRequest() throws Exception {
//        // Initialize the database
//        paymentRequestRepository.saveAndFlush(paymentRequest);
//
//        // Get the paymentRequest
//        restPaymentRequestMockMvc.perform(get("/api/payment-requests/{id}", paymentRequest.getId()))
//            .andExpect(status().isOk())
//            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
//            .andExpect(jsonPath("$.id").value(paymentRequest.getId().intValue()))
//            .andExpect(jsonPath("$.msisdn").value(DEFAULT_MSISDN))
//            .andExpect(jsonPath("$.amount").value(DEFAULT_AMOUNT))
//            .andExpect(jsonPath("$.identificationNo").value(DEFAULT_IDENTIFICATION_NO))
//            .andExpect(jsonPath("$.firstName").value(DEFAULT_FIRST_NAME))
//            .andExpect(jsonPath("$.lastName").value(DEFAULT_LAST_NAME))
//            .andExpect(jsonPath("$.comment").value(DEFAULT_COMMENT))
//            .andExpect(jsonPath("$.paymentState").value(DEFAULT_PAYMENT_STATE.toString()))
//            .andExpect(jsonPath("$.paymentTxId").value(DEFAULT_PAYMENT_TX_ID))
//            .andExpect(jsonPath("$.paymentMessage").value(DEFAULT_PAYMENT_MESSAGE));
//    }
//
//    @Test
//    @Transactional
//    public void getNonExistingPaymentRequest() throws Exception {
//        // Get the paymentRequest
//        restPaymentRequestMockMvc.perform(get("/api/payment-requests/{id}", Long.MAX_VALUE))
//            .andExpect(status().isNotFound());
//    }
//
//    @Test
//    @Transactional
//    public void updatePaymentRequest() throws Exception {
//        // Initialize the database
//        paymentRequestRepository.saveAndFlush(paymentRequest);
//
//        int databaseSizeBeforeUpdate = paymentRequestRepository.findAll().size();
//
//        // Update the paymentRequest
//        PaymentRequest updatedPaymentRequest = paymentRequestRepository.findById(paymentRequest.getId()).get();
//        // Disconnect from session so that the updates on updatedPaymentRequest are not directly saved in db
//        em.detach(updatedPaymentRequest);
//        updatedPaymentRequest
//            .msisdn(UPDATED_MSISDN)
//            .amount(UPDATED_AMOUNT)
//            .identificationNo(UPDATED_IDENTIFICATION_NO)
//            .firstName(UPDATED_FIRST_NAME)
//            .lastName(UPDATED_LAST_NAME)
//            .comment(UPDATED_COMMENT)
//            .paymentState(UPDATED_PAYMENT_STATE)
//            .paymentTxId(UPDATED_PAYMENT_TX_ID)
//            .paymentMessage(UPDATED_PAYMENT_MESSAGE);
//        PaymentRequestDTO paymentRequestDTO = paymentRequestMapper.toDto(updatedPaymentRequest);
//
//        restPaymentRequestMockMvc.perform(put("/api/payment-requests")
//            .contentType(TestUtil.APPLICATION_JSON)
//            .content(TestUtil.convertObjectToJsonBytes(paymentRequestDTO)))
//            .andExpect(status().isOk());
//
//        // Validate the PaymentRequest in the database
//        List<PaymentRequest> paymentRequestList = paymentRequestRepository.findAll();
//        assertThat(paymentRequestList).hasSize(databaseSizeBeforeUpdate);
//        PaymentRequest testPaymentRequest = paymentRequestList.get(paymentRequestList.size() - 1);
//        assertThat(testPaymentRequest.getMsisdn()).isEqualTo(UPDATED_MSISDN);
//        assertThat(testPaymentRequest.getAmount()).isEqualTo(UPDATED_AMOUNT);
//        assertThat(testPaymentRequest.getIdentificationNo()).isEqualTo(UPDATED_IDENTIFICATION_NO);
//        assertThat(testPaymentRequest.getFirstName()).isEqualTo(UPDATED_FIRST_NAME);
//        assertThat(testPaymentRequest.getLastName()).isEqualTo(UPDATED_LAST_NAME);
//        assertThat(testPaymentRequest.getComment()).isEqualTo(UPDATED_COMMENT);
//        assertThat(testPaymentRequest.getPaymentState()).isEqualTo(UPDATED_PAYMENT_STATE);
//        assertThat(testPaymentRequest.getPaymentTxId()).isEqualTo(UPDATED_PAYMENT_TX_ID);
//        assertThat(testPaymentRequest.getPaymentMessage()).isEqualTo(UPDATED_PAYMENT_MESSAGE);
//    }
//
//    @Test
//    @Transactional
//    public void updateNonExistingPaymentRequest() throws Exception {
//        int databaseSizeBeforeUpdate = paymentRequestRepository.findAll().size();
//
//        // Create the PaymentRequest
//        PaymentRequestDTO paymentRequestDTO = paymentRequestMapper.toDto(paymentRequest);
//
//        // If the entity doesn't have an ID, it will throw BadRequestAlertException
//        restPaymentRequestMockMvc.perform(put("/api/payment-requests")
//            .contentType(TestUtil.APPLICATION_JSON)
//            .content(TestUtil.convertObjectToJsonBytes(paymentRequestDTO)))
//            .andExpect(status().isBadRequest());
//
//        // Validate the PaymentRequest in the database
//        List<PaymentRequest> paymentRequestList = paymentRequestRepository.findAll();
//        assertThat(paymentRequestList).hasSize(databaseSizeBeforeUpdate);
//    }
//
//    @Test
//    @Transactional
//    public void deletePaymentRequest() throws Exception {
//        // Initialize the database
//        paymentRequestRepository.saveAndFlush(paymentRequest);
//
//        int databaseSizeBeforeDelete = paymentRequestRepository.findAll().size();
//
//        // Delete the paymentRequest
//        restPaymentRequestMockMvc.perform(delete("/api/payment-requests/{id}", paymentRequest.getId())
//            .accept(TestUtil.APPLICATION_JSON))
//            .andExpect(status().isNoContent());
//
//        // Validate the database contains one less item
//        List<PaymentRequest> paymentRequestList = paymentRequestRepository.findAll();
//        assertThat(paymentRequestList).hasSize(databaseSizeBeforeDelete - 1);
//    }
//}
