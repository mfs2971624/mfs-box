package sn.free.mfs.web.rest;

import sn.free.mfs.MfsboxApp;
import sn.free.mfs.domain.RemittanceMtoTypes;
import sn.free.mfs.repository.RemittanceMtoTypesRepository;
import sn.free.mfs.service.RemittanceMtoTypesService;
import sn.free.mfs.service.dto.RemittanceMtoTypesDTO;
import sn.free.mfs.service.mapper.RemittanceMtoTypesMapper;
import sn.free.mfs.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static sn.free.mfs.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link RemittanceMtoTypesResource} REST controller.
 */
@SpringBootTest(classes = MfsboxApp.class)
public class RemittanceMtoTypesResourceIT {

    private static final Integer DEFAULT_DOMAINE_ID = 1;
    private static final Integer UPDATED_DOMAINE_ID = 2;

    private static final String DEFAULT_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_LABEL = "AAAAAAAAAA";
    private static final String UPDATED_LABEL = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private RemittanceMtoTypesRepository remittanceMtoTypesRepository;

    @Autowired
    private RemittanceMtoTypesMapper remittanceMtoTypesMapper;

    @Autowired
    private RemittanceMtoTypesService remittanceMtoTypesService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restRemittanceMtoTypesMockMvc;

    private RemittanceMtoTypes remittanceMtoTypes;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RemittanceMtoTypesResource remittanceMtoTypesResource = new RemittanceMtoTypesResource(remittanceMtoTypesService);
        this.restRemittanceMtoTypesMockMvc = MockMvcBuilders.standaloneSetup(remittanceMtoTypesResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RemittanceMtoTypes createEntity(EntityManager em) {
        RemittanceMtoTypes remittanceMtoTypes = new RemittanceMtoTypes()
            .domaineId(DEFAULT_DOMAINE_ID)
            .type(DEFAULT_TYPE)
            .label(DEFAULT_LABEL)
            .description(DEFAULT_DESCRIPTION);
        return remittanceMtoTypes;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RemittanceMtoTypes createUpdatedEntity(EntityManager em) {
        RemittanceMtoTypes remittanceMtoTypes = new RemittanceMtoTypes()
            .domaineId(UPDATED_DOMAINE_ID)
            .type(UPDATED_TYPE)
            .label(UPDATED_LABEL)
            .description(UPDATED_DESCRIPTION);
        return remittanceMtoTypes;
    }

    @BeforeEach
    public void initTest() {
        remittanceMtoTypes = createEntity(em);
    }

    @Test
    @Transactional
    public void createRemittanceMtoTypes() throws Exception {
        int databaseSizeBeforeCreate = remittanceMtoTypesRepository.findAll().size();

        // Create the RemittanceMtoTypes
        RemittanceMtoTypesDTO remittanceMtoTypesDTO = remittanceMtoTypesMapper.toDto(remittanceMtoTypes);
        restRemittanceMtoTypesMockMvc.perform(post("/api/remittance-mto-types")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(remittanceMtoTypesDTO)))
            .andExpect(status().isCreated());

        // Validate the RemittanceMtoTypes in the database
        List<RemittanceMtoTypes> remittanceMtoTypesList = remittanceMtoTypesRepository.findAll();
        assertThat(remittanceMtoTypesList).hasSize(databaseSizeBeforeCreate + 1);
        RemittanceMtoTypes testRemittanceMtoTypes = remittanceMtoTypesList.get(remittanceMtoTypesList.size() - 1);
        assertThat(testRemittanceMtoTypes.getDomaineId()).isEqualTo(DEFAULT_DOMAINE_ID);
        assertThat(testRemittanceMtoTypes.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(testRemittanceMtoTypes.getLabel()).isEqualTo(DEFAULT_LABEL);
        assertThat(testRemittanceMtoTypes.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createRemittanceMtoTypesWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = remittanceMtoTypesRepository.findAll().size();

        // Create the RemittanceMtoTypes with an existing ID
        remittanceMtoTypes.setId(1L);
        RemittanceMtoTypesDTO remittanceMtoTypesDTO = remittanceMtoTypesMapper.toDto(remittanceMtoTypes);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRemittanceMtoTypesMockMvc.perform(post("/api/remittance-mto-types")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(remittanceMtoTypesDTO)))
            .andExpect(status().isBadRequest());

        // Validate the RemittanceMtoTypes in the database
        List<RemittanceMtoTypes> remittanceMtoTypesList = remittanceMtoTypesRepository.findAll();
        assertThat(remittanceMtoTypesList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkDomaineIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = remittanceMtoTypesRepository.findAll().size();
        // set the field null
        remittanceMtoTypes.setDomaineId(null);

        // Create the RemittanceMtoTypes, which fails.
        RemittanceMtoTypesDTO remittanceMtoTypesDTO = remittanceMtoTypesMapper.toDto(remittanceMtoTypes);

        restRemittanceMtoTypesMockMvc.perform(post("/api/remittance-mto-types")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(remittanceMtoTypesDTO)))
            .andExpect(status().isBadRequest());

        List<RemittanceMtoTypes> remittanceMtoTypesList = remittanceMtoTypesRepository.findAll();
        assertThat(remittanceMtoTypesList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = remittanceMtoTypesRepository.findAll().size();
        // set the field null
        remittanceMtoTypes.setType(null);

        // Create the RemittanceMtoTypes, which fails.
        RemittanceMtoTypesDTO remittanceMtoTypesDTO = remittanceMtoTypesMapper.toDto(remittanceMtoTypes);

        restRemittanceMtoTypesMockMvc.perform(post("/api/remittance-mto-types")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(remittanceMtoTypesDTO)))
            .andExpect(status().isBadRequest());

        List<RemittanceMtoTypes> remittanceMtoTypesList = remittanceMtoTypesRepository.findAll();
        assertThat(remittanceMtoTypesList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLabelIsRequired() throws Exception {
        int databaseSizeBeforeTest = remittanceMtoTypesRepository.findAll().size();
        // set the field null
        remittanceMtoTypes.setLabel(null);

        // Create the RemittanceMtoTypes, which fails.
        RemittanceMtoTypesDTO remittanceMtoTypesDTO = remittanceMtoTypesMapper.toDto(remittanceMtoTypes);

        restRemittanceMtoTypesMockMvc.perform(post("/api/remittance-mto-types")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(remittanceMtoTypesDTO)))
            .andExpect(status().isBadRequest());

        List<RemittanceMtoTypes> remittanceMtoTypesList = remittanceMtoTypesRepository.findAll();
        assertThat(remittanceMtoTypesList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllRemittanceMtoTypes() throws Exception {
        // Initialize the database
        remittanceMtoTypesRepository.saveAndFlush(remittanceMtoTypes);

        // Get all the remittanceMtoTypesList
        restRemittanceMtoTypesMockMvc.perform(get("/api/remittance-mto-types?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(remittanceMtoTypes.getId().intValue())))
            .andExpect(jsonPath("$.[*].domaineId").value(hasItem(DEFAULT_DOMAINE_ID)))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE)))
            .andExpect(jsonPath("$.[*].label").value(hasItem(DEFAULT_LABEL)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)));
    }
    
    @Test
    @Transactional
    public void getRemittanceMtoTypes() throws Exception {
        // Initialize the database
        remittanceMtoTypesRepository.saveAndFlush(remittanceMtoTypes);

        // Get the remittanceMtoTypes
        restRemittanceMtoTypesMockMvc.perform(get("/api/remittance-mto-types/{id}", remittanceMtoTypes.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(remittanceMtoTypes.getId().intValue()))
            .andExpect(jsonPath("$.domaineId").value(DEFAULT_DOMAINE_ID))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE))
            .andExpect(jsonPath("$.label").value(DEFAULT_LABEL))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION));
    }

    @Test
    @Transactional
    public void getNonExistingRemittanceMtoTypes() throws Exception {
        // Get the remittanceMtoTypes
        restRemittanceMtoTypesMockMvc.perform(get("/api/remittance-mto-types/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRemittanceMtoTypes() throws Exception {
        // Initialize the database
        remittanceMtoTypesRepository.saveAndFlush(remittanceMtoTypes);

        int databaseSizeBeforeUpdate = remittanceMtoTypesRepository.findAll().size();

        // Update the remittanceMtoTypes
        RemittanceMtoTypes updatedRemittanceMtoTypes = remittanceMtoTypesRepository.findById(remittanceMtoTypes.getId()).get();
        // Disconnect from session so that the updates on updatedRemittanceMtoTypes are not directly saved in db
        em.detach(updatedRemittanceMtoTypes);
        updatedRemittanceMtoTypes
            .domaineId(UPDATED_DOMAINE_ID)
            .type(UPDATED_TYPE)
            .label(UPDATED_LABEL)
            .description(UPDATED_DESCRIPTION);
        RemittanceMtoTypesDTO remittanceMtoTypesDTO = remittanceMtoTypesMapper.toDto(updatedRemittanceMtoTypes);

        restRemittanceMtoTypesMockMvc.perform(put("/api/remittance-mto-types")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(remittanceMtoTypesDTO)))
            .andExpect(status().isOk());

        // Validate the RemittanceMtoTypes in the database
        List<RemittanceMtoTypes> remittanceMtoTypesList = remittanceMtoTypesRepository.findAll();
        assertThat(remittanceMtoTypesList).hasSize(databaseSizeBeforeUpdate);
        RemittanceMtoTypes testRemittanceMtoTypes = remittanceMtoTypesList.get(remittanceMtoTypesList.size() - 1);
        assertThat(testRemittanceMtoTypes.getDomaineId()).isEqualTo(UPDATED_DOMAINE_ID);
        assertThat(testRemittanceMtoTypes.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testRemittanceMtoTypes.getLabel()).isEqualTo(UPDATED_LABEL);
        assertThat(testRemittanceMtoTypes.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingRemittanceMtoTypes() throws Exception {
        int databaseSizeBeforeUpdate = remittanceMtoTypesRepository.findAll().size();

        // Create the RemittanceMtoTypes
        RemittanceMtoTypesDTO remittanceMtoTypesDTO = remittanceMtoTypesMapper.toDto(remittanceMtoTypes);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRemittanceMtoTypesMockMvc.perform(put("/api/remittance-mto-types")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(remittanceMtoTypesDTO)))
            .andExpect(status().isBadRequest());

        // Validate the RemittanceMtoTypes in the database
        List<RemittanceMtoTypes> remittanceMtoTypesList = remittanceMtoTypesRepository.findAll();
        assertThat(remittanceMtoTypesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRemittanceMtoTypes() throws Exception {
        // Initialize the database
        remittanceMtoTypesRepository.saveAndFlush(remittanceMtoTypes);

        int databaseSizeBeforeDelete = remittanceMtoTypesRepository.findAll().size();

        // Delete the remittanceMtoTypes
        restRemittanceMtoTypesMockMvc.perform(delete("/api/remittance-mto-types/{id}", remittanceMtoTypes.getId())
            .accept(TestUtil.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<RemittanceMtoTypes> remittanceMtoTypesList = remittanceMtoTypesRepository.findAll();
        assertThat(remittanceMtoTypesList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
