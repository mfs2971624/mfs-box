package sn.free.mfs.web.rest;

import sn.free.mfs.MfsboxApp;
import sn.free.mfs.domain.RemittanceTransaction;
import sn.free.mfs.repository.RemittanceTransactionRepository;
import sn.free.mfs.service.RemittanceTransactionService;
import sn.free.mfs.service.dto.RemittanceTransactionDTO;
import sn.free.mfs.service.mapper.RemittanceTransactionMapper;
import sn.free.mfs.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.Column;
import javax.persistence.EntityManager;
import java.util.List;

import static sn.free.mfs.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link RemittanceTransactionResource} REST controller.
 */
@SpringBootTest(classes = MfsboxApp.class)
public class RemittanceTransactionResourceIT {

    private static final String DEFAULT_EXTERNAL_ID = "AAAAAAAAAA";
    private static final String UPDATED_EXTERNAL_ID = "BBBBBBBBBB";

    private static final String DEFAULT_EXTERNAL_CODE = "AAAAAAAAAA";
    private static final String UPDATED_EXTERNAL_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_BENEFICIARY_FIRST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_BENEFICIARY_FIRST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_BENEFICIARY_LAST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_BENEFICIARY_LAST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_BENEFICIARY_MSISDN = "AAAAAAAAAA";
    private static final String UPDATED_BENEFICIARY_MSISDN = "BBBBBBBBBB";

    private static final String DEFAULT_SENDER_FIRST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_SENDER_FIRST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_SENDER_LAST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_SENDER_LAST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_SOURCE_COUNTRY_ISO_CODE = "AAAAAAAAAA";
    private static final String UPDATED_SOURCE_COUNTRY_ISO_CODE = "BBBBBBBBBB";

    private static final Double DEFAULT_SENT_AMOUNT = 1D;
    private static final Double UPDATED_SENT_AMOUNT = 2D;

    private static final String DEFAULT_SENT_AMOUNT_CURRENCY = "AAAAAAAAAA";
    private static final String UPDATED_SENT_AMOUNT_CURRENCY = "BBBBBBBBBB";

    private static final Double DEFAULT_DESTINATION_AMOUNT = 1D;
    private static final Double UPDATED_DESTINATION_AMOUNT = 2D;

    private static final String DEFAULT_DESTINATION_AMOUNT_CURRENCY = "AAAAAAAAAA";
    private static final String UPDATED_DESTINATION_AMOUNT_CURRENCY = "BBBBBBBBBB";

    private static final String DEFAULT_CREATION_DATE = "AAAAAAAAAA";
    private static final String UPDATED_CREATION_DATE = "BBBBBBBBBB";

    private static final String DEFAULT_STATUS_MESSAGE = "AAAAAAAAAA";
    private static final String UPDATED_STATUS_MESSAGE = "BBBBBBBBBB";


    private static final String DEFAULT_MTO_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_MTO_TYPE = "BBBBBBBBBB";


    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    @Autowired
    private RemittanceTransactionRepository remittanceTransactionRepository;

    @Autowired
    private RemittanceTransactionMapper remittanceTransactionMapper;

    @Autowired
    private RemittanceTransactionService remittanceTransactionService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restRemittanceTransactionMockMvc;

    private RemittanceTransaction remittanceTransaction;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RemittanceTransactionResource remittanceTransactionResource = new RemittanceTransactionResource(remittanceTransactionService);
        this.restRemittanceTransactionMockMvc = MockMvcBuilders.standaloneSetup(remittanceTransactionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RemittanceTransaction createEntity(EntityManager em) {
        RemittanceTransaction remittanceTransaction = new RemittanceTransaction()
            .externalId(DEFAULT_EXTERNAL_ID)
            .externalCode(DEFAULT_EXTERNAL_CODE)
            .beneficiaryFirstName(DEFAULT_BENEFICIARY_FIRST_NAME)
            .beneficiaryLastName(DEFAULT_BENEFICIARY_LAST_NAME)
            .beneficiaryMsisdn(DEFAULT_BENEFICIARY_MSISDN)
            .senderFirstName(DEFAULT_SENDER_FIRST_NAME)
            .senderLastName(DEFAULT_SENDER_LAST_NAME)
            .sourceCountryIsoCode(DEFAULT_SOURCE_COUNTRY_ISO_CODE)
            .sentAmount(DEFAULT_SENT_AMOUNT)
            .sentAmountCurrency(DEFAULT_SENT_AMOUNT_CURRENCY)
            .destinationAmount(DEFAULT_DESTINATION_AMOUNT)
            .destinationAmountCurrency(DEFAULT_DESTINATION_AMOUNT_CURRENCY)
            .creationDate(DEFAULT_CREATION_DATE)
            .creationDate(DEFAULT_CREATION_DATE)
            .creationDate(DEFAULT_CREATION_DATE)
            .statusMessage(DEFAULT_STATUS_MESSAGE)
            .mtoType(DEFAULT_MTO_TYPE)
            .status(DEFAULT_STATUS);
        return remittanceTransaction;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RemittanceTransaction createUpdatedEntity(EntityManager em) {
        RemittanceTransaction remittanceTransaction = new RemittanceTransaction()
            .externalId(UPDATED_EXTERNAL_ID)
            .externalCode(UPDATED_EXTERNAL_CODE)
            .beneficiaryFirstName(UPDATED_BENEFICIARY_FIRST_NAME)
            .beneficiaryLastName(UPDATED_BENEFICIARY_LAST_NAME)
            .beneficiaryMsisdn(UPDATED_BENEFICIARY_MSISDN)
            .senderFirstName(UPDATED_SENDER_FIRST_NAME)
            .senderLastName(UPDATED_SENDER_LAST_NAME)
            .sourceCountryIsoCode(UPDATED_SOURCE_COUNTRY_ISO_CODE)
            .sentAmount(UPDATED_SENT_AMOUNT)
            .sentAmountCurrency(UPDATED_SENT_AMOUNT_CURRENCY)
            .destinationAmount(UPDATED_DESTINATION_AMOUNT)
            .destinationAmountCurrency(UPDATED_DESTINATION_AMOUNT_CURRENCY)
            .creationDate(UPDATED_CREATION_DATE)
            .statusMessage(UPDATED_STATUS_MESSAGE)
            .mtoType(UPDATED_MTO_TYPE)
            .status(UPDATED_STATUS);
        return remittanceTransaction;
    }

    @BeforeEach
    public void initTest() {
        remittanceTransaction = createEntity(em);
    }

    @Test
    @Transactional
    public void createRemittanceTransaction() throws Exception {
        int databaseSizeBeforeCreate = remittanceTransactionRepository.findAll().size();

        // Create the RemittanceTransaction
        RemittanceTransactionDTO remittanceTransactionDTO = remittanceTransactionMapper.toDto(remittanceTransaction);
        restRemittanceTransactionMockMvc.perform(post("/api/remittance-transactions")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(remittanceTransactionDTO)))
            .andExpect(status().isCreated());

        // Validate the RemittanceTransaction in the database
        List<RemittanceTransaction> remittanceTransactionList = remittanceTransactionRepository.findAll();
        assertThat(remittanceTransactionList).hasSize(databaseSizeBeforeCreate + 1);
        RemittanceTransaction testRemittanceTransaction = remittanceTransactionList.get(remittanceTransactionList.size() - 1);
        assertThat(testRemittanceTransaction.getExternalId()).isEqualTo(DEFAULT_EXTERNAL_ID);
        assertThat(testRemittanceTransaction.getExternalCode()).isEqualTo(DEFAULT_EXTERNAL_CODE);
        assertThat(testRemittanceTransaction.getBeneficiaryFirstName()).isEqualTo(DEFAULT_BENEFICIARY_FIRST_NAME);
        assertThat(testRemittanceTransaction.getBeneficiaryLastName()).isEqualTo(DEFAULT_BENEFICIARY_LAST_NAME);
        assertThat(testRemittanceTransaction.getBeneficiaryMsisdn()).isEqualTo(DEFAULT_BENEFICIARY_MSISDN);
        assertThat(testRemittanceTransaction.getSenderFirstName()).isEqualTo(DEFAULT_SENDER_FIRST_NAME);
        assertThat(testRemittanceTransaction.getSenderLastName()).isEqualTo(DEFAULT_SENDER_LAST_NAME);
        assertThat(testRemittanceTransaction.getSourceCountryIsoCode()).isEqualTo(DEFAULT_SOURCE_COUNTRY_ISO_CODE);
        assertThat(testRemittanceTransaction.getSentAmount()).isEqualTo(DEFAULT_SENT_AMOUNT);
        assertThat(testRemittanceTransaction.getSentAmountCurrency()).isEqualTo(DEFAULT_SENT_AMOUNT_CURRENCY);
        assertThat(testRemittanceTransaction.getDestinationAmount()).isEqualTo(DEFAULT_DESTINATION_AMOUNT);
        assertThat(testRemittanceTransaction.getDestinationAmountCurrency()).isEqualTo(DEFAULT_DESTINATION_AMOUNT_CURRENCY);
        assertThat(testRemittanceTransaction.getCreationDate()).isEqualTo(DEFAULT_CREATION_DATE);
        assertThat(testRemittanceTransaction.getStatusMessage()).isEqualTo(DEFAULT_STATUS_MESSAGE);
        assertThat(testRemittanceTransaction.getMtoType()).isEqualTo(DEFAULT_MTO_TYPE);
        assertThat(testRemittanceTransaction.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    public void createRemittanceTransactionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = remittanceTransactionRepository.findAll().size();

        // Create the RemittanceTransaction with an existing ID
        remittanceTransaction.setId(1L);
        RemittanceTransactionDTO remittanceTransactionDTO = remittanceTransactionMapper.toDto(remittanceTransaction);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRemittanceTransactionMockMvc.perform(post("/api/remittance-transactions")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(remittanceTransactionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the RemittanceTransaction in the database
        List<RemittanceTransaction> remittanceTransactionList = remittanceTransactionRepository.findAll();
        assertThat(remittanceTransactionList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkExternalIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = remittanceTransactionRepository.findAll().size();
        // set the field null
        remittanceTransaction.setExternalId(null);

        // Create the RemittanceTransaction, which fails.
        RemittanceTransactionDTO remittanceTransactionDTO = remittanceTransactionMapper.toDto(remittanceTransaction);

        restRemittanceTransactionMockMvc.perform(post("/api/remittance-transactions")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(remittanceTransactionDTO)))
            .andExpect(status().isBadRequest());

        List<RemittanceTransaction> remittanceTransactionList = remittanceTransactionRepository.findAll();
        assertThat(remittanceTransactionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkExternalCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = remittanceTransactionRepository.findAll().size();
        // set the field null
        remittanceTransaction.setExternalCode(null);

        // Create the RemittanceTransaction, which fails.
        RemittanceTransactionDTO remittanceTransactionDTO = remittanceTransactionMapper.toDto(remittanceTransaction);

        restRemittanceTransactionMockMvc.perform(post("/api/remittance-transactions")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(remittanceTransactionDTO)))
            .andExpect(status().isBadRequest());

        List<RemittanceTransaction> remittanceTransactionList = remittanceTransactionRepository.findAll();
        assertThat(remittanceTransactionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllRemittanceTransactions() throws Exception {
        // Initialize the database
        remittanceTransactionRepository.saveAndFlush(remittanceTransaction);

        // Get all the remittanceTransactionList
        restRemittanceTransactionMockMvc.perform(get("/api/remittance-transactions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(remittanceTransaction.getId().intValue())))
            .andExpect(jsonPath("$.[*].externalId").value(hasItem(DEFAULT_EXTERNAL_ID)))
            .andExpect(jsonPath("$.[*].externalCode").value(hasItem(DEFAULT_EXTERNAL_CODE)))
            .andExpect(jsonPath("$.[*].beneficiaryFirstName").value(hasItem(DEFAULT_BENEFICIARY_FIRST_NAME)))
            .andExpect(jsonPath("$.[*].beneficiaryLastName").value(hasItem(DEFAULT_BENEFICIARY_LAST_NAME)))
            .andExpect(jsonPath("$.[*].beneficiaryMsisdn").value(hasItem(DEFAULT_BENEFICIARY_MSISDN)))
            .andExpect(jsonPath("$.[*].senderFirstName").value(hasItem(DEFAULT_SENDER_FIRST_NAME)))
            .andExpect(jsonPath("$.[*].senderLastName").value(hasItem(DEFAULT_SENDER_LAST_NAME)))
            .andExpect(jsonPath("$.[*].sourceCountryIsoCode").value(hasItem(DEFAULT_SOURCE_COUNTRY_ISO_CODE)))
            .andExpect(jsonPath("$.[*].sentAmount").value(hasItem(DEFAULT_SENT_AMOUNT.doubleValue())))
            .andExpect(jsonPath("$.[*].sentAmountCurrency").value(hasItem(DEFAULT_SENT_AMOUNT_CURRENCY)))
            .andExpect(jsonPath("$.[*].destinationAmount").value(hasItem(DEFAULT_DESTINATION_AMOUNT.doubleValue())))
            .andExpect(jsonPath("$.[*].destinationAmountCurrency").value(hasItem(DEFAULT_DESTINATION_AMOUNT_CURRENCY)))
            .andExpect(jsonPath("$.[*].creationDate").value(hasItem(DEFAULT_CREATION_DATE)))
            .andExpect(jsonPath("$.[*].statusMessage").value(hasItem(DEFAULT_STATUS_MESSAGE)))
            .andExpect(jsonPath("$.[*].mtoType").value(hasItem(DEFAULT_MTO_TYPE)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)));
    }

    @Test
    @Transactional
    public void getRemittanceTransaction() throws Exception {
        // Initialize the database
        remittanceTransactionRepository.saveAndFlush(remittanceTransaction);

        // Get the remittanceTransaction
        restRemittanceTransactionMockMvc.perform(get("/api/remittance-transactions/{id}", remittanceTransaction.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(remittanceTransaction.getId().intValue()))
            .andExpect(jsonPath("$.externalId").value(DEFAULT_EXTERNAL_ID))
            .andExpect(jsonPath("$.externalCode").value(DEFAULT_EXTERNAL_CODE))
            .andExpect(jsonPath("$.beneficiaryFirstName").value(DEFAULT_BENEFICIARY_FIRST_NAME))
            .andExpect(jsonPath("$.beneficiaryLastName").value(DEFAULT_BENEFICIARY_LAST_NAME))
            .andExpect(jsonPath("$.beneficiaryMsisdn").value(DEFAULT_BENEFICIARY_MSISDN))
            .andExpect(jsonPath("$.senderFirstName").value(DEFAULT_SENDER_FIRST_NAME))
            .andExpect(jsonPath("$.senderLastName").value(DEFAULT_SENDER_LAST_NAME))
            .andExpect(jsonPath("$.sourceCountryIsoCode").value(DEFAULT_SOURCE_COUNTRY_ISO_CODE))
            .andExpect(jsonPath("$.sentAmount").value(DEFAULT_SENT_AMOUNT.doubleValue()))
            .andExpect(jsonPath("$.sentAmountCurrency").value(DEFAULT_SENT_AMOUNT_CURRENCY))
            .andExpect(jsonPath("$.destinationAmount").value(DEFAULT_DESTINATION_AMOUNT.doubleValue()))
            .andExpect(jsonPath("$.destinationAmountCurrency").value(DEFAULT_DESTINATION_AMOUNT_CURRENCY))
            .andExpect(jsonPath("$.creationDate").value(DEFAULT_CREATION_DATE))
            .andExpect(jsonPath("$.statusMessage").value(DEFAULT_STATUS_MESSAGE))
            .andExpect(jsonPath("$.mtoType").value(DEFAULT_MTO_TYPE))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS));
    }

    @Test
    @Transactional
    public void getNonExistingRemittanceTransaction() throws Exception {
        // Get the remittanceTransaction
        restRemittanceTransactionMockMvc.perform(get("/api/remittance-transactions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRemittanceTransaction() throws Exception {
        // Initialize the database
        remittanceTransactionRepository.saveAndFlush(remittanceTransaction);

        int databaseSizeBeforeUpdate = remittanceTransactionRepository.findAll().size();

        // Update the remittanceTransaction
        RemittanceTransaction updatedRemittanceTransaction = remittanceTransactionRepository.findById(remittanceTransaction.getId()).get();
        // Disconnect from session so that the updates on updatedRemittanceTransaction are not directly saved in db
        em.detach(updatedRemittanceTransaction);
        updatedRemittanceTransaction
            .externalId(UPDATED_EXTERNAL_ID)
            .externalCode(UPDATED_EXTERNAL_CODE)
            .beneficiaryFirstName(UPDATED_BENEFICIARY_FIRST_NAME)
            .beneficiaryLastName(UPDATED_BENEFICIARY_LAST_NAME)
            .beneficiaryMsisdn(UPDATED_BENEFICIARY_MSISDN)
            .senderFirstName(UPDATED_SENDER_FIRST_NAME)
            .senderLastName(UPDATED_SENDER_LAST_NAME)
            .sourceCountryIsoCode(UPDATED_SOURCE_COUNTRY_ISO_CODE)
            .sentAmount(UPDATED_SENT_AMOUNT)
            .sentAmountCurrency(UPDATED_SENT_AMOUNT_CURRENCY)
            .destinationAmount(UPDATED_DESTINATION_AMOUNT)
            .destinationAmountCurrency(UPDATED_DESTINATION_AMOUNT_CURRENCY)
            .creationDate(UPDATED_CREATION_DATE)
            .statusMessage(UPDATED_STATUS_MESSAGE)
            .mtoType(UPDATED_MTO_TYPE)
            .status(UPDATED_STATUS);
        RemittanceTransactionDTO remittanceTransactionDTO = remittanceTransactionMapper.toDto(updatedRemittanceTransaction);

        restRemittanceTransactionMockMvc.perform(put("/api/remittance-transactions")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(remittanceTransactionDTO)))
            .andExpect(status().isOk());

        // Validate the RemittanceTransaction in the database
        List<RemittanceTransaction> remittanceTransactionList = remittanceTransactionRepository.findAll();
        assertThat(remittanceTransactionList).hasSize(databaseSizeBeforeUpdate);
        RemittanceTransaction testRemittanceTransaction = remittanceTransactionList.get(remittanceTransactionList.size() - 1);
        assertThat(testRemittanceTransaction.getExternalId()).isEqualTo(UPDATED_EXTERNAL_ID);
        assertThat(testRemittanceTransaction.getExternalCode()).isEqualTo(UPDATED_EXTERNAL_CODE);
        assertThat(testRemittanceTransaction.getBeneficiaryFirstName()).isEqualTo(UPDATED_BENEFICIARY_FIRST_NAME);
        assertThat(testRemittanceTransaction.getBeneficiaryLastName()).isEqualTo(UPDATED_BENEFICIARY_LAST_NAME);
        assertThat(testRemittanceTransaction.getBeneficiaryMsisdn()).isEqualTo(UPDATED_BENEFICIARY_MSISDN);
        assertThat(testRemittanceTransaction.getSenderFirstName()).isEqualTo(UPDATED_SENDER_FIRST_NAME);
        assertThat(testRemittanceTransaction.getSenderLastName()).isEqualTo(UPDATED_SENDER_LAST_NAME);
        assertThat(testRemittanceTransaction.getSourceCountryIsoCode()).isEqualTo(UPDATED_SOURCE_COUNTRY_ISO_CODE);
        assertThat(testRemittanceTransaction.getSentAmount()).isEqualTo(UPDATED_SENT_AMOUNT);
        assertThat(testRemittanceTransaction.getSentAmountCurrency()).isEqualTo(UPDATED_SENT_AMOUNT_CURRENCY);
        assertThat(testRemittanceTransaction.getDestinationAmount()).isEqualTo(UPDATED_DESTINATION_AMOUNT);
        assertThat(testRemittanceTransaction.getDestinationAmountCurrency()).isEqualTo(UPDATED_DESTINATION_AMOUNT_CURRENCY);
        assertThat(testRemittanceTransaction.getCreationDate()).isEqualTo(UPDATED_CREATION_DATE);
        assertThat(testRemittanceTransaction.getStatusMessage()).isEqualTo(UPDATED_STATUS_MESSAGE);
        assertThat(testRemittanceTransaction.getMtoType()).isEqualTo(UPDATED_MTO_TYPE);
        assertThat(testRemittanceTransaction.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void updateNonExistingRemittanceTransaction() throws Exception {
        int databaseSizeBeforeUpdate = remittanceTransactionRepository.findAll().size();

        // Create the RemittanceTransaction
        RemittanceTransactionDTO remittanceTransactionDTO = remittanceTransactionMapper.toDto(remittanceTransaction);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRemittanceTransactionMockMvc.perform(put("/api/remittance-transactions")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(remittanceTransactionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the RemittanceTransaction in the database
        List<RemittanceTransaction> remittanceTransactionList = remittanceTransactionRepository.findAll();
        assertThat(remittanceTransactionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRemittanceTransaction() throws Exception {
        // Initialize the database
        remittanceTransactionRepository.saveAndFlush(remittanceTransaction);

        int databaseSizeBeforeDelete = remittanceTransactionRepository.findAll().size();

        // Delete the remittanceTransaction
        restRemittanceTransactionMockMvc.perform(delete("/api/remittance-transactions/{id}", remittanceTransaction.getId())
            .accept(TestUtil.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<RemittanceTransaction> remittanceTransactionList = remittanceTransactionRepository.findAll();
        assertThat(remittanceTransactionList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
