package sn.free.mfs.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class BulkDisbursementMapperTest {

    private BulkDisbursementMapper bulkDisbursementMapper;

    @BeforeEach
    public void setUp() {
        bulkDisbursementMapper = new BulkDisbursementMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(bulkDisbursementMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(bulkDisbursementMapper.fromId(null)).isNull();
    }
}
