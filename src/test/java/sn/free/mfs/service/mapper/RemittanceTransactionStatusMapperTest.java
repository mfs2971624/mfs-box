package sn.free.mfs.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class RemittanceTransactionStatusMapperTest {

    private RemittanceTransactionStatusMapper remittanceTransactionStatusMapper;

    @BeforeEach
    public void setUp() {
        remittanceTransactionStatusMapper = new RemittanceTransactionStatusMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(remittanceTransactionStatusMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(remittanceTransactionStatusMapper.fromId(null)).isNull();
    }
}
