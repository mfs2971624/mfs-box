package sn.free.mfs.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class RemittanceMtoTypesMapperTest {

    private RemittanceMtoTypesMapper remittanceMtoTypesMapper;

    @BeforeEach
    public void setUp() {
        remittanceMtoTypesMapper = new RemittanceMtoTypesMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(remittanceMtoTypesMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(remittanceMtoTypesMapper.fromId(null)).isNull();
    }
}
