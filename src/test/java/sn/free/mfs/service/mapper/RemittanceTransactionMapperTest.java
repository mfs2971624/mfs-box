package sn.free.mfs.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class RemittanceTransactionMapperTest {

    private RemittanceTransactionMapper remittanceTransactionMapper;

    @BeforeEach
    public void setUp() {
        remittanceTransactionMapper = new RemittanceTransactionMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(remittanceTransactionMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(remittanceTransactionMapper.fromId(null)).isNull();
    }
}
