package sn.free.mfs.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class PaymentRequestMapperTest {

    private PaymentRequestMapper paymentRequestMapper;

    @BeforeEach
    public void setUp() {
        paymentRequestMapper = new PaymentRequestMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(paymentRequestMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(paymentRequestMapper.fromId(null)).isNull();
    }
}
