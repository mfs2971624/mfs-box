package sn.free.mfs.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import sn.free.mfs.web.rest.TestUtil;

public class BulkDisbursementDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(BulkDisbursementDTO.class);
        BulkDisbursementDTO bulkDisbursementDTO1 = new BulkDisbursementDTO();
        bulkDisbursementDTO1.setId(1L);
        BulkDisbursementDTO bulkDisbursementDTO2 = new BulkDisbursementDTO();
        assertThat(bulkDisbursementDTO1).isNotEqualTo(bulkDisbursementDTO2);
        bulkDisbursementDTO2.setId(bulkDisbursementDTO1.getId());
        assertThat(bulkDisbursementDTO1).isEqualTo(bulkDisbursementDTO2);
        bulkDisbursementDTO2.setId(2L);
        assertThat(bulkDisbursementDTO1).isNotEqualTo(bulkDisbursementDTO2);
        bulkDisbursementDTO1.setId(null);
        assertThat(bulkDisbursementDTO1).isNotEqualTo(bulkDisbursementDTO2);
    }
}
