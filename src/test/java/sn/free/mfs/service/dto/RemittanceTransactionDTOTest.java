package sn.free.mfs.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import sn.free.mfs.web.rest.TestUtil;

public class RemittanceTransactionDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(RemittanceTransactionDTO.class);
        RemittanceTransactionDTO remittanceTransactionDTO1 = new RemittanceTransactionDTO();
        remittanceTransactionDTO1.setId(1L);
        RemittanceTransactionDTO remittanceTransactionDTO2 = new RemittanceTransactionDTO();
        assertThat(remittanceTransactionDTO1).isNotEqualTo(remittanceTransactionDTO2);
        remittanceTransactionDTO2.setId(remittanceTransactionDTO1.getId());
        assertThat(remittanceTransactionDTO1).isEqualTo(remittanceTransactionDTO2);
        remittanceTransactionDTO2.setId(2L);
        assertThat(remittanceTransactionDTO1).isNotEqualTo(remittanceTransactionDTO2);
        remittanceTransactionDTO1.setId(null);
        assertThat(remittanceTransactionDTO1).isNotEqualTo(remittanceTransactionDTO2);
    }
}
