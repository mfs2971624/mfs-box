package sn.free.mfs.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import sn.free.mfs.web.rest.TestUtil;

public class RemittanceMtoTypesDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(RemittanceMtoTypesDTO.class);
        RemittanceMtoTypesDTO remittanceMtoTypesDTO1 = new RemittanceMtoTypesDTO();
        remittanceMtoTypesDTO1.setId(1L);
        RemittanceMtoTypesDTO remittanceMtoTypesDTO2 = new RemittanceMtoTypesDTO();
        assertThat(remittanceMtoTypesDTO1).isNotEqualTo(remittanceMtoTypesDTO2);
        remittanceMtoTypesDTO2.setId(remittanceMtoTypesDTO1.getId());
        assertThat(remittanceMtoTypesDTO1).isEqualTo(remittanceMtoTypesDTO2);
        remittanceMtoTypesDTO2.setId(2L);
        assertThat(remittanceMtoTypesDTO1).isNotEqualTo(remittanceMtoTypesDTO2);
        remittanceMtoTypesDTO1.setId(null);
        assertThat(remittanceMtoTypesDTO1).isNotEqualTo(remittanceMtoTypesDTO2);
    }
}
