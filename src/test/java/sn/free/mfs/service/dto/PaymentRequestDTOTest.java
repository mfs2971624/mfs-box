package sn.free.mfs.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import sn.free.mfs.web.rest.TestUtil;

public class PaymentRequestDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PaymentRequestDTO.class);
        PaymentRequestDTO paymentRequestDTO1 = new PaymentRequestDTO();
        paymentRequestDTO1.setId(1L);
        PaymentRequestDTO paymentRequestDTO2 = new PaymentRequestDTO();
        assertThat(paymentRequestDTO1).isNotEqualTo(paymentRequestDTO2);
        paymentRequestDTO2.setId(paymentRequestDTO1.getId());
        assertThat(paymentRequestDTO1).isEqualTo(paymentRequestDTO2);
        paymentRequestDTO2.setId(2L);
        assertThat(paymentRequestDTO1).isNotEqualTo(paymentRequestDTO2);
        paymentRequestDTO1.setId(null);
        assertThat(paymentRequestDTO1).isNotEqualTo(paymentRequestDTO2);
    }
}
