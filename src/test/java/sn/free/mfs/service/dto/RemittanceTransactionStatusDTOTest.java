package sn.free.mfs.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import sn.free.mfs.web.rest.TestUtil;

public class RemittanceTransactionStatusDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(RemittanceTransactionStatusDTO.class);
        RemittanceTransactionStatusDTO remittanceTransactionStatusDTO1 = new RemittanceTransactionStatusDTO();
        remittanceTransactionStatusDTO1.setId(1L);
        RemittanceTransactionStatusDTO remittanceTransactionStatusDTO2 = new RemittanceTransactionStatusDTO();
        assertThat(remittanceTransactionStatusDTO1).isNotEqualTo(remittanceTransactionStatusDTO2);
        remittanceTransactionStatusDTO2.setId(remittanceTransactionStatusDTO1.getId());
        assertThat(remittanceTransactionStatusDTO1).isEqualTo(remittanceTransactionStatusDTO2);
        remittanceTransactionStatusDTO2.setId(2L);
        assertThat(remittanceTransactionStatusDTO1).isNotEqualTo(remittanceTransactionStatusDTO2);
        remittanceTransactionStatusDTO1.setId(null);
        assertThat(remittanceTransactionStatusDTO1).isNotEqualTo(remittanceTransactionStatusDTO2);
    }
}
