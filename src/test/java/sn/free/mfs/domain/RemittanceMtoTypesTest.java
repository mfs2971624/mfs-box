package sn.free.mfs.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import sn.free.mfs.web.rest.TestUtil;

public class RemittanceMtoTypesTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RemittanceMtoTypes.class);
        RemittanceMtoTypes remittanceMtoTypes1 = new RemittanceMtoTypes();
        remittanceMtoTypes1.setId(1L);
        RemittanceMtoTypes remittanceMtoTypes2 = new RemittanceMtoTypes();
        remittanceMtoTypes2.setId(remittanceMtoTypes1.getId());
        assertThat(remittanceMtoTypes1).isEqualTo(remittanceMtoTypes2);
        remittanceMtoTypes2.setId(2L);
        assertThat(remittanceMtoTypes1).isNotEqualTo(remittanceMtoTypes2);
        remittanceMtoTypes1.setId(null);
        assertThat(remittanceMtoTypes1).isNotEqualTo(remittanceMtoTypes2);
    }
}
