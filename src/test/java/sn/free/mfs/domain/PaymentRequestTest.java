package sn.free.mfs.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import sn.free.mfs.web.rest.TestUtil;

public class PaymentRequestTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PaymentRequest.class);
        PaymentRequest paymentRequest1 = new PaymentRequest();
        paymentRequest1.setId(1L);
        PaymentRequest paymentRequest2 = new PaymentRequest();
        paymentRequest2.setId(paymentRequest1.getId());
        assertThat(paymentRequest1).isEqualTo(paymentRequest2);
        paymentRequest2.setId(2L);
        assertThat(paymentRequest1).isNotEqualTo(paymentRequest2);
        paymentRequest1.setId(null);
        assertThat(paymentRequest1).isNotEqualTo(paymentRequest2);
    }
}
