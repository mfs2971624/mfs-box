package sn.free.mfs.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import sn.free.mfs.web.rest.TestUtil;

public class RemittanceTransactionStatusTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RemittanceTransactionStatus.class);
        RemittanceTransactionStatus remittanceTransactionStatus1 = new RemittanceTransactionStatus();
        remittanceTransactionStatus1.setId(1L);
        RemittanceTransactionStatus remittanceTransactionStatus2 = new RemittanceTransactionStatus();
        remittanceTransactionStatus2.setId(remittanceTransactionStatus1.getId());
        assertThat(remittanceTransactionStatus1).isEqualTo(remittanceTransactionStatus2);
        remittanceTransactionStatus2.setId(2L);
        assertThat(remittanceTransactionStatus1).isNotEqualTo(remittanceTransactionStatus2);
        remittanceTransactionStatus1.setId(null);
        assertThat(remittanceTransactionStatus1).isNotEqualTo(remittanceTransactionStatus2);
    }
}
