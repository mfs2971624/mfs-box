package sn.free.mfs.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import sn.free.mfs.web.rest.TestUtil;

public class BulkDisbursementTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(BulkDisbursement.class);
        BulkDisbursement bulkDisbursement1 = new BulkDisbursement();
        bulkDisbursement1.setId(1L);
        BulkDisbursement bulkDisbursement2 = new BulkDisbursement();
        bulkDisbursement2.setId(bulkDisbursement1.getId());
        assertThat(bulkDisbursement1).isEqualTo(bulkDisbursement2);
        bulkDisbursement2.setId(2L);
        assertThat(bulkDisbursement1).isNotEqualTo(bulkDisbursement2);
        bulkDisbursement1.setId(null);
        assertThat(bulkDisbursement1).isNotEqualTo(bulkDisbursement2);
    }
}
