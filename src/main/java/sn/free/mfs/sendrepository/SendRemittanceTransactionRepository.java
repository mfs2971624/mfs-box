package sn.free.mfs.sendrepository;

import sn.free.mfs.seconddomain.RemittanceTransactionSent;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the RemittanceTransactionSent entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SendRemittanceTransactionRepository extends JpaRepository<RemittanceTransactionSent, Long> {
    List<RemittanceTransactionSent> findBySenderIdNumber(String senderIdNumber);
    List<RemittanceTransactionSent> findByRecipientIdNumber(String recipientIdNumber);
    List<RemittanceTransactionSent> findBySenderMsisdn(String senderMsisdn);
    List<RemittanceTransactionSent> findByRecipientMsisdn(String recipientMsisdn);
    List<RemittanceTransactionSent> findByCodeMto(String codeMto);
    List<RemittanceTransactionSent> findByMobiquityTransactionId(String mobiquityTransactionId);
    List<RemittanceTransactionSent> findByMtoTransactionId(String mtoTransactionId);
    List<RemittanceTransactionSent> findByTransactionStatus(String transactionStatus);
}
