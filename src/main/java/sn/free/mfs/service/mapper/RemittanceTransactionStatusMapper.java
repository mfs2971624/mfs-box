package sn.free.mfs.service.mapper;


import sn.free.mfs.domain.*;
import sn.free.mfs.service.dto.RemittanceTransactionStatusDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link RemittanceTransactionStatus} and its DTO {@link RemittanceTransactionStatusDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface RemittanceTransactionStatusMapper extends EntityMapper<RemittanceTransactionStatusDTO, RemittanceTransactionStatus> {



    default RemittanceTransactionStatus fromId(Long id) {
        if (id == null) {
            return null;
        }
        RemittanceTransactionStatus remittanceTransactionStatus = new RemittanceTransactionStatus();
        remittanceTransactionStatus.setId(id);
        return remittanceTransactionStatus;
    }
}
