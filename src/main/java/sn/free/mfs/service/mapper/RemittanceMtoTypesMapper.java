package sn.free.mfs.service.mapper;


import sn.free.mfs.domain.*;
import sn.free.mfs.service.dto.RemittanceMtoTypesDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link RemittanceMtoTypes} and its DTO {@link RemittanceMtoTypesDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface RemittanceMtoTypesMapper extends EntityMapper<RemittanceMtoTypesDTO, RemittanceMtoTypes> {



    default RemittanceMtoTypes fromId(Long id) {
        if (id == null) {
            return null;
        }
        RemittanceMtoTypes remittanceMtoTypes = new RemittanceMtoTypes();
        remittanceMtoTypes.setId(id);
        return remittanceMtoTypes;
    }
}
