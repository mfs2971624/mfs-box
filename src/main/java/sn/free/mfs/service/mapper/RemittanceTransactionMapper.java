package sn.free.mfs.service.mapper;


import sn.free.mfs.domain.*;
import sn.free.mfs.service.dto.RemittanceTransactionDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link RemittanceTransaction} and its DTO {@link RemittanceTransactionDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface RemittanceTransactionMapper extends EntityMapper<RemittanceTransactionDTO, RemittanceTransaction> {


    default RemittanceTransaction fromId(Long id) {
        if (id == null) {
            return null;
        }
        RemittanceTransaction remittanceTransaction = new RemittanceTransaction();
        remittanceTransaction.setId(id);
        return remittanceTransaction;
    }
}
