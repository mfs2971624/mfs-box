package sn.free.mfs.service.mapper;


import sn.free.mfs.domain.*;
import sn.free.mfs.service.dto.BulkDisbursementDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link BulkDisbursement} and its DTO {@link BulkDisbursementDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface BulkDisbursementMapper extends EntityMapper<BulkDisbursementDTO, BulkDisbursement> {



    default BulkDisbursement fromId(Long id) {
        if (id == null) {
            return null;
        }
        BulkDisbursement bulkDisbursement = new BulkDisbursement();
        bulkDisbursement.setId(id);
        return bulkDisbursement;
    }
}
