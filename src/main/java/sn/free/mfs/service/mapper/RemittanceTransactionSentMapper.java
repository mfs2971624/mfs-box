package sn.free.mfs.service.mapper;


import sn.free.mfs.seconddomain.*;
import sn.free.mfs.service.dto.RemittanceTransactionSentDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link RemittanceTransactionSent} and its DTO {@link RemittanceTransactionSentDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface RemittanceTransactionSentMapper extends EntityMapper<RemittanceTransactionSentDTO, RemittanceTransactionSent> {


    default RemittanceTransactionSent fromId(Long id) {
        if (id == null) {
            return null;
        }
        RemittanceTransactionSent remittanceTransactionSent = new RemittanceTransactionSent();
        remittanceTransactionSent.setId(id);
        return remittanceTransactionSent;
    }
}
