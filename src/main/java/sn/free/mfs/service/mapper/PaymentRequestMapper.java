package sn.free.mfs.service.mapper;


import sn.free.mfs.domain.*;
import sn.free.mfs.service.dto.PaymentRequestDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link PaymentRequest} and its DTO {@link PaymentRequestDTO}.
 */
@Mapper(componentModel = "spring", uses = {BulkDisbursementMapper.class})
public interface PaymentRequestMapper extends EntityMapper<PaymentRequestDTO, PaymentRequest> {

    @Mapping(source = "bulkDisbursement.id", target = "bulkDisbursementId")
    @Mapping(source = "bulkDisbursement.bulkId", target = "bulkDisbursementBulkId")
    PaymentRequestDTO toDto(PaymentRequest paymentRequest);

    @Mapping(source = "bulkDisbursementId", target = "bulkDisbursement")
    PaymentRequest toEntity(PaymentRequestDTO paymentRequestDTO);

    default PaymentRequest fromId(Long id) {
        if (id == null) {
            return null;
        }
        PaymentRequest paymentRequest = new PaymentRequest();
        paymentRequest.setId(id);
        return paymentRequest;
    }
}
