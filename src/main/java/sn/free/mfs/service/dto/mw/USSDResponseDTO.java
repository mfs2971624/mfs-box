package sn.free.mfs.service.dto.mw;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * @project: mfs
 * @author: psow on 24/03/2020
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class USSDResponseDTO {
    private DocNode doc;
}
