package sn.free.mfs.service.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import sn.free.mfs.domain.enumeration.BulkType;
import sn.free.mfs.domain.enumeration.ProcessingStatus;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;

/**
 * A DTO for the {@link sn.free.mfs.domain.BulkDisbursement} entity.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BulkDisbursementDTO implements Serializable {

    private Long id;

    @NotNull
    private String bulkId;

    @NotNull
    private String msisdnSource;

    @NotNull
    private BulkType type;

    private String description;

    private ProcessingStatus processingStatus;

    private Instant uploadStart;

    private Instant uploadEnd;

    private Instant paymentStart;

    private Instant paymentEnd;

}
