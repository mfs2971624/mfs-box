package sn.free.mfs.service.dto;

import javax.persistence.Column;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link sn.free.mfs.domain.RemittanceTransaction} entity.
 */
public class RemittanceTransactionDTO implements Serializable {

    private Long id;

    @NotNull
    private String externalId;

    @NotNull
    private String externalCode;

    private String beneficiaryFirstName;

    private String beneficiaryLastName;

    private String beneficiaryMsisdn;

    private String senderFirstName;

    private String senderLastName;

    private String sourceCountryIsoCode;

    private Double sentAmount;

    private String sentAmountCurrency;

    private Double destinationAmount;

    private String destinationAmountCurrency;

    private String creationDate;

    private String statusMessage;

    private String mtoType;

    private String status;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public String getExternalCode() {
        return externalCode;
    }

    public void setExternalCode(String externalCode) {
        this.externalCode = externalCode;
    }

    public String getBeneficiaryFirstName() {
        return beneficiaryFirstName;
    }

    public void setBeneficiaryFirstName(String beneficiaryFirstName) {
        this.beneficiaryFirstName = beneficiaryFirstName;
    }

    public String getBeneficiaryLastName() {
        return beneficiaryLastName;
    }

    public void setBeneficiaryLastName(String beneficiaryLastName) {
        this.beneficiaryLastName = beneficiaryLastName;
    }

    public String getBeneficiaryMsisdn() {
        return beneficiaryMsisdn;
    }

    public void setBeneficiaryMsisdn(String beneficiaryMsisdn) {
        this.beneficiaryMsisdn = beneficiaryMsisdn;
    }

    public String getSenderFirstName() {
        return senderFirstName;
    }

    public void setSenderFirstName(String senderFirstName) {
        this.senderFirstName = senderFirstName;
    }

    public String getSenderLastName() {
        return senderLastName;
    }

    public void setSenderLastName(String senderLastName) {
        this.senderLastName = senderLastName;
    }

    public String getSourceCountryIsoCode() {
        return sourceCountryIsoCode;
    }

    public void setSourceCountryIsoCode(String sourceCountryIsoCode) {
        this.sourceCountryIsoCode = sourceCountryIsoCode;
    }

    public Double getSentAmount() {
        return sentAmount;
    }

    public void setSentAmount(Double sentAmount) {
        this.sentAmount = sentAmount;
    }

    public String getSentAmountCurrency() {
        return sentAmountCurrency;
    }

    public void setSentAmountCurrency(String sentAmountCurrency) {
        this.sentAmountCurrency = sentAmountCurrency;
    }

    public Double getDestinationAmount() {
        return destinationAmount;
    }

    public void setDestinationAmount(Double destinationAmount) {
        this.destinationAmount = destinationAmount;
    }

    public String getDestinationAmountCurrency() {
        return destinationAmountCurrency;
    }

    public void setDestinationAmountCurrency(String destinationAmountCurrency) {
        this.destinationAmountCurrency = destinationAmountCurrency;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public String getMtoType() {
        return mtoType;
    }

    public void setMtoType(String mtoType) {
        this.mtoType = mtoType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        RemittanceTransactionDTO remittanceTransactionDTO = (RemittanceTransactionDTO) o;
        if (remittanceTransactionDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), remittanceTransactionDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "RemittanceTransactionDTO{" +
            "id=" + getId() +
            ", externalId=" + getExternalId() +
            ", externalCode='" + getExternalCode() + "'" +
            ", beneficiaryFirstName='" + getBeneficiaryFirstName() + "'" +
            ", beneficiaryLastName='" + getBeneficiaryLastName() + "'" +
            ", beneficiaryMsisdn='" + getBeneficiaryMsisdn() + "'" +
            ", senderFirstName='" + getSenderFirstName() + "'" +
            ", senderLastName='" + getSenderLastName() + "'" +
            ", sourceCountryIsoCode='" + getSourceCountryIsoCode() + "'" +
            ", sentAmount=" + getSentAmount() +
            ", sentAmountCurrency='" + getSentAmountCurrency() + "'" +
            ", destinationAmount=" + getDestinationAmount() +
            ", destinationAmountCurrency='" + getDestinationAmountCurrency() + "'" +
            ", creationDate='" + getCreationDate() + "'" +
            ", statusMessage='" + getStatusMessage() + "'" +
            ", statusId=" + getMtoType() +
            ", statusDomaineId='" + getStatus() + "'" +
            "}";
    }
}
