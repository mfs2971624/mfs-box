package sn.free.mfs.service.dto;

import javax.persistence.Column;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link sn.free.mfs.domain.RemittanceTransactionSent} entity.
 */

public class RemittanceTransactionSentDTO implements Serializable {

    private static final long serialVersionUID = 1L;


    private Long id;

    private String mobiquityTransactionId;

    private String mtoTransactionId;

    private String createdAt;

    private String updatedAt;

    private String recipientSurname;

    private String recipientLastName;

    private String recipientPrename;

    private String recipientMiddleName;

    private String recipientPostalCode;

    private String recipientState;

    private String recipientToCountry;

    private String recipientMsisdn;
    
    private String recipientAddress;
    
    private String recipientCity;

    private String recipientDateOfBirth;

    private String recipientIdNumber;

    private String recipientIdType;

    private String recipientIdCountry;

    private String recipientExpirationDate;

    private String recipientEmail;

    private String senderFirstName;

    private String senderLastName;

    private String senderMiddleName;
    
    private String senderAddress;

    private String senderCity;

    private String senderDateOfBirth;

    private String senderIdNumber;

    private String senderIdType;

    private String senderIdCountry;
  
    private String senderEmail;

    private String senderFromCountry;
 
    private String senderMsisdn;

    private String senderIdExpirationDate;
    
    private String senderSurname;

    private String senderPostalCode;
 
    private Double sentAmount;

    private String sentCurrencyCode;

    private Double sentFeeAmount;

    private String sentFeeCurrencyCode;

    private String codeMto;

    private String transactionStatus;

    private String transactionStatusMessage;

    private String transactionAttemptsNumber;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMobiquityTransactionId() {
		return mobiquityTransactionId;
	}

	public void setMobiquityTransactionId(String mobiquityTransactionId) {
		this.mobiquityTransactionId = mobiquityTransactionId;
	}

	public String getMtoTransactionId() {
		return mtoTransactionId;
	}

	public void setMtoTransactionId(String mtoTransactionId) {
		this.mtoTransactionId = mtoTransactionId;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getRecipientSurname() {
		return recipientSurname;
	}

	public void setRecipientSurname(String recipientSurname) {
		this.recipientSurname = recipientSurname;
	}

	public String getRecipientLastName() {
		return recipientLastName;
	}

	public void setRecipientLastName(String recipientLastName) {
		this.recipientLastName = recipientLastName;
	}

	public String getRecipientPrename() {
		return recipientPrename;
	}

	public void setRecipientPrename(String recipientPrename) {
		this.recipientPrename = recipientPrename;
	}

	public String getRecipientMiddleName() {
		return recipientMiddleName;
	}

	public void setRecipientMiddleName(String recipientMiddleName) {
		this.recipientMiddleName = recipientMiddleName;
	}

	public String getRecipientPostalCode() {
		return recipientPostalCode;
	}

	public void setRecipientPostalCode(String recipientPostalCode) {
		this.recipientPostalCode = recipientPostalCode;
	}

	public String getRecipientState() {
		return recipientState;
	}

	public void setRecipientState(String recipientState) {
		this.recipientState = recipientState;
	}

	public String getRecipientToCountry() {
		return recipientToCountry;
	}

	public void setRecipientToCountry(String recipientToCountry) {
		this.recipientToCountry = recipientToCountry;
	}

	public String getRecipientMsisdn() {
		return recipientMsisdn;
	}

	public void setRecipientMsisdn(String recipientMsisdn) {
		this.recipientMsisdn = recipientMsisdn;
	}

	public String getRecipientAddress() {
		return recipientAddress;
	}

	public void setRecipientAddress(String recipientAddress) {
		this.recipientAddress = recipientAddress;
	}

	public String getRecipientCity() {
		return recipientCity;
	}

	public void setRecipientCity(String recipientCity) {
		this.recipientCity = recipientCity;
	}

	public String getRecipientDateOfBirth() {
		return recipientDateOfBirth;
	}

	public void setRecipientDateOfBirth(String recipientDateOfBirth) {
		this.recipientDateOfBirth = recipientDateOfBirth;
	}

	public String getRecipientIdNumber() {
		return recipientIdNumber;
	}

	public void setRecipientIdNumber(String recipientIdNumber) {
		this.recipientIdNumber = recipientIdNumber;
	}

	public String getRecipientIdType() {
		return recipientIdType;
	}

	public void setRecipientIdType(String recipientIdType) {
		this.recipientIdType = recipientIdType;
	}

	public String getRecipientIdCountry() {
		return recipientIdCountry;
	}

	public void setRecipientIdCountry(String recipientIdCountry) {
		this.recipientIdCountry = recipientIdCountry;
	}

	public String getRecipientExpirationDate() {
		return recipientExpirationDate;
	}

	public void setRecipientExpirationDate(String recipientExpirationDate) {
		this.recipientExpirationDate = recipientExpirationDate;
	}

	public String getRecipientEmail() {
		return recipientEmail;
	}

	public void setRecipientEmail(String recipientEmail) {
		this.recipientEmail = recipientEmail;
	}


	public String getSenderFirstName() {
		return senderFirstName;
	}

	public void setSenderFirstName(String senderFirstName) {
		this.senderFirstName = senderFirstName;
	}


	public String getSenderLastName() {
		return senderLastName;
	}

	public void setSenderLastName(String senderLastName) {
		this.senderLastName = senderLastName;
	}


	public String getSenderMiddleName() {
		return senderMiddleName;
	}

	public void setSenderMiddleName(String senderMiddleName) {
		this.senderMiddleName = senderMiddleName;
	}


	public String getSenderAddress() {
		return senderAddress;
	}

	public void setSenderAddress(String senderAddress) {
		this.senderAddress = senderAddress;
	}


	public String getSenderCity() {
		return senderCity;
	}

	public void setSenderCity(String senderCity) {
		this.senderCity = senderCity;
	}


	public String getSenderDateOfBirth() {
		return senderDateOfBirth;
	}

	public void setSenderDateOfBirth(String senderDateOfBirth) {
		this.senderDateOfBirth = senderDateOfBirth;
	}


	public String getSenderIdNumber() {
		return senderIdNumber;
	}

	public void setSenderIdNumber(String senderIdNumber) {
		this.senderIdNumber = senderIdNumber;
	}


	public String getSenderIdType() {
		return senderIdType;
	}

	public void setSenderIdType(String senderIdType) {
		this.senderIdType = senderIdType;
	}


	public String getSenderIdCountry() {
		return senderIdCountry;
	}

	public void setSenderIdCountry(String senderIdCountry) {
		this.senderIdCountry = senderIdCountry;
	}


	public String getSenderEmail() {
		return senderEmail;
	}

	public void setSenderEmail(String senderEmail) {
		this.senderEmail = senderEmail;
	}


	public String getSenderFromCountry() {
		return senderFromCountry;
	}

	public void setSenderFromCountry(String senderFromCountry) {
		this.senderFromCountry = senderFromCountry;
	}


	public String getSenderMsisdn() {
		return senderMsisdn;
	}

	public void setSenderMsisdn(String senderMsisdn) {
		this.senderMsisdn = senderMsisdn;
	}


	public String getSenderIdExpirationDate() {
		return senderIdExpirationDate;
	}

	public void setSenderIdExpirationDate(String senderIdExpirationDate) {
		this.senderIdExpirationDate = senderIdExpirationDate;
	}


	public String getSenderSurname() {
		return senderSurname;
	}

	public void setSenderSurname(String senderSurname) {
		this.senderSurname = senderSurname;
	}


	public String getSenderPostalCode() {
		return senderPostalCode;
	}

	public void setSenderPostalCode(String senderPostalCode) {
		this.senderPostalCode = senderPostalCode;
	}


	public Double getSentAmount() {
		return sentAmount;
	}

	public void setSentAmount(Double sentAmount) {
		this.sentAmount = sentAmount;
	}


	public String getSentCurrencyCode() {
		return sentCurrencyCode;
	}

	public void setSentCurrencyCode(String sentCurrencyCode) {
		this.sentCurrencyCode = sentCurrencyCode;
	}


	public Double getSentFeeAmount() {
		return sentFeeAmount;
	}

	public void setSentFeeAmount(Double sentFeeAmount) {
		this.sentFeeAmount = sentFeeAmount;
	}


	public String getSentFeeCurrencyCode() {
		return sentFeeCurrencyCode;
	}

	public void setSentFeeCurrencyCode(String sentFeeCurrencyCode) {
		this.sentFeeCurrencyCode = sentFeeCurrencyCode;
	}


	public String getCodeMto() {
		return codeMto;
	}

	public void setCodeMto(String codeMto) {
		this.codeMto = codeMto;
	}


	public String getTransactionStatus() {
		return transactionStatus;
	}

	public void setTransactionStatus(String transactionStatus) {
		this.transactionStatus = transactionStatus;
	}


	public String getTransactionStatusMessage() {
		return transactionStatusMessage;
	}

	public void setTransactionStatusMessage(String transactionStatusMessage) {
		this.transactionStatusMessage = transactionStatusMessage;
	}


	public String getTransactionAttemptsNumber() {
		return transactionAttemptsNumber;
	}

	public void setTransactionAttemptsNumber(String transactionAttemptsNumber) {
		this.transactionAttemptsNumber = transactionAttemptsNumber;
	}


    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        RemittanceTransactionSentDTO remittanceTransactionSentDTO = (RemittanceTransactionSentDTO) o;
        if (remittanceTransactionSentDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), remittanceTransactionSentDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }


    @Override
    public String toString() {
        return "RemittanceTransactionSentDTO{" +
            " id='" + getId() + "'" +
            ", mobiquityTransactionId='" + getMobiquityTransactionId() + "'" +
            ", mtoTransactionId='" + getMtoTransactionId() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            ", recipientSurname='" + getRecipientSurname() + "'" +
            ", recipientLastName='" + getRecipientLastName() + "'" +
            ", recipientPrename='" + getRecipientPrename() + "'" +
            ", recipientMiddleName='" + getRecipientMiddleName() + "'" +
            ", recipientPostalCode='" + getRecipientPostalCode() + "'" +
            ", recipientState='" + getRecipientState() + "'" +
            ", recipientToCountry='" + getRecipientToCountry() + "'" +
            ", recipientMsisdn='" + getRecipientMsisdn() + "'" +
            ", recipientAddress='" + getRecipientAddress() + "'" +
            ", recipientCity='" + getRecipientCity() + "'" +
            ", recipientDateOfBirth='" + getRecipientDateOfBirth() + "'" +
            ", recipientIdNumber='" + getRecipientIdNumber() + "'" +
            ", recipientIdType='" + getRecipientIdType() + "'" +
            ", recipientIdCountry='" + getRecipientIdCountry() + "'" +
            ", recipientExpirationDate='" + getRecipientExpirationDate() + "'" +
            ", recipientEmail='" + getRecipientEmail() + "'" +
            ", senderFirstName='" + getSenderFirstName() + "'" +
            ", senderLastName='" + getSenderLastName() + "'" +
            ", senderMiddleName='" + getSenderMiddleName() + "'" +
            ", senderAddress='" + getSenderAddress() + "'" +
            ", senderCity='" + getSenderCity() + "'" +
            ", senderDateOfBirth='" + getSenderDateOfBirth() + "'" +
            ", senderIdNumber='" + getSenderIdNumber() + "'" +
            ", senderIdType='" + getSenderIdType() + "'" +
            ", senderIdCountry='" + getSenderIdCountry() + "'" +
            ", senderEmail='" + getSenderEmail() + "'" +
            ", senderFromCountry='" + getSenderFromCountry() + "'" +
            ", senderMsisdn='" + getSenderMsisdn() + "'" +
            ", senderIdExpirationDate='" + getSenderIdExpirationDate() + "'" +
            ", senderSurname='" + getSenderSurname() + "'" +
            ", senderPostalCode='" + getSenderPostalCode() + "'" +
            ", sentAmount='" + getSentAmount() + "'" +
            ", sentCurrencyCode='" + getSentCurrencyCode() + "'" +
            ", sentFeeAmount='" + getSentFeeAmount() + "'" +
            ", sentFeeCurrencyCode='" + getSentFeeCurrencyCode() + "'" +
            ", codeMto='" + getCodeMto() + "'" +
            ", transactionStatus='" + getTransactionStatus() + "'" +
            ", transactionStatusMessage='" + getTransactionStatusMessage() + "'" +
            ", transactionAttemptsNumber='" + getTransactionAttemptsNumber() + "'" +
            "}";
    }


}
