package sn.free.mfs.service.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonIgnoreProperties
public class OrderDTO implements Serializable {

    @JsonProperty("PCOrderNo")
    private String PCOrderNo;
    @JsonProperty("SCOrderNo")
    private String SCOrderNo;
    @JsonProperty("OrderStatus")
    private String OrderStatus;
    @JsonProperty("StatusDate")
    private String StatusDate;
    @JsonProperty("StatusTime")
    private String StatusTime;
    @JsonProperty("Reason")
    private String Reason;

}
