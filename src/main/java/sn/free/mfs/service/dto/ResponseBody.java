package sn.free.mfs.service.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonIgnoreProperties
public class ResponseBody {
    @JsonProperty("PCOrderNo")
    private String PCOrderNo;
    @JsonProperty("SCOrderNo")
    private String SCOrderNo;
    @JsonProperty("ProcessDate")
    private String ProcessDate;
    @JsonProperty("ProcessTime")
    private String ProcessTime;
    @JsonProperty("NotificationCode")
    private String NotificationCode;
    @JsonProperty("NotificationDesc")
    private String NotificationDesc;
}
