package sn.free.mfs.service.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonIgnoreProperties
public class OrderDTOResponse {

    @JsonIgnoreProperties
    public ResponseBody Response;

    @JsonIgnoreProperties
    public ErrorsBody Errors;

}
