package sn.free.mfs.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link sn.free.mfs.domain.RemittanceMtoTypes} entity.
 */
public class RemittanceMtoTypesDTO implements Serializable {

    private Long id;

    @NotNull
    private Integer domaineId;

    @NotNull
    private String type;

    @NotNull
    private String label;

    private String description;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getDomaineId() {
        return domaineId;
    }

    public void setDomaineId(Integer domaineId) {
        this.domaineId = domaineId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        RemittanceMtoTypesDTO remittanceMtoTypesDTO = (RemittanceMtoTypesDTO) o;
        if (remittanceMtoTypesDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), remittanceMtoTypesDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "RemittanceMtoTypesDTO{" +
            "id=" + getId() +
            ", domaineId=" + getDomaineId() +
            ", type='" + getType() + "'" +
            ", label='" + getLabel() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
