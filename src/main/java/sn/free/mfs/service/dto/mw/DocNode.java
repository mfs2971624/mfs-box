package sn.free.mfs.service.dto.mw;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @project: mfs
 * @author: psow on 24/03/2020
 */
@Data
@NoArgsConstructor
@AllArgsConstructor

@XmlRootElement(name = "doc")
@XmlAccessorType(XmlAccessType.FIELD)
public class DocNode {
    private ParamNode[] param;

}
