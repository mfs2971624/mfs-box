package sn.free.mfs.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import sn.free.mfs.domain.enumeration.PaymentState;

/**
 * A DTO for the {@link sn.free.mfs.domain.PaymentRequest} entity.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PaymentRequestDTO implements Serializable {

    private Long id;

    @NotNull
    private String msisdn;

    @NotNull
    private Double amount;

    private String identificationNo;

    private String firstName;

    private String lastName;

    private String comment;

    private String sms;

    private String smsSender;

    private PaymentState paymentState;

    private String paymentTxId;

    private String paymentMessage;

    private Long bulkDisbursementId;

    private String bulkDisbursementBulkId;

    private String redeemSt;

}
