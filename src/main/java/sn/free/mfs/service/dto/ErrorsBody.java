package sn.free.mfs.service.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonIgnoreProperties
public class ErrorsBody {
    @JsonProperty("InputParams")
    private String InputParams;
    @JsonProperty("ErrorMsg")
    private String ErrorMsg;
}
