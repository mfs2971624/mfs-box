package sn.free.mfs.service;

import sn.free.mfs.service.dto.PaymentRequestDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;
import java.util.stream.Stream;

/**
 * Service Interface for managing {@link sn.free.mfs.domain.PaymentRequest}.
 */
public interface PaymentRequestService {

    /**
     * Save a paymentRequest.
     *
     * @param paymentRequestDTO the entity to save.
     * @return the persisted entity.
     */
    PaymentRequestDTO save(PaymentRequestDTO paymentRequestDTO);

    /**
     * Get all the paymentRequests.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<PaymentRequestDTO> findAll(Pageable pageable);

    /**
     * Get the "id" paymentRequest.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<PaymentRequestDTO> findOne(Long id);

    /**
     * Delete the "id" paymentRequest.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Find all payment request related to a bulk disbursement
     *
     * @param bulkId bulk disbursement id
     * @param pageable page information
     * @return result in page format
     */
    Page<PaymentRequestDTO> findByBulkId(String bulkId, Pageable pageable);
}
