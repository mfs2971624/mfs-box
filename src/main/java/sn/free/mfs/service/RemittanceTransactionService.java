package sn.free.mfs.service;

import sn.free.mfs.service.dto.OrderDTO;
import sn.free.mfs.service.dto.OrderDTOResponse;
import sn.free.mfs.service.dto.RemittanceTransactionDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link sn.free.mfs.domain.RemittanceTransaction}.
 */
public interface RemittanceTransactionService {

    /**
     * Save a remittanceTransaction.
     *
     * @param remittanceTransactionDTO the entity to save.
     * @return the persisted entity.
     */
    RemittanceTransactionDTO save(RemittanceTransactionDTO remittanceTransactionDTO);

    /**
     * Get all the remittanceTransactions.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<RemittanceTransactionDTO> findAll(Pageable pageable);

    /**
     * Get the "id" remittanceTransaction.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<RemittanceTransactionDTO> findOne(Long id);

    /**
     * Delete the "id" remittanceTransaction.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    List<RemittanceTransactionDTO> findByMtoTypeOrStatusOrMsisdnOrExternalCodeOrExternalId(Pageable pageable, String mtoType, String status, String msisdn, String externalCode, String externalId);

    OrderDTOResponse cancelRemittanceTransaction(OrderDTO orderDTO);
}
