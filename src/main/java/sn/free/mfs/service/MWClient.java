package sn.free.mfs.service;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.ws.client.WebServiceClientException;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.client.support.interceptor.ClientInterceptor;
import org.springframework.ws.context.MessageContext;
import org.springframework.ws.soap.client.SoapFaultClientException;
import sn.free.mfs.service.jaxb.adjustmmoney.AdjustMmoneyAccountRequest;
import sn.free.mfs.service.jaxb.adjustmmoney.AdjustMmoneyAccountResponse;
import sn.free.mfs.service.jaxb.adjustmmoney.RequestBodyType;
import sn.free.mfs.service.jaxb.getbalance.*;

import java.io.IOException;
import java.math.BigDecimal;

/**
 * @project: mfs
 * @author: psow on 25/03/2020
 */
@Slf4j
public class MWClient extends WebServiceGatewaySupport {

    private ObjectFactory ofGetBalance = new ObjectFactory();
    private sn.free.mfs.service.jaxb.adjustmmoney.ObjectFactory ofAdjustMmoney = new sn.free.mfs.service.jaxb.adjustmmoney.ObjectFactory();

    public MWClient() {
        ClientInterceptor[] interceptors = getWebServiceTemplate().getInterceptors();

        interceptors = ArrayUtils.add(interceptors, new ClientInterceptor() {
            @Override
            public boolean handleRequest(MessageContext messageContext) throws WebServiceClientException {
                return true;
            }

            @Override
            public boolean handleResponse(MessageContext messageContext) throws WebServiceClientException {
                return true;
            }

            @Override
            public boolean handleFault(MessageContext messageContext) throws WebServiceClientException {
                return true;
            }

            @Override
            public void afterCompletion(MessageContext messageContext, Exception ex) throws WebServiceClientException {
                try {
                    System.out.println("Request :");
                    messageContext.getRequest().writeTo(System.out);
                    System.out.println("\nResponse : ");
                    messageContext.getResponse().writeTo(System.out);
                    System.out.println();
                } catch (IOException ignored) {
                }
            }
        });

        getWebServiceTemplate().setInterceptors(interceptors);
    }

    public GetBalanceResponse getBalance(String msisdn, String pin){
        GetBalanceRequest getBalanceRequest = ofGetBalance.createGetBalanceRequest();
        getBalanceRequest.setRequestHeader((RequestHeader) buildRequestHeader());
        GetBalanceRequest.RequestBody req = ofGetBalance.createGetBalanceRequestRequestBody();
        req.setUserWallet(ofGetBalance.createUserwalletType());
        req.getUserWallet().setMsisdn(msisdn);
        req.setPassword(pin);
        getBalanceRequest.setRequestBody(req);
        return (GetBalanceResponse) this.getWebServiceTemplate().marshalSendAndReceive(getBalanceRequest);
    }

    public AdjustMmoneyAccountResponse adjustMmoneyAccount(String source, String sourcePin, String target, Double amount, String uuid){
        AdjustMmoneyAccountRequest adjustMmoneyAccountRequest = ofAdjustMmoney.createAdjustMmoneyAccountRequest();
        adjustMmoneyAccountRequest.setRequestHeader((sn.free.mfs.service.jaxb.adjustmmoney.RequestHeader) buildRequestHeaderAM());
        RequestBodyType req = ofAdjustMmoney.createRequestBodyType();
        req.setSourceWallet(ofAdjustMmoney.createRequestBodyTypeSourceWallet());
        req.setTargetWallet(ofAdjustMmoney.createRequestBodyTypeTargetWallet());
        req.getSourceWallet().setMsisdn(source);
        req.getTargetWallet().setMsisdn(target);
        req.setAdjustmentType("CREDIT");
        req.setPin(sourcePin);
        req.setPaymentReference(uuid);
        try{
            req.setAmount(new BigDecimal(amount));
        }catch (NumberFormatException e){
            throw new IllegalArgumentException("Montant incorrect"+amount);
        }
        adjustMmoneyAccountRequest.setRequestBody(req);
        return (AdjustMmoneyAccountResponse) this.getWebServiceTemplate().marshalSendAndReceive(adjustMmoneyAccountRequest);

    }

    private Object buildRequestHeaderAM() {
        sn.free.mfs.service.jaxb.adjustmmoney.RequestHeader requestHeader = ofAdjustMmoney.createRequestHeader();
        requestHeader.setGeneralConsumerInformation(ofAdjustMmoney.createGeneralConsumerInfoType());
        requestHeader.getGeneralConsumerInformation().setConsumerID("MFSBOX");
        requestHeader.getGeneralConsumerInformation().setCorrelationID("RANDOM");
        requestHeader.getGeneralConsumerInformation().setCountry(sn.free.mfs.service.jaxb.adjustmmoney.CountryContentType.SEN);
        return requestHeader;
    }

    private Object buildRequestHeader() {
        RequestHeader requestHeader = ofGetBalance.createRequestHeader();
        requestHeader.setGeneralConsumerInformation(ofGetBalance.createGeneralConsumerInfoType());
        requestHeader.getGeneralConsumerInformation().setConsumerID("MFSBOX");
        requestHeader.getGeneralConsumerInformation().setCorrelationID("RANDOM");
        requestHeader.getGeneralConsumerInformation().setCountry(CountryContentType.SEN);
        return requestHeader;
    }

    public Double checkPin(String msisdnSource, String pin) {
        try {
            GetBalanceResponse balance = this.getBalance(msisdnSource, pin);
            GetBalanceResponse.ResponseBody.WalletCollection.Wallet wallet0 = balance.getResponseBody().getWalletCollection().getWallet().get(0);
            log.info("wallet0 == {}", wallet0);
            return wallet0.getWalletBalance();
        }catch (SoapFaultClientException sfe){
            sfe.getFaultStringOrReason();
            return null;
        }
    }
}
