package sn.free.mfs.service;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.ws.client.WebServiceClientException;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.client.support.interceptor.ClientInterceptor;
import org.springframework.ws.context.MessageContext;
import sn.free.mfs.service.jaxb.sendnotification.*;

import java.io.IOException;

/**
 * @project: mfs
 * @author: psow on 25/03/2020
 */
@Slf4j
public class OSBClient extends WebServiceGatewaySupport {

    private ObjectFactory snOf = new ObjectFactory();

    public OSBClient() {
        ClientInterceptor[] interceptors = getWebServiceTemplate().getInterceptors();

        interceptors = ArrayUtils.add(interceptors, new ClientInterceptor() {
            @Override
            public boolean handleRequest(MessageContext messageContext) throws WebServiceClientException {
                return true;
            }

            @Override
            public boolean handleResponse(MessageContext messageContext) throws WebServiceClientException {
                return true;
            }

            @Override
            public boolean handleFault(MessageContext messageContext) throws WebServiceClientException {
                return true;
            }

            @Override
            public void afterCompletion(MessageContext messageContext, Exception ex) throws WebServiceClientException {
                try {
                    System.out.println("Request :");
                    messageContext.getRequest().writeTo(System.out);
                    System.out.println("\nResponse : ");
                    messageContext.getResponse().writeTo(System.out);
                    System.out.println();
                } catch (IOException ignored) {
                }
            }
        });

        getWebServiceTemplate().setInterceptors(interceptors);
    }

    public SendNotificationResponse sendNotif(String msisdn, String sender, String body){
        SendNotificationRequest req = snOf.createSendNotificationRequest();
        req.setRequestHeader(buildHeader());
        RequestBodyType r = snOf.createRequestBodyType();
        r.setChannelId(ChannelIDType.SMS);
        r.setCustomerId(msisdn);
        r.setMessage(body);
        RequestBodyType.AdditionalParameters ap = snOf.createRequestBodyTypeAdditionalParameters();
        ParameterType paramSender = snOf.createParameterType();
        paramSender.setParameterName("smsShortCode");
        paramSender.setParameterValue(sender);
        ap.getParameterType().add(paramSender);
        r.setAdditionalParameters(ap);
        req.setRequestBody(r);
        return (SendNotificationResponse) this.getWebServiceTemplate().marshalSendAndReceive(req);
    }

    private RequestHeader buildHeader() {
        RequestHeader requestHeader = snOf.createRequestHeader();
        requestHeader.setGeneralConsumerInformation(snOf.createGeneralConsumerInfoType());
        requestHeader.getGeneralConsumerInformation().setConsumerID("DTS");
        requestHeader.getGeneralConsumerInformation().setCorrelationID(RandomStringUtils.randomAlphanumeric(10));
        requestHeader.getGeneralConsumerInformation().setCountry(CountryContentType.SEN);
        return requestHeader;
    }

}
