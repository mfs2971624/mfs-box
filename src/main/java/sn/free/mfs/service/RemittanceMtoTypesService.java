package sn.free.mfs.service;

import sn.free.mfs.service.dto.RemittanceMtoTypesDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link sn.free.mfs.domain.RemittanceMtoTypes}.
 */
public interface RemittanceMtoTypesService {

    /**
     * Save a remittanceMtoTypes.
     *
     * @param remittanceMtoTypesDTO the entity to save.
     * @return the persisted entity.
     */
    RemittanceMtoTypesDTO save(RemittanceMtoTypesDTO remittanceMtoTypesDTO);

    /**
     * Get all the remittanceMtoTypes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<RemittanceMtoTypesDTO> findAll(Pageable pageable);

    /**
     * Get the "id" remittanceMtoTypes.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<RemittanceMtoTypesDTO> findOne(Long id);

    /**
     * Delete the "id" remittanceMtoTypes.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
