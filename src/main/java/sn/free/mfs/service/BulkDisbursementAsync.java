package sn.free.mfs.service;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ws.soap.client.SoapFaultClientException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import sn.free.mfs.domain.BulkDisbursement;
import sn.free.mfs.domain.PaymentRequest;
import sn.free.mfs.domain.enumeration.PaymentState;
import sn.free.mfs.domain.enumeration.ProcessingStatus;
import sn.free.mfs.repository.BulkDisbursementRepository;
import sn.free.mfs.repository.PaymentRequestRepository;
import sn.free.mfs.service.dto.PaymentRequestDTO;
import sn.free.mfs.service.jaxb.adjustmmoney.AdjustMmoneyAccountResponse;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;
import java.util.concurrent.Future;
import java.util.stream.Stream;

/**
 * @project: mfs
 * @author: psow on 20/04/2020
 */

@Component
@Slf4j
public class BulkDisbursementAsync {

    private final PaymentRequestService paymentRequestService;
    private final BulkDisbursementRepository bulkDisbursementRepository;
    private final PaymentRequestRepository paymentRequestRepo;
    private final OSBClient sendNotifClient;
    private final MWClient amClient;

    public BulkDisbursementAsync(PaymentRequestService paymentRequestService, BulkDisbursementRepository bulkDisbursementRepository, PaymentRequestRepository paymentRequestRepo, OSBClient sendNotifClient, MWClient amClient) {
        this.paymentRequestService = paymentRequestService;
        this.bulkDisbursementRepository = bulkDisbursementRepository;
        this.paymentRequestRepo = paymentRequestRepo;
        this.sendNotifClient = sendNotifClient;
        this.amClient = amClient;
    }


    @Async("taskExecutor")
    public Future<List<PaymentRequestDTO>> addPayments(InputStream inputStream, BulkDisbursement bulkDisbursement) throws IOException {
        final Long id = bulkDisbursement.getId();
        CSVParser csvParser = new CSVParserBuilder().withSeparator(';').build();

        CSVReader csvReader = new CSVReaderBuilder(new InputStreamReader(inputStream)).withCSVParser(csvParser).build();
        List<String[]> allLine = csvReader.readAll();
        List<PaymentRequestDTO> res = new ArrayList<>();
        allLine.parallelStream().skip(1).forEach(paymentL -> res.add(this.paymentRequestService.save(PaymentRequestDTO.builder()
            .msisdn(paymentL[0])
            .amount(Double.valueOf(paymentL[1]))
            .identificationNo(paymentL[2])
            .firstName(paymentL[3])
            .lastName(paymentL[4])
            .comment(paymentL[5])
            .sms(paymentL[6])
            .smsSender(paymentL[7])
            .paymentState(PaymentState.PENDING)
            .bulkDisbursementId(id)
            .build())));
        //close
        IOUtils.closeQuietly(inputStream);
        bulkDisbursement.setProcessingStatus(ProcessingStatus.INITIATED);
        bulkDisbursement.setUploadEnd(LocalDateTime.now().toInstant(ZoneOffset.UTC));
        bulkDisbursementRepository.save(bulkDisbursement);
        return new AsyncResult<>(res);
    }

    @Async
    @Transactional
    public void bulkPayments(String pin, BulkDisbursement bulkDisbursement, String msisdnSource) {
        String paymentUUID = UUID.randomUUID().toString();
        Map<Long, String> doneList = new Hashtable<>();
        log.info("Processing payment of bulk with id = [{}] and command ID {}", bulkDisbursement.getBulkId(), paymentUUID);
        try (Stream<PaymentRequest> allByBulkDisbursement_bulkId = paymentRequestRepo.findAllByBulkDisbursement_BulkIdAndPaymentStateIsNot(bulkDisbursement.getBulkId(), PaymentState.SUCCESS)) {
            allByBulkDisbursement_bulkId
                .parallel()
                .forEach(paymentRequest -> {
                    if (doneList.get(paymentRequest.getId()) == null) {
                        try {
                            doneList.putIfAbsent(paymentRequest.getId(), paymentUUID);
                            log.info("doing payment of payment request {} in the command with id : {}", paymentRequest, paymentUUID);
                            AdjustMmoneyAccountResponse adjustMmoneyAccountResponse = amClient.adjustMmoneyAccount(msisdnSource, pin, paymentRequest.getMsisdn(), paymentRequest.getAmount(), paymentUUID);
                            paymentRequest.setPaymentMessage(adjustMmoneyAccountResponse.getResponseHeader().getGeneralResponse().getDescription().getValue());
                            paymentRequest.setPaymentTxId(adjustMmoneyAccountResponse.getResponseBody().getTransactionId());
                            paymentRequest.setPaymentState(PaymentState.SUCCESS);
                            if (StringUtils.isNotBlank(paymentRequest.getSmsSender()) && StringUtils.isNotBlank(paymentRequest.getSms())) {
                                try {
                                    sendNotifClient.sendNotif(paymentRequest.getMsisdn(), paymentRequest.getSmsSender(), paymentRequest.getSms());
                                } catch (Exception e) {
                                    log.warn(e.getMessage());
                                }
                            }
                        } catch (SoapFaultClientException sfce) {
                            try {
                                String textContent = getDetail(sfce).getTextContent();
                                paymentRequest.setPaymentMessage(textContent);
                                paymentRequest.setPaymentState(PaymentState.FAILED);

                            } catch (TransformerException e) {
                                paymentRequest.setPaymentMessage(e.getMessage());
                                paymentRequest.setPaymentState(PaymentState.FAILED);
                            }
                        } finally {
                            this.paymentRequestRepo.save(paymentRequest);
                        }
                    }
                });
        }
        bulkDisbursement.setProcessingStatus(ProcessingStatus.PROCESSED);
        bulkDisbursement.setPaymentEnd(LocalDateTime.now().toInstant(ZoneOffset.UTC));
        bulkDisbursementRepository.save(bulkDisbursement);
    }

    private Element getDetail(SoapFaultClientException e) throws TransformerException {
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMResult result = new DOMResult();
        transformer.transform(e.getSoapFault().getSource(), result);
        NodeList nl = ((Document) result.getNode()).getElementsByTagNameNS("http://xmlns.tigo.com/ResponseHeader/V3", "description");
        return (Element) nl.item(0);
    }
}
