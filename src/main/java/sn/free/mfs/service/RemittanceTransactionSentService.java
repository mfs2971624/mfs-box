package sn.free.mfs.service;


import sn.free.mfs.service.dto.RemittanceTransactionSentDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link sn.free.mfs.domain.RemittanceTransaction}.
 */
public interface RemittanceTransactionSentService {


    /**
     * Get all the remittanceTransactions.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<RemittanceTransactionSentDTO> findAll(Pageable pageable);

    /**
     * Get the "id" remittanceTransaction.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<RemittanceTransactionSentDTO> findOne(Long id);


    List<RemittanceTransactionSentDTO> findBySenderIdNumberOrRecipientIdNumberOrSenderMsisdnOrRecipientMsisdnOrCodeMtoOrMobiquityTransactionIdOrMtoTransactionIdOrTransactionStatus(Pageable pageable, String senderIdNumber, String recipientIdNumber, String senderMsisdn, String recipientMsisdn, String codeMto, String mobiquityTransactionId, String mtoTransactionId, String transactionStatus);


}
