package sn.free.mfs.service;

/**
 * @project: mfs
 * @author: psow on 24/03/2020
 */
public interface IMobiquityService {
    /**
     * Check if pin is correct on mw backend
     * @param msisdn msisdn
     * @param pin pin
     * @return true if ok
     */
    boolean checkPin(String msisdn, String pin);
}
