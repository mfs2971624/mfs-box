package sn.free.mfs.service.impl;

import sn.free.mfs.service.RemittanceTransactionStatusService;
import sn.free.mfs.domain.RemittanceTransactionStatus;
import sn.free.mfs.repository.RemittanceTransactionStatusRepository;
import sn.free.mfs.service.dto.RemittanceTransactionStatusDTO;
import sn.free.mfs.service.mapper.RemittanceTransactionStatusMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Service Implementation for managing {@link RemittanceTransactionStatus}.
 */
@Service
@Transactional
public class RemittanceTransactionStatusServiceImpl implements RemittanceTransactionStatusService {

    private final Logger log = LoggerFactory.getLogger(RemittanceTransactionStatusServiceImpl.class);

    @Autowired
    private final RemittanceTransactionStatusRepository remittanceTransactionStatusRepository;

    private final RemittanceTransactionStatusMapper remittanceTransactionStatusMapper;

    public RemittanceTransactionStatusServiceImpl(RemittanceTransactionStatusRepository remittanceTransactionStatusRepository, RemittanceTransactionStatusMapper remittanceTransactionStatusMapper) {
        this.remittanceTransactionStatusRepository = remittanceTransactionStatusRepository;
        this.remittanceTransactionStatusMapper = remittanceTransactionStatusMapper;
    }

    /**
     * Save a remittanceTransactionStatus.
     *
     * @param remittanceTransactionStatusDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public RemittanceTransactionStatusDTO save(RemittanceTransactionStatusDTO remittanceTransactionStatusDTO) {
        log.debug("Request to save RemittanceTransactionStatus : {}", remittanceTransactionStatusDTO);
        RemittanceTransactionStatus remittanceTransactionStatus = remittanceTransactionStatusMapper.toEntity(remittanceTransactionStatusDTO);
        remittanceTransactionStatus = remittanceTransactionStatusRepository.save(remittanceTransactionStatus);
        return remittanceTransactionStatusMapper.toDto(remittanceTransactionStatus);
    }

    /**
     * Get all the remittanceTransactionStatuses.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<RemittanceTransactionStatusDTO> findAll(Pageable pageable) {
        log.debug("Request to get all RemittanceTransactionStatuses");
        //return remittanceTransactionStatusRepository.findAllRemittanceTransactionStatus(pageable)
        return remittanceTransactionStatusRepository.findAll(pageable)
            .map(remittanceTransactionStatusMapper::toDto);
    }

    /**
     * Get one remittanceTransactionStatus by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<RemittanceTransactionStatusDTO> findOne(Long id) {
        log.debug("Request to get RemittanceTransactionStatus : {}", id);
        return remittanceTransactionStatusRepository.findById(id)
            .map(remittanceTransactionStatusMapper::toDto);
    }

    /**
     * Delete the remittanceTransactionStatus by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete RemittanceTransactionStatus : {}", id);
        remittanceTransactionStatusRepository.deleteById(id);
    }
}
