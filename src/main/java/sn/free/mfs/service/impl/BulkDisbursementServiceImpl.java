package sn.free.mfs.service.impl;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sn.free.mfs.domain.BulkDisbursement;
import sn.free.mfs.domain.PaymentRequest;
import sn.free.mfs.domain.enumeration.ProcessingStatus;
import sn.free.mfs.repository.BulkDisbursementRepository;
import sn.free.mfs.repository.PaymentRequestRepository;
import sn.free.mfs.service.*;
import sn.free.mfs.service.dto.BulkDisbursementDTO;
import sn.free.mfs.service.mapper.BulkDisbursementMapper;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.StandardCopyOption;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Service Implementation for managing {@link BulkDisbursement}.
 */
@Service
@Transactional
public class BulkDisbursementServiceImpl implements BulkDisbursementService {

    private final Logger log = LoggerFactory.getLogger(BulkDisbursementServiceImpl.class);

    @Autowired
    private final BulkDisbursementRepository bulkDisbursementRepository;
    private final BulkDisbursementMapper bulkDisbursementMapper;
    private final PaymentRequestService paymentRequestService;
    @Autowired
    private final PaymentRequestRepository paymentRequestRepo;
    private final MWClient gbClient;
    private final MWClient amClient;
    private final OSBClient sendNotifClient;
    private final BulkDisbursementAsync bdAsync;

    public BulkDisbursementServiceImpl(BulkDisbursementRepository bulkDisbursementRepository, BulkDisbursementMapper bulkDisbursementMapper,
                                       PaymentRequestService paymentRequestService, PaymentRequestRepository paymentRequestRepo,
                                       @Qualifier("gbClient") MWClient gbClient, @Qualifier("amClient") MWClient amClient, OSBClient sendNotifClient, BulkDisbursementAsync bdAsync) {
        this.bulkDisbursementRepository = bulkDisbursementRepository;
        this.bulkDisbursementMapper = bulkDisbursementMapper;
        this.paymentRequestService = paymentRequestService;
        this.paymentRequestRepo = paymentRequestRepo;
        this.gbClient = gbClient;
        this.amClient = amClient;
        this.sendNotifClient = sendNotifClient;
        this.bdAsync = bdAsync;
    }

    /**
     * Save a bulkDisbursement.
     *
     * @param bulkDisbursementDTO the entity to save.
     * @param inputStream
     * @return the persisted entity.
     */
    @Override
    public BulkDisbursementDTO save(BulkDisbursementDTO bulkDisbursementDTO, InputStream inputStream) throws IOException {
        log.debug("Request to save BulkDisbursement : {}", bulkDisbursementDTO);
        bulkDisbursementDTO.setUploadStart(LocalDateTime.now().toInstant(ZoneOffset.UTC));
        BulkDisbursement bulkDisbursement = bulkDisbursementMapper.toEntity(bulkDisbursementDTO);
        bulkDisbursement = bulkDisbursementRepository.save(bulkDisbursement);
        //save stream to a file and read it as stream
        File targetFile = new File(bulkDisbursementDTO.getBulkId() + "LOAD.csv");
        java.nio.file.Files.copy(
            inputStream,
            targetFile.toPath(),
            StandardCopyOption.REPLACE_EXISTING);
        IOUtils.closeQuietly(inputStream);
        InputStream inputStream1 = new FileInputStream(targetFile);
        //end
        bdAsync.addPayments(inputStream1, bulkDisbursement);
        return bulkDisbursementMapper.toDto(bulkDisbursement);
    }


    /**
     * Get all the bulkDisbursements.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<BulkDisbursementDTO> findAll(Pageable pageable) {
        log.debug("Request to get all BulkDisbursements");
        return bulkDisbursementRepository.findAll(pageable)
            .map(bulkDisbursementMapper::toDto);
    }

    /**
     * Get one bulkDisbursement by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<BulkDisbursementDTO> findOne(Long id) {
        log.debug("Request to get BulkDisbursement : {}", id);
        return bulkDisbursementRepository.findById(id)
            .map(bulkDisbursementMapper::toDto);
    }

    /**
     * Delete the bulkDisbursement by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete BulkDisbursement : {}", id);
        bulkDisbursementRepository.deleteById(id);
    }

    @Override
    public BulkDisbursementDTO payment(Long id, String pin) {
        BulkDisbursement bulkDisbursement = this.bulkDisbursementRepository.findById(id).orElseThrow(IllegalAccessError::new);
        if (bulkDisbursement.getProcessingStatus() == ProcessingStatus.UPLOADING)
            throw new IllegalArgumentException("Chargement du fichier est en cours");
        if (bulkDisbursement.getProcessingStatus() != ProcessingStatus.INITIATED)
            throw new IllegalArgumentException("Payment déjà lancé");
        String msisdnSource = bulkDisbursement.getMsisdnSource();
        Double balance = gbClient.checkPin(msisdnSource, pin);
        if (balance == null) {
            bulkDisbursement.setDescription(bulkDisbursement.getDescription() + " --- PIN INCORRECT");
            this.bulkDisbursementRepository.save(bulkDisbursement);
            throw new IllegalArgumentException("PIN/MSISDN incorrect");
        }

        Double totalAmountForBulk = paymentRequestRepo.getTotalAmountForBulk(id);
        log.debug("total amount of bulk {} is  ===> {} XOF", bulkDisbursement.getBulkId(), totalAmountForBulk);
        log.debug("balance is  ===> {} XOF", balance);
        if (balance < totalAmountForBulk)
            throw new IllegalArgumentException("Balance insuffisante pour transfert total de : " + totalAmountForBulk);

        bulkDisbursement.setProcessingStatus(ProcessingStatus.VALIDATED);
        bulkDisbursement.setProcessingStatus(ProcessingStatus.PROCESSING);
        bulkDisbursement.setPaymentStart(LocalDateTime.now().toInstant(ZoneOffset.UTC));
        BulkDisbursement disbursement = bulkDisbursementRepository.save(bulkDisbursement);
        bdAsync.bulkPayments(pin, bulkDisbursement, msisdnSource);
        return bulkDisbursementMapper.toDto(disbursement);
    }

    @Override
    public List<PaymentRequest> findByBulkId(String bulkId) {
        return paymentRequestRepo.findAllByBulkDisbursement_BulkId(bulkId)
            .collect(Collectors.toList());
    }


}
