package sn.free.mfs.service.impl;

import sn.free.mfs.service.PaymentRequestService;
import sn.free.mfs.domain.PaymentRequest;
import sn.free.mfs.repository.PaymentRequestRepository;
import sn.free.mfs.service.dto.PaymentRequestDTO;
import sn.free.mfs.service.mapper.PaymentRequestMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.DecimalFormat;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Service Implementation for managing {@link PaymentRequest}.
 */
@Service
@Transactional
public class PaymentRequestServiceImpl implements PaymentRequestService {

    private final Logger log = LoggerFactory.getLogger(PaymentRequestServiceImpl.class);

    @Autowired
    private final PaymentRequestRepository paymentRequestRepository;

    private final PaymentRequestMapper paymentRequestMapper;

    private final DecimalFormat decimalFormatter = new DecimalFormat("#.0#");

    public PaymentRequestServiceImpl(PaymentRequestRepository paymentRequestRepository, PaymentRequestMapper paymentRequestMapper) {
        this.paymentRequestRepository = paymentRequestRepository;
        this.paymentRequestMapper = paymentRequestMapper;
    }

    /**
     * Save a paymentRequest.
     *
     * @param paymentRequestDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public PaymentRequestDTO save(PaymentRequestDTO paymentRequestDTO) {
        log.debug("Request to save PaymentRequest : {}", paymentRequestDTO);
        paymentRequestDTO.setSms(buildSMS(paymentRequestDTO.getSms(), paymentRequestDTO));
        PaymentRequest paymentRequest = paymentRequestMapper.toEntity(paymentRequestDTO);
        paymentRequest = paymentRequestRepository.save(paymentRequest);
        return paymentRequestMapper.toDto(paymentRequest);
    }

    private String buildSMS(String sms, PaymentRequestDTO paymentRequestDTO) {
        return sms.replace("${nom}", paymentRequestDTO.getFirstName())
            .replace("${prenom}", paymentRequestDTO.getLastName())
            .replace("${amount}", decimalFormatter.format(paymentRequestDTO.getAmount()));
    }

    /**
     * Get all the paymentRequests.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PaymentRequestDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PaymentRequests");
        return paymentRequestRepository.findAll(pageable)
            .map(paymentRequestMapper::toDto);
    }

    /**
     * Get one paymentRequest by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<PaymentRequestDTO> findOne(Long id) {
        log.debug("Request to get PaymentRequest : {}", id);
        return paymentRequestRepository.findById(id)
            .map(paymentRequestMapper::toDto);
    }

    /**
     * Delete the paymentRequest by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete PaymentRequest : {}", id);
        paymentRequestRepository.deleteById(id);
    }

    @Override
    public Page<PaymentRequestDTO> findByBulkId(String bulkId, Pageable pageable) {
        log.debug("find payements of {} bulk id", bulkId);
        return paymentRequestRepository.findAllByBulkDisbursement_BulkId(bulkId, pageable)
            .map(paymentRequestMapper::toDto);
    }
}
