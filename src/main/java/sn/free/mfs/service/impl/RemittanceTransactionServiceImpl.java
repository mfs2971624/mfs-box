package sn.free.mfs.service.impl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
import sn.free.mfs.service.RemittanceTransactionService;
import sn.free.mfs.domain.RemittanceTransaction;
import sn.free.mfs.repository.RemittanceTransactionRepository;
import sn.free.mfs.service.dto.OrderDTO;
import sn.free.mfs.service.dto.OrderDTOResponse;
import sn.free.mfs.service.dto.RemittanceTransactionDTO;
import sn.free.mfs.service.mapper.RemittanceTransactionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Service Implementation for managing {@link RemittanceTransaction}.
 */
@Service
@Transactional
public class RemittanceTransactionServiceImpl implements RemittanceTransactionService {

    private final Logger log = LoggerFactory.getLogger(RemittanceTransactionServiceImpl.class);

    @Autowired
    private final RemittanceTransactionRepository remittanceTransactionRepository;

    private final RemittanceTransactionMapper remittanceTransactionMapper;

    static RestTemplate restTemplate = new RestTemplate();

    @Value("${app.remittance.urlCancel}")
    static String baseUrl;

    public RemittanceTransactionServiceImpl(RemittanceTransactionRepository remittanceTransactionRepository, RemittanceTransactionMapper remittanceTransactionMapper) {
        this.remittanceTransactionRepository = remittanceTransactionRepository;
        this.remittanceTransactionMapper = remittanceTransactionMapper;
    }

    /**
     * Save a remittanceTransaction.
     *
     * @param remittanceTransactionDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public RemittanceTransactionDTO save(RemittanceTransactionDTO remittanceTransactionDTO) {
        log.debug("Request to save RemittanceTransaction : {}", remittanceTransactionDTO);
        RemittanceTransaction remittanceTransaction = remittanceTransactionMapper.toEntity(remittanceTransactionDTO);
        remittanceTransaction = remittanceTransactionRepository.save(remittanceTransaction);
        return remittanceTransactionMapper.toDto(remittanceTransaction);
    }

    /**
     * Get all the remittanceTransactions.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<RemittanceTransactionDTO> findAll(Pageable pageable) {
        log.debug("Request to get all RemittanceTransactions");
        return remittanceTransactionRepository.findAll(pageable)
            .map(remittanceTransactionMapper::toDto);
    }

    /**
     * Get one remittanceTransaction by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<RemittanceTransactionDTO> findOne(Long id) {
        log.debug("Request to get RemittanceTransaction : {}", id);
        return remittanceTransactionRepository.findById(id)
            .map(remittanceTransactionMapper::toDto);
    }

    /**
     * Delete the remittanceTransaction by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete RemittanceTransaction : {}", id);
        remittanceTransactionRepository.deleteById(id);
    }

    @Override
    public List<RemittanceTransactionDTO> findByMtoTypeOrStatusOrMsisdnOrExternalCodeOrExternalId(Pageable pageable, String mtoType, String status, String msisdn, String externalCode, String externalId) {
        List<RemittanceTransaction> remitanceList = new ArrayList<>();
        if (StringUtils.isBlank(msisdn) && StringUtils.isBlank(mtoType) && StringUtils.isBlank(status) && StringUtils.isBlank(externalCode) && StringUtils.isBlank(externalId)) {
            return this.findAll(Pageable.unpaged()).getContent();
        }
        if (StringUtils.isNotBlank(mtoType)) {
            remitanceList.addAll(remittanceTransactionRepository.findByMtoType(mtoType));
        }
        if (StringUtils.isNotBlank(msisdn)) {
            remitanceList.addAll(remittanceTransactionRepository.findByBeneficiaryMsisdn(msisdn));
        }
        if (StringUtils.isNotBlank(status)) {
            remitanceList.addAll(remittanceTransactionRepository.findByStatus(status));
        }
        if (StringUtils.isNotBlank(externalCode)) {
            remitanceList.addAll(remittanceTransactionRepository.findByExternalCode(externalCode));
        }
        if (StringUtils.isNotBlank(externalId)) {
            remitanceList.addAll(remittanceTransactionRepository.findByExternalId(externalId));
        }
        return remitanceList.stream()
            .distinct()
            .map(remittanceTransactionMapper::toDto)
            .collect(Collectors.toList());
    }

    @Override
    public OrderDTOResponse cancelRemittanceTransaction(OrderDTO orderDTO) {
        Date now = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        String date = sdf.format(now);
        Random rand = new Random();
        int nbrFourDigit = rand.nextInt(10000);
        //ignoring ssl certificate
        HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        });
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("ria-CallerCorrelationId", ""+date+""+nbrFourDigit);
        headers.set("ria-CallDateTimeLocal", ""+date);
        headers.set("ria-AgentId", "38783411");
        headers.set("Ocp-Apim-Subscription-Key", "ebf8dcf4c7bb4f668423344833c6c384");

        HttpEntity<OrderDTO> entity = new HttpEntity<OrderDTO>(orderDTO,headers);
        //OrderDTOResponse response = restTemplate.postForObject(baseUrl,entity, OrderDTOResponse.class);

        return restTemplate.postForObject(baseUrl,entity, OrderDTOResponse.class);
    }

}
