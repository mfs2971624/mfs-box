package sn.free.mfs.service.impl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
import sn.free.mfs.service.RemittanceTransactionSentService;
import sn.free.mfs.seconddomain.RemittanceTransactionSent;
import sn.free.mfs.sendrepository.SendRemittanceTransactionRepository;
import sn.free.mfs.service.dto.RemittanceTransactionSentDTO;
import sn.free.mfs.service.mapper.RemittanceTransactionSentMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Service Implementation for managing {@link RemittanceTransactionSent}.
 */
@Service
@Transactional
public class RemittanceTransactionSentServiceImpl implements RemittanceTransactionSentService {

    private final Logger log = LoggerFactory.getLogger(RemittanceTransactionSentServiceImpl.class);

    @Autowired
    private final SendRemittanceTransactionRepository sendRemittanceTransactionRepository;

    private final RemittanceTransactionSentMapper remittanceTransactionSentMapper;

    static RestTemplate restTemplate = new RestTemplate();

    @Value("${app.remittance.urlCancel}")
    static String baseUrl;

    public RemittanceTransactionSentServiceImpl(SendRemittanceTransactionRepository sendRemittanceTransactionRepository, RemittanceTransactionSentMapper remittanceTransactionSentMapper) {
        this.sendRemittanceTransactionRepository = sendRemittanceTransactionRepository;
        this.remittanceTransactionSentMapper = remittanceTransactionSentMapper;
    }


    /**
     * Get all the remittanceTransactions.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<RemittanceTransactionSentDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Send RemittanceTransactions");
        return sendRemittanceTransactionRepository.findAll(pageable)
            .map(remittanceTransactionSentMapper::toDto);
    }

    /**
     * Get one remittanceTransaction by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<RemittanceTransactionSentDTO> findOne(Long id) {
        log.debug("Request to get Send RemittanceTransaction : {}", id);
        return sendRemittanceTransactionRepository.findById(id)
            .map(remittanceTransactionSentMapper::toDto);
    }

    @Override
    public List<RemittanceTransactionSentDTO> findBySenderIdNumberOrRecipientIdNumberOrSenderMsisdnOrRecipientMsisdnOrCodeMtoOrMobiquityTransactionIdOrMtoTransactionIdOrTransactionStatus(Pageable pageable, String senderIdNumber, String recipientIdNumber, String senderMsisdn, String recipientMsisdn, String codeMto, String mobiquityTransactionId, String mtoTransactionId, String transactionStatus) {
        List<RemittanceTransactionSent> remitanceList = new ArrayList<>();
        if (StringUtils.isBlank(senderIdNumber) && StringUtils.isBlank(recipientIdNumber) && StringUtils.isBlank(senderMsisdn) && StringUtils.isBlank(recipientMsisdn) && StringUtils.isBlank(codeMto) && StringUtils.isBlank(mobiquityTransactionId) && StringUtils.isBlank(mtoTransactionId) && StringUtils.isBlank(transactionStatus)) {
            return this.findAll(Pageable.unpaged()).getContent();
        }
        if (StringUtils.isNotBlank(senderIdNumber)) {
            remitanceList.addAll(sendRemittanceTransactionRepository.findBySenderIdNumber(senderIdNumber));
        }
        if (StringUtils.isNotBlank(recipientIdNumber)) {
            remitanceList.addAll(sendRemittanceTransactionRepository.findByRecipientIdNumber(recipientIdNumber));
        }
        if (StringUtils.isNotBlank(senderMsisdn)) {
            remitanceList.addAll(sendRemittanceTransactionRepository.findBySenderMsisdn(senderMsisdn));
        }
        if (StringUtils.isNotBlank(recipientMsisdn)) {
            remitanceList.addAll(sendRemittanceTransactionRepository.findByRecipientMsisdn(recipientMsisdn));
        }
        if (StringUtils.isNotBlank(codeMto)) {
            remitanceList.addAll(sendRemittanceTransactionRepository.findByCodeMto(codeMto));
        }
        if (StringUtils.isNotBlank(mobiquityTransactionId)) {
            remitanceList.addAll(sendRemittanceTransactionRepository.findByMobiquityTransactionId(mobiquityTransactionId));
        }
        if (StringUtils.isNotBlank(mtoTransactionId)) {
            remitanceList.addAll(sendRemittanceTransactionRepository.findByMtoTransactionId(mtoTransactionId));
        }
        if (StringUtils.isNotBlank(transactionStatus)) {
            remitanceList.addAll(sendRemittanceTransactionRepository.findByTransactionStatus(transactionStatus));
        }
        return remitanceList.stream()
            .distinct()
            .map(remittanceTransactionSentMapper::toDto)
            .collect(Collectors.toList());
    }


}
