package sn.free.mfs.service.impl;

import sn.free.mfs.service.RemittanceMtoTypesService;
import sn.free.mfs.domain.RemittanceMtoTypes;
import sn.free.mfs.repository.RemittanceMtoTypesRepository;
import sn.free.mfs.service.dto.RemittanceMtoTypesDTO;
import sn.free.mfs.service.mapper.RemittanceMtoTypesMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * Service Implementation for managing {@link RemittanceMtoTypes}.
 */
@Service
@Transactional
public class RemittanceMtoTypesServiceImpl implements RemittanceMtoTypesService {

    private final Logger log = LoggerFactory.getLogger(RemittanceMtoTypesServiceImpl.class);

    @Autowired
    private final RemittanceMtoTypesRepository remittanceMtoTypesRepository;

    private final RemittanceMtoTypesMapper remittanceMtoTypesMapper;

    public RemittanceMtoTypesServiceImpl(RemittanceMtoTypesRepository remittanceMtoTypesRepository, RemittanceMtoTypesMapper remittanceMtoTypesMapper) {
        this.remittanceMtoTypesRepository = remittanceMtoTypesRepository;
        this.remittanceMtoTypesMapper = remittanceMtoTypesMapper;
    }

    /**
     * Save a remittanceMtoTypes.
     *
     * @param remittanceMtoTypesDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public RemittanceMtoTypesDTO save(RemittanceMtoTypesDTO remittanceMtoTypesDTO) {
        log.debug("Request to save RemittanceMtoTypes : {}", remittanceMtoTypesDTO);
        RemittanceMtoTypes remittanceMtoTypes = remittanceMtoTypesMapper.toEntity(remittanceMtoTypesDTO);
        remittanceMtoTypes = remittanceMtoTypesRepository.save(remittanceMtoTypes);
        return remittanceMtoTypesMapper.toDto(remittanceMtoTypes);
    }

    /**
     * Get all the remittanceMtoTypes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<RemittanceMtoTypesDTO> findAll(Pageable pageable) {
        log.debug("Request to get all RemittanceMtoTypes");
        return remittanceMtoTypesRepository.findAll(pageable)
            .map(remittanceMtoTypesMapper::toDto);
    }

    /**
     * Get one remittanceMtoTypes by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<RemittanceMtoTypesDTO> findOne(Long id) {
        log.debug("Request to get RemittanceMtoTypes : {}", id);
        return remittanceMtoTypesRepository.findById(id)
            .map(remittanceMtoTypesMapper::toDto);
    }

    /**
     * Delete the remittanceMtoTypes by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete RemittanceMtoTypes : {}", id);
        remittanceMtoTypesRepository.deleteById(id);
    }
}
