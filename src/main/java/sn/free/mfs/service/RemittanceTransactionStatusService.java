package sn.free.mfs.service;

import org.springframework.data.jpa.repository.Query;
import sn.free.mfs.service.dto.RemittanceTransactionStatusDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link sn.free.mfs.domain.RemittanceTransactionStatus}.
 */
public interface RemittanceTransactionStatusService {

    /**
     * Save a remittanceTransactionStatus.
     *
     * @param remittanceTransactionStatusDTO the entity to save.
     * @return the persisted entity.
     */
    RemittanceTransactionStatusDTO save(RemittanceTransactionStatusDTO remittanceTransactionStatusDTO);

    /**
     * Get all the remittanceTransactionStatuses.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<RemittanceTransactionStatusDTO> findAll(Pageable pageable);

    /**
     * Get the "id" remittanceTransactionStatus.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<RemittanceTransactionStatusDTO> findOne(Long id);

    /**
     * Delete the "id" remittanceTransactionStatus.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
