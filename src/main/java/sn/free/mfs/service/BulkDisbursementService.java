package sn.free.mfs.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import sn.free.mfs.domain.PaymentRequest;
import sn.free.mfs.service.dto.BulkDisbursementDTO;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link sn.free.mfs.domain.BulkDisbursement}.
 */
public interface BulkDisbursementService {

    /**
     * Save a bulkDisbursement.
     *
     * @param bulkDisbursementDTO the entity to save.
     * @param inputStream
     * @return the persisted entity.
     */
    BulkDisbursementDTO save(BulkDisbursementDTO bulkDisbursementDTO, InputStream inputStream) throws IOException;

    /**
     * Get all the bulkDisbursements.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<BulkDisbursementDTO> findAll(Pageable pageable);

    /**
     * Get the "id" bulkDisbursement.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<BulkDisbursementDTO> findOne(Long id);

    /**
     * Delete the "id" bulkDisbursement.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Do a payment of a pending request
     *
     * @param id
     * @param pin
     * @return
     */
    BulkDisbursementDTO payment(Long id, String pin);

    /**
     * Find all paymenet of a bulk file
     * @param bulkId bulk id
     * @return all payment
     */
    List<PaymentRequest> findByBulkId(String bulkId);
}
