package sn.free.mfs.seconddomain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A RemittanceTransactionSent.
 */
@Entity
@Table(name = "sir_remittance_transaction")
public class RemittanceTransactionSent implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "Id")
    private Long id;

    @Column(name = "mobiquity_transaction_id")
    private String mobiquityTransactionId;

    @Column(name = "mto_transaction_id")
    private String mtoTransactionId;

    @Column(name = "createdAt")
    private String createdAt;

    @Column(name = "updatedAt")
    private String updatedAt;

    @Column(name = "recipient_surname")
    private String recipientSurname;

    @Column(name = "recipient_lastname")
    private String recipientLastName;

    @Column(name = "recipient_prename")
    private String recipientPrename;

    @Column(name = "recipient_middle_name")
    private String recipientMiddleName;

    @Column(name = "recipient_postal_code")
    private String recipientPostalCode;

    @Column(name = "recipient_state")
    private String recipientState;

    @Column(name = "recipient_to_country")
    private String recipientToCountry;

    @Column(name = "recipient_msisdn")
    private String recipientMsisdn;
    
    @Column(name = "recipient_address")
    private String recipientAddress;

    @Column(name = "recipient_city")
    private String recipientCity;

    @Column(name = "recipient_date_of_birth")
    private String recipientDateOfBirth;

    @Column(name = "recipient_id_number")
    private String recipientIdNumber;

    @Column(name = "recipient_id_type")
    private String recipientIdType;

    @Column(name = "recipient_id_country")
    private String recipientIdCountry;

    @Column(name = "recipient_id_expiration_date")
    private String recipientIdExpirationDate;

    @Column(name = "recipient_email")
    private String recipientEmail;

    @Column(name = "sender_firstname")
    private String senderFirstName;

    @Column(name = "sender_lastname")
    private String senderLastName;

    @Column(name = "sender_middlename")
    private String senderMiddleName;

    @Column(name = "sender_address")
    private String senderAddress;

    @Column(name = "sender_city")
    private String senderCity;

    @Column(name = "sender_date_of_birth")
    private String senderDateOfBirth;

    @Column(name = "sender_id_number")
    private String senderIdNumber;

    @Column(name = "sender_id_type")
    private String senderIdType;

    @Column(name = "sender_id_country")
    private String senderIdCountry;

    @Column(name = "sender_email")
    private String senderEmail;

    @Column(name = "sender_from_country")
    private String senderFromCountry;

    @Column(name = "sender_msisdn")
    private String senderMsisdn;

    @Column(name = "sender_id_expiration_date")
    private String senderIdExpirationDate;

    @Column(name = "sender_surname")
    private String senderSurname;

    @Column(name = "sender_postal_code")
    private String senderPostalCode;

    @Column(name = "sent_amount")
    private Double sentAmount;

    @Column(name = "sent_currency_code")
    private String sentCurrencyCode;

    @Column(name = "sent_fee_amount")
    private Double sentFeeAmount;

    @Column(name = "sent_fee_currency_code")
    private String sentFeeCurrencyCode;

    @Column(name = "code_mto")
    private String codeMto;

    @Column(name = "transaction_status")
    private String transactionStatus;

    @Column(name = "transaction_status_message")
    private String transactionStatusMessage;

    @Column(name = "transaction_attempts_number")
    private String transactionAttemptsNumber;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMobiquityTransactionId() {
		return this.mobiquityTransactionId;
	}

	public void setMobiquityTransactionId(String mobiquityTransactionId) {
		this.mobiquityTransactionId = mobiquityTransactionId;
	}

	public String getMtoTransactionId() {
		return this.mtoTransactionId;
	}

	public void setMtoTransactionId(String mtoTransactionId) {
		this.mtoTransactionId = mtoTransactionId;
	}

	public String getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getRecipientSurname() {
		return this.recipientSurname;
	}

	public void setRecipientSurname(String recipientSurname) {
		this.recipientSurname = recipientSurname;
	}

	public String getRecipientLastName() {
		return this.recipientLastName;
	}

	public void setRecipientLastName(String recipientLastName) {
		this.recipientLastName = recipientLastName;
	}

	public String getRecipientPrename() {
		return this.recipientPrename;
	}

	public void setRecipientPrename(String recipientPrename) {
		this.recipientPrename = recipientPrename;
	}

	public String getRecipientMiddleName() {
		return this.recipientMiddleName;
	}

	public void setRecipientMiddleName(String recipientMiddleName) {
		this.recipientMiddleName = recipientMiddleName;
	}

	public String getRecipientPostalCode() {
		return this.recipientPostalCode;
	}

	public void setRecipientPostalCode(String recipientPostalCode) {
		this.recipientPostalCode = recipientPostalCode;
	}

	public String getRecipientState() {
		return this.recipientState;
	}

	public void setRecipientState(String recipientState) {
		this.recipientState = recipientState;
	}

	public String getRecipientToCountry() {
		return this.recipientToCountry;
	}

	public void setRecipientToCountry(String recipientToCountry) {
		this.recipientToCountry = recipientToCountry;
	}

	public String getRecipientMsisdn() {
		return this.recipientMsisdn;
	}

	public void setRecipientMsisdn(String recipientMsisdn) {
		this.recipientMsisdn = recipientMsisdn;
	}

	public String getRecipientAddress() {
		return this.recipientAddress;
	}

	public void setRecipientAddress(String recipientAddress) {
		this.recipientAddress = recipientAddress;
	}

	public String getRecipientCity() {
		return this.recipientCity;
	}

	public void setRecipientCity(String recipientCity) {
		this.recipientCity = recipientCity;
	}

	public String getRecipientDateOfBirth() {
		return this.recipientDateOfBirth;
	}

	public void setRecipientDateOfBirth(String recipientDateOfBirth) {
		this.recipientDateOfBirth = recipientDateOfBirth;
	}

	public String getRecipientIdNumber() {
		return this.recipientIdNumber;
	}

	public void setRecipientIdNumber(String recipientIdNumber) {
		this.recipientIdNumber = recipientIdNumber;
	}

	public String getRecipientIdType() {
		return this.recipientIdType;
	}

	public void setRecipientIdType(String recipientIdType) {
		this.recipientIdType = recipientIdType;
	}

	public String getRecipientIdCountry() {
		return this.recipientIdCountry;
	}

	public void setRecipientIdCountry(String recipientIdCountry) {
		this.recipientIdCountry = recipientIdCountry;
	}

	public String getRecipientIdExpirationDate() {
		return this.recipientIdExpirationDate;
	}

	public void setRecipientIdExpirationDate(String recipientIdExpirationDate) {
		this.recipientIdExpirationDate = recipientIdExpirationDate;
	}

	public String getRecipientEmail() {
		return this.recipientEmail;
	}

	public void setRecipientEmail(String recipientEmail) {
		this.recipientEmail = recipientEmail;
	}


	public String getSenderFirstName() {
		return this.senderFirstName;
	}

	public void setSenderFirstName(String senderFirstName) {
		this.senderFirstName = senderFirstName;
	}


	public String getSenderLastName() {
		return this.senderLastName;
	}

	public void setSenderLastName(String senderLastName) {
		this.senderLastName = senderLastName;
	}


	public String getSenderMiddleName() {
		return this.senderMiddleName;
	}

	public void setSenderMiddleName(String senderMiddleName) {
		this.senderMiddleName = senderMiddleName;
	}


	public String getSenderAddress() {
		return this.senderAddress;
	}

	public void setSenderAddress(String senderAddress) {
		this.senderAddress = senderAddress;
	}


	public String getSenderCity() {
		return this.senderCity;
	}

	public void setSenderCity(String senderCity) {
		this.senderCity = senderCity;
	}


	public String getSenderDateOfBirth() {
		return this.senderDateOfBirth;
	}

	public void setSenderDateOfBirth(String senderDateOfBirth) {
		this.senderDateOfBirth = senderDateOfBirth;
	}


	public String getSenderIdNumber() {
		return this.senderIdNumber;
	}

	public void setSenderIdNumber(String senderIdNumber) {
		this.senderIdNumber = senderIdNumber;
	}


	public String getSenderIdType() {
		return this.senderIdType;
	}

	public void setSenderIdType(String senderIdType) {
		this.senderIdType = senderIdType;
	}


	public String getSenderIdCountry() {
		return this.senderIdCountry;
	}

	public void setSenderIdCountry(String senderIdCountry) {
		this.senderIdCountry = senderIdCountry;
	}


	public String getSenderEmail() {
		return this.senderEmail;
	}

	public void setSenderEmail(String senderEmail) {
		this.senderEmail = senderEmail;
	}


	public String getSenderFromCountry() {
		return this.senderFromCountry;
	}

	public void setSenderFromCountry(String senderFromCountry) {
		this.senderFromCountry = senderFromCountry;
	}


	public String getSenderMsisdn() {
		return this.senderMsisdn;
	}

	public void setSenderMsisdn(String senderMsisdn) {
		this.senderMsisdn = senderMsisdn;
	}


	public String getSenderIdExpirationDate() {
		return this.senderIdExpirationDate;
	}

	public void setSenderIdExpirationDate(String senderIdExpirationDate) {
		this.senderIdExpirationDate = senderIdExpirationDate;
	}


	public String getSenderSurname() {
		return this.senderSurname;
	}

	public void setSenderSurname(String senderSurname) {
		this.senderSurname = senderSurname;
	}


	public String getSenderPostalCode() {
		return this.senderPostalCode;
	}

	public void setSenderPostalCode(String senderPostalCode) {
		this.senderPostalCode = senderPostalCode;
	}


	public Double getSentAmount() {
		return this.sentAmount;
	}

	public void setSentAmount(Double sentAmount) {
		this.sentAmount = sentAmount;
	}


	public String getSentCurrencyCode() {
		return this.sentCurrencyCode;
	}

	public void setSentCurrencyCode(String sentCurrencyCode) {
		this.sentCurrencyCode = sentCurrencyCode;
	}


	public Double getSentFeeAmount() {
		return this.sentFeeAmount;
	}

	public void setSentFeeAmount(Double sentFeeAmount) {
		this.sentFeeAmount = sentFeeAmount;
	}


	public String getSentFeeCurrencyCode() {
		return this.sentFeeCurrencyCode;
	}

	public void setSentFeeCurrencyCode(String sentFeeCurrencyCode) {
		this.sentFeeCurrencyCode = sentFeeCurrencyCode;
	}


	public String getCodeMto() {
		return this.codeMto;
	}

	public void setCodeMto(String codeMto) {
		this.codeMto = codeMto;
	}


	public String getTransactionStatus() {
		return this.transactionStatus;
	}

	public void setTransactionStatus(String transactionStatus) {
		this.transactionStatus = transactionStatus;
	}


	public String getTransactionStatusMessage() {
		return this.transactionStatusMessage;
	}

	public void setTransactionStatusMessage(String transactionStatusMessage) {
		this.transactionStatusMessage = transactionStatusMessage;
	}


	public String getTransactionAttemptsNumber() {
		return this.transactionAttemptsNumber;
	}

	public void setTransactionAttemptsNumber(String transactionAttemptsNumber) {
		this.transactionAttemptsNumber = transactionAttemptsNumber;
	}


    /*
    @ManyToOne
    @JsonIgnoreProperties("mto_type")
    @JoinColumn(name = "mto_type")
    private RemittanceMtoTypes mtoType;

    @ManyToOne
    @JsonIgnoreProperties("Status")
    @JoinColumn(name = "Status")
    private RemittanceTransactionStatus status;*/

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RemittanceTransactionSent)) {
            return false;
        }
        return id != null && id.equals(((RemittanceTransactionSent) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }


    @Override
    public String toString() {
        return "{" +
            " id='" + getId() + "'" +
            ", mobiquityTransactionId='" + getMobiquityTransactionId() + "'" +
            ", mtoTransactionId='" + getMtoTransactionId() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            ", recipientSurname='" + getRecipientSurname() + "'" +
            ", recipientLastName='" + getRecipientLastName() + "'" +
            ", recipientPrename='" + getRecipientPrename() + "'" +
            ", recipientMiddleName='" + getRecipientMiddleName() + "'" +
            ", recipientPostalCode='" + getRecipientPostalCode() + "'" +
            ", recipientState='" + getRecipientState() + "'" +
            ", recipientToCountry='" + getRecipientToCountry() + "'" +
            ", recipientMsisdn='" + getRecipientMsisdn() + "'" +
            ", recipientAddress='" + getRecipientAddress() + "'" +
            ", recipientCity='" + getRecipientCity() + "'" +
            ", recipientDateOfBirth='" + getRecipientDateOfBirth() + "'" +
            ", recipientIdNumber='" + getRecipientIdNumber() + "'" +
            ", recipientIdType='" + getRecipientIdType() + "'" +
            ", recipientIdCountry='" + getRecipientIdCountry() + "'" +
            ", recipientIdExpirationDate='" + getRecipientIdExpirationDate() + "'" +
            ", recipientEmail='" + getRecipientEmail() + "'" +
            ", senderFirstName='" + getSenderFirstName() + "'" +
            ", senderLastName='" + getSenderLastName() + "'" +
            ", senderMiddleName='" + getSenderMiddleName() + "'" +
            ", senderAddress='" + getSenderAddress() + "'" +
            ", senderCity='" + getSenderCity() + "'" +
            ", senderDateOfBirth='" + getSenderDateOfBirth() + "'" +
            ", senderIdNumber='" + getSenderIdNumber() + "'" +
            ", senderIdType='" + getSenderIdType() + "'" +
            ", senderIdCountry='" + getSenderIdCountry() + "'" +
            ", senderEmail='" + getSenderEmail() + "'" +
            ", senderFromCountry='" + getSenderFromCountry() + "'" +
            ", senderMsisdn='" + getSenderMsisdn() + "'" +
            ", senderIdExpirationDate='" + getSenderIdExpirationDate() + "'" +
            ", senderSurname='" + getSenderSurname() + "'" +
            ", senderPostalCode='" + getSenderPostalCode() + "'" +
            ", sentAmount='" + getSentAmount() + "'" +
            ", sentCurrencyCode='" + getSentCurrencyCode() + "'" +
            ", sentFeeAmount='" + getSentFeeAmount() + "'" +
            ", sentFeeCurrencyCode='" + getSentFeeCurrencyCode() + "'" +
            ", codeMto='" + getCodeMto() + "'" +
            ", transactionStatus='" + getTransactionStatus() + "'" +
            ", transactionStatusMessage='" + getTransactionStatusMessage() + "'" +
            ", transactionAttemptsNumber='" + getTransactionAttemptsNumber() + "'" +
            "}";
    }


}
