package sn.free.mfs.config;

import io.github.jhipster.config.JHipsterConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import javax.sql.DataSource;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import javax.persistence.EntityManagerFactory;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import java.util.Collections;
import org.springframework.orm.jpa.JpaTransactionManager;
import java.util.HashMap;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.core.env.Environment;


@Configuration
@EnableJpaRepositories( entityManagerFactoryRef = "secondEntityManagerFactory",
                        transactionManagerRef = "secondTransactionManager",
                        basePackages = "sn.free.mfs.sendrepository")
@EnableJpaAuditing(auditorAwareRef = "springSecurityAuditorAware")
@EnableTransactionManagement
public class SecondDatabaseConfiguration {

    private final Logger log = LoggerFactory.getLogger(SecondDatabaseConfiguration.class);

    @Autowired Environment env;

    @Bean
    public EntityManagerFactoryBuilder entityManagerFactoryBuilder() {
        return new EntityManagerFactoryBuilder(new HibernateJpaVendorAdapter(), new HashMap<>(), null);
    }

    @Bean(name = "secondDataSource")
    @ConfigurationProperties("spring.seconddatasource")
    public DataSource firstDataSource() {
        log.info("Configuring JDBC second datasource");
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(env.getProperty("spring.seconddatasource.driverClassName"));
        dataSource.setUrl(env.getProperty("spring.seconddatasource.url"));
        dataSource.setUsername(env.getProperty("spring.seconddatasource.username"));
        dataSource.setPassword(env.getProperty("spring.seconddatasource.password"));
        return dataSource;
    }

    @Bean(name = "secondEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean secondEntityManagerFactory(final EntityManagerFactoryBuilder builder,
                                                                             final @Qualifier("secondDataSource") DataSource dataSource) {
        return builder
                .dataSource(dataSource)
                .packages("sn.free.mfs.seconddomain")
                .persistenceUnit("INTERNATIONAL_REMITTANCE_ENVOI")
                .properties(Collections.singletonMap("hibernate.hbm2ddl.auto", "update"))
                .build();
    }

    @Bean(name = "secondTransactionManager")
    public PlatformTransactionManager secondTransactionManager(@Qualifier("secondEntityManagerFactory")
                                                               EntityManagerFactory secondEntityManagerFactory) {
        return new JpaTransactionManager(secondEntityManagerFactory);
    }

}
