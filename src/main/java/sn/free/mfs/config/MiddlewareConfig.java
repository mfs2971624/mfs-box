package sn.free.mfs.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import sn.free.mfs.service.MWClient;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;


/**
 * @project: mfs
 * @author: psow on 25/03/2020
 */
@Configuration
public class MiddlewareConfig    {

    @Value("${app.talend.host}")
    private String host;

    @Value("${app.talend.getbalance.endpoint}")
    private String getBalEndpoint;

    @Value("${app.talend.adjustmoney.endpoint}")
    private String amEndpoint;

    @Value("${app.talend.getbalance.context}")
    private String gbContext;

    @Value("${app.talend.adjustmoney.context}")
    private String amContext;

    private Jaxb2Marshaller buildMarshaller(String contextPath) {
        final Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath(contextPath);
        return marshaller;
    }

    private MWClient getMwClient(String gbContext, String getBalEndpoint) {
        //ignoring ssl certificate
        HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        });
        final MWClient mwClient = new MWClient();
        final Jaxb2Marshaller marshaller = this.buildMarshaller(gbContext);
        String uri = this.host + getBalEndpoint;
        mwClient.setDefaultUri(uri);
        mwClient.setMarshaller(marshaller);
        mwClient.setUnmarshaller(marshaller);
        return mwClient;
    }

    @Bean(name = "gbClient")
    public MWClient gbClient() {
        return getMwClient(gbContext, this.getBalEndpoint);
    }

    @Bean(name = "amClient")
    public MWClient amClient() {
        return getMwClient(amContext, this.amEndpoint);
    }
}
