package sn.free.mfs.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import sn.free.mfs.service.MWClient;
import sn.free.mfs.service.OSBClient;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;


/**
 * @project: mfs
 * @author: psow on 25/03/2020
 */
@Configuration
public class OSBConfig {

    @Value("${app.osb.host}")
    private String host;

    @Value("${app.osb.sendnotif.endpoint}")
    private String sendNotifEndpoint;

    @Value("${app.osb.sendnotif.context}")
    private String sendNotifContext;

    private Jaxb2Marshaller buildMarshaller(String contextPath) {
        final Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath(contextPath);
        return marshaller;
    }

    private OSBClient getOSBClient(String context, String endpoint) {
        //ignoring ssl certificate
        HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        });
        final OSBClient osbClient = new OSBClient();
        final Jaxb2Marshaller marshaller = this.buildMarshaller(context);
        String uri = this.host + endpoint;
        osbClient.setDefaultUri(uri);
        osbClient.setMarshaller(marshaller);
        osbClient.setUnmarshaller(marshaller);
        return osbClient;
    }

    @Bean(name = "sendNotifClient")
    public OSBClient sendNotifClient() {
        return getOSBClient(sendNotifContext, sendNotifEndpoint);
    }
}
