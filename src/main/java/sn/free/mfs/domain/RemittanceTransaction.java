package sn.free.mfs.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A RemittanceTransaction.
 */
@Entity
@Table(name = "remittance_transaction_vw")
public class RemittanceTransaction implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "Id")
    private Long id;

    @NotNull
    @Column(name = "external_id", nullable = false)
    private String externalId;

    @NotNull
    @Column(name = "external_code", nullable = false)
    private String externalCode;

    @Column(name = "beneficiary_firstname")
    private String beneficiaryFirstName;

    @Column(name = "beneficiary_lastname")
    private String beneficiaryLastName;

    @Column(name = "beneficiary_msisdn")
    private String beneficiaryMsisdn;

    @Column(name = "sender_firstname")
    private String senderFirstName;

    @Column(name = "sender_lastname")
    private String senderLastName;

    @Column(name = "source_country_iso_code")
    private String sourceCountryIsoCode;

    @Column(name = "sent_amount")
    private Double sentAmount;

    @Column(name = "sent_amount_currency")
    private String sentAmountCurrency;

    @Column(name = "destination_amount")
    private Double destinationAmount;

    @Column(name = "destination_currency")
    private String destinationAmountCurrency;

    @Column(name = "creation_date")
    private String creationDate;

    @Column(name = "status_message")
    private String statusMessage;

    @Column(name = "mto_type")
    private String mtoType;

    @Column(name = "Status")
    private String status;
    /*
    @ManyToOne
    @JsonIgnoreProperties("mto_type")
    @JoinColumn(name = "mto_type")
    private RemittanceMtoTypes mtoType;

    @ManyToOne
    @JsonIgnoreProperties("Status")
    @JoinColumn(name = "Status")
    private RemittanceTransactionStatus status;*/

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getExternalId() {
        return externalId;
    }

    public RemittanceTransaction externalId(String externalId) {
        this.externalId = externalId;
        return this;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public String getExternalCode() {
        return externalCode;
    }

    public RemittanceTransaction externalCode(String externalCode) {
        this.externalCode = externalCode;
        return this;
    }

    public void setExternalCode(String externalCode) {
        this.externalCode = externalCode;
    }

    public String getBeneficiaryFirstName() {
        return beneficiaryFirstName;
    }

    public RemittanceTransaction beneficiaryFirstName(String beneficiaryFirstName) {
        this.beneficiaryFirstName = beneficiaryFirstName;
        return this;
    }

    public void setBeneficiaryFirstName(String beneficiaryFirstName) {
        this.beneficiaryFirstName = beneficiaryFirstName;
    }

    public String getBeneficiaryLastName() {
        return beneficiaryLastName;
    }

    public RemittanceTransaction beneficiaryLastName(String beneficiaryLastName) {
        this.beneficiaryLastName = beneficiaryLastName;
        return this;
    }

    public void setBeneficiaryLastName(String beneficiaryLastName) {
        this.beneficiaryLastName = beneficiaryLastName;
    }

    public String getBeneficiaryMsisdn() {
        return beneficiaryMsisdn;
    }

    public RemittanceTransaction beneficiaryMsisdn(String beneficiaryMsisdn) {
        this.beneficiaryMsisdn = beneficiaryMsisdn;
        return this;
    }

    public void setBeneficiaryMsisdn(String beneficiaryMsisdn) {
        this.beneficiaryMsisdn = beneficiaryMsisdn;
    }

    public String getSenderFirstName() {
        return senderFirstName;
    }

    public RemittanceTransaction senderFirstName(String senderFirstName) {
        this.senderFirstName = senderFirstName;
        return this;
    }

    public void setSenderFirstName(String senderFirstName) {
        this.senderFirstName = senderFirstName;
    }

    public String getSenderLastName() {
        return senderLastName;
    }

    public RemittanceTransaction senderLastName(String senderLastName) {
        this.senderLastName = senderLastName;
        return this;
    }

    public void setSenderLastName(String senderLastName) {
        this.senderLastName = senderLastName;
    }

    public String getSourceCountryIsoCode() {
        return sourceCountryIsoCode;
    }

    public RemittanceTransaction sourceCountryIsoCode(String sourceCountryIsoCode) {
        this.sourceCountryIsoCode = sourceCountryIsoCode;
        return this;
    }

    public void setSourceCountryIsoCode(String sourceCountryIsoCode) {
        this.sourceCountryIsoCode = sourceCountryIsoCode;
    }

    public Double getSentAmount() {
        return sentAmount;
    }

    public RemittanceTransaction sentAmount(Double sentAmount) {
        this.sentAmount = sentAmount;
        return this;
    }

    public void setSentAmount(Double sentAmount) {
        this.sentAmount = sentAmount;
    }

    public String getSentAmountCurrency() {
        return sentAmountCurrency;
    }

    public RemittanceTransaction sentAmountCurrency(String sentAmountCurrency) {
        this.sentAmountCurrency = sentAmountCurrency;
        return this;
    }

    public void setSentAmountCurrency(String sentAmountCurrency) {
        this.sentAmountCurrency = sentAmountCurrency;
    }

    public Double getDestinationAmount() {
        return destinationAmount;
    }

    public RemittanceTransaction destinationAmount(Double destinationAmount) {
        this.destinationAmount = destinationAmount;
        return this;
    }

    public void setDestinationAmount(Double destinationAmount) {
        this.destinationAmount = destinationAmount;
    }

    public String getDestinationAmountCurrency() {
        return destinationAmountCurrency;
    }

    public RemittanceTransaction destinationAmountCurrency(String destinationAmountCurrency) {
        this.destinationAmountCurrency = destinationAmountCurrency;
        return this;
    }

    public void setDestinationAmountCurrency(String destinationAmountCurrency) {
        this.destinationAmountCurrency = destinationAmountCurrency;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public RemittanceTransaction creationDate(String creationDate) {
        this.creationDate = creationDate;
        return this;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public RemittanceTransaction statusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
        return this;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public String getMtoType() {
        return mtoType;
    }

    public RemittanceTransaction mtoType(String mtoType) {
        this.mtoType = mtoType;
        return this;
    }

    public void setMtoType(String mtoType) {
        this.mtoType = mtoType;
    }

    public String getStatus() {
        return status;
    }

    public RemittanceTransaction status(String status) {
        this.status = status;
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RemittanceTransaction)) {
            return false;
        }
        return id != null && id.equals(((RemittanceTransaction) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "RemittanceTransaction{" +
            "id=" + getId() +
            ", externalId=" + getExternalId() +
            ", externalCode='" + getExternalCode() + "'" +
            ", beneficiaryFirstName='" + getBeneficiaryFirstName() + "'" +
            ", beneficiaryLastName='" + getBeneficiaryLastName() + "'" +
            ", beneficiaryMsisdn='" + getBeneficiaryMsisdn() + "'" +
            ", senderFirstName='" + getSenderFirstName() + "'" +
            ", senderLastName='" + getSenderLastName() + "'" +
            ", sourceCountryIsoCode='" + getSourceCountryIsoCode() + "'" +
            ", sentAmount=" + getSentAmount() +
            ", sentAmountCurrency='" + getSentAmountCurrency() + "'" +
            ", destinationAmount=" + getDestinationAmount() +
            ", destinationAmountCurrency='" + getDestinationAmountCurrency() + "'" +
            ", creationDate='" + getCreationDate() + "'" +
            ", statusMessage='" + getStatusMessage() + "'" +
            "}";
    }
}
