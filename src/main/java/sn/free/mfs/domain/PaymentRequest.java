package sn.free.mfs.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import sn.free.mfs.domain.enumeration.PaymentState;

/**
 * A PaymentRequest.
 */
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "payment_request",
    indexes = {
        @Index(name = "payment_tx_id_idx", columnList = "payment_tx_id")
    })
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PaymentRequest extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "msisdn", nullable = false)
    private String msisdn;

    @NotNull
    @Column(name = "amount", nullable = false)
    private Double amount;

    @Column(name = "identification_no")
    private String identificationNo;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "comment")
    private String comment;

    @Column(name = "sms")
    private String sms;

    @Column(name = "sms_sender")
    private String smsSender;

    @Enumerated(EnumType.STRING)
    @Column(name = "payment_state")
    private PaymentState paymentState;

    @Column(name = "payment_tx_id")
    private String paymentTxId;

    @Column(name = "payment_message")
    private String paymentMessage;

    @Column(name = "redeem_status")
    private String redeemSt;

    @ManyToOne
    @JsonIgnoreProperties("paymentRequests")
    private BulkDisbursement bulkDisbursement;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
