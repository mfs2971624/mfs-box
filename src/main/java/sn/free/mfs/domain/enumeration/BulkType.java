package sn.free.mfs.domain.enumeration;

/**
 * The BulkType enumeration.
 */
public enum BulkType {
    BOURSE_ETUDIANT, FREE_STAFF, SALAIRE_DEALER
}
