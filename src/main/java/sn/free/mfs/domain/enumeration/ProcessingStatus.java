package sn.free.mfs.domain.enumeration;

/**
 * The ProcessingStatus enumeration.
 */
public enum ProcessingStatus {
    UPLOADING, INITIATED, VALIDATED, PROCESSING, PROCESSED
}
