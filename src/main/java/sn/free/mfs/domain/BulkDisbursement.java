package sn.free.mfs.domain;


import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;

import sn.free.mfs.domain.enumeration.BulkType;

import sn.free.mfs.domain.enumeration.ProcessingStatus;

/**
 * A BulkDisbursement.
 */
@Entity
@Table(name = "bulk_disbursement")
public class BulkDisbursement extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "bulk_id", nullable = false)
    private String bulkId;

    @NotNull
    @Column(name = "msisdn_source", nullable = false)
    private String msisdnSource;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "type", nullable = false)
    private BulkType type;

    @Column(name = "description")
    private String description;

    @Enumerated(EnumType.STRING)
    @Column(name = "processing_status")
    private ProcessingStatus processingStatus;

    @Column(name = "upload_start")
    private Instant uploadStart;

    @Column(name = "upload_end")
    private Instant uploadEnd;

    @Column(name = "payment_start")
    private Instant paymentStart;

    @Column(name = "payment_end")
    private Instant paymentEnd;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBulkId() {
        return bulkId;
    }

    public BulkDisbursement bulkId(String bulkId) {
        this.bulkId = bulkId;
        return this;
    }

    public void setBulkId(String bulkId) {
        this.bulkId = bulkId;
    }

    public String getMsisdnSource() {
        return msisdnSource;
    }

    public BulkDisbursement msisdnSource(String msisdnSource) {
        this.msisdnSource = msisdnSource;
        return this;
    }

    public void setMsisdnSource(String msisdnSource) {
        this.msisdnSource = msisdnSource;
    }

    public BulkType getType() {
        return type;
    }

    public BulkDisbursement type(BulkType type) {
        this.type = type;
        return this;
    }

    public void setType(BulkType type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public BulkDisbursement description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ProcessingStatus getProcessingStatus() {
        return processingStatus;
    }

    public BulkDisbursement processingStatus(ProcessingStatus processingStatus) {
        this.processingStatus = processingStatus;
        return this;
    }

    public void setProcessingStatus(ProcessingStatus processingStatus) {
        this.processingStatus = processingStatus;
    }

    public Instant getUploadStart() {
        return uploadStart;
    }

    public BulkDisbursement uploadStart(Instant uploadStart) {
        this.uploadStart = uploadStart;
        return this;
    }

    public void setUploadStart(Instant uploadStart) {
        this.uploadStart = uploadStart;
    }

    public Instant getUploadEnd() {
        return uploadEnd;
    }

    public BulkDisbursement uploadEnd(Instant uploadEnd) {
        this.uploadEnd = uploadEnd;
        return this;
    }

    public void setUploadEnd(Instant uploadEnd) {
        this.uploadEnd = uploadEnd;
    }

    public Instant getPaymentStart() {
        return paymentStart;
    }

    public BulkDisbursement paymentStart(Instant paymentStart) {
        this.paymentStart = paymentStart;
        return this;
    }

    public void setPaymentStart(Instant paymentStart) {
        this.paymentStart = paymentStart;
    }

    public Instant getPaymentEnd() {
        return paymentEnd;
    }

    public BulkDisbursement paymentEnd(Instant paymentEnd) {
        this.paymentEnd = paymentEnd;
        return this;
    }

    public void setPaymentEnd(Instant paymentEnd) {
        this.paymentEnd = paymentEnd;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BulkDisbursement)) {
            return false;
        }
        return id != null && id.equals(((BulkDisbursement) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "BulkDisbursement{" +
            "id=" + getId() +
            ", bulkId='" + getBulkId() + "'" +
            ", msisdnSource='" + getMsisdnSource() + "'" +
            ", type='" + getType() + "'" +
            ", description='" + getDescription() + "'" +
            ", processingStatus='" + getProcessingStatus() + "'" +
            ", uploadStart='" + getUploadStart() + "'" +
            ", uploadEnd='" + getUploadEnd() + "'" +
            ", paymentStart='" + getPaymentStart() + "'" +
            ", paymentEnd='" + getPaymentEnd() + "'" +
            "}";
    }
}
