package sn.free.mfs.domain;


import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A RemittanceTransactionStatus.
 */
@Entity
@Table(name = "remittance_transaction_status_vw")
public class RemittanceTransactionStatus implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "DomainID", nullable = false)
    private Integer domaineId;

    @NotNull
    @Column(name = "Type", nullable = false)
    private String type;

    @NotNull
    @Column(name = "Label", nullable = false)
    private String label;

    @Column(name = "Description")
    private String description;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getDomaineId() {
        return domaineId;
    }

    public RemittanceTransactionStatus domaineId(Integer domaineId) {
        this.domaineId = domaineId;
        return this;
    }

    public void setDomaineId(Integer domaineId) {
        this.domaineId = domaineId;
    }

    public String getType() {
        return type;
    }

    public RemittanceTransactionStatus type(String type) {
        this.type = type;
        return this;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLabel() {
        return label;
    }

    public RemittanceTransactionStatus label(String label) {
        this.label = label;
        return this;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getDescription() {
        return description;
    }

    public RemittanceTransactionStatus description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RemittanceTransactionStatus)) {
            return false;
        }
        return id != null && id.equals(((RemittanceTransactionStatus) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "RemittanceTransactionStatus{" +
            "id=" + getId() +
            ", domaineId=" + getDomaineId() +
            ", type='" + getType() + "'" +
            ", label='" + getLabel() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
