package sn.free.mfs.web.rest;

import sn.free.mfs.service.RemittanceMtoTypesService;
import sn.free.mfs.web.rest.errors.BadRequestAlertException;
import sn.free.mfs.service.dto.RemittanceMtoTypesDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link sn.free.mfs.domain.RemittanceMtoTypes}.
 */
@RestController
@RequestMapping("/api")
public class RemittanceMtoTypesResource {

    private final Logger log = LoggerFactory.getLogger(RemittanceMtoTypesResource.class);

    private static final String ENTITY_NAME = "remittanceMtoTypes";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RemittanceMtoTypesService remittanceMtoTypesService;

    public RemittanceMtoTypesResource(RemittanceMtoTypesService remittanceMtoTypesService) {
        this.remittanceMtoTypesService = remittanceMtoTypesService;
    }

    /**
     * {@code POST  /remittance-mto-types} : Create a new remittanceMtoTypes.
     *
     * @param remittanceMtoTypesDTO the remittanceMtoTypesDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new remittanceMtoTypesDTO, or with status {@code 400 (Bad Request)} if the remittanceMtoTypes has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/remittance-mto-types")
    public ResponseEntity<RemittanceMtoTypesDTO> createRemittanceMtoTypes(@Valid @RequestBody RemittanceMtoTypesDTO remittanceMtoTypesDTO) throws URISyntaxException {
        log.debug("REST request to save RemittanceMtoTypes : {}", remittanceMtoTypesDTO);
        if (remittanceMtoTypesDTO.getId() != null) {
            throw new BadRequestAlertException("A new remittanceMtoTypes cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RemittanceMtoTypesDTO result = remittanceMtoTypesService.save(remittanceMtoTypesDTO);
        return ResponseEntity.created(new URI("/api/remittance-mto-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /remittance-mto-types} : Updates an existing remittanceMtoTypes.
     *
     * @param remittanceMtoTypesDTO the remittanceMtoTypesDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated remittanceMtoTypesDTO,
     * or with status {@code 400 (Bad Request)} if the remittanceMtoTypesDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the remittanceMtoTypesDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/remittance-mto-types")
    public ResponseEntity<RemittanceMtoTypesDTO> updateRemittanceMtoTypes(@Valid @RequestBody RemittanceMtoTypesDTO remittanceMtoTypesDTO) throws URISyntaxException {
        log.debug("REST request to update RemittanceMtoTypes : {}", remittanceMtoTypesDTO);
        if (remittanceMtoTypesDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        RemittanceMtoTypesDTO result = remittanceMtoTypesService.save(remittanceMtoTypesDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, remittanceMtoTypesDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /remittance-mto-types} : get all the remittanceMtoTypes.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of remittanceMtoTypes in body.
     */
    @GetMapping("/remittance-mto-types")
    public ResponseEntity<List<RemittanceMtoTypesDTO>> getAllRemittanceMtoTypes(Pageable pageable) {
        log.debug("REST request to get a page of RemittanceMtoTypes");
        Page<RemittanceMtoTypesDTO> page = remittanceMtoTypesService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /remittance-mto-types/:id} : get the "id" remittanceMtoTypes.
     *
     * @param id the id of the remittanceMtoTypesDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the remittanceMtoTypesDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/remittance-mto-types/{id}")
    public ResponseEntity<RemittanceMtoTypesDTO> getRemittanceMtoTypes(@PathVariable Long id) {
        log.debug("REST request to get RemittanceMtoTypes : {}", id);
        Optional<RemittanceMtoTypesDTO> remittanceMtoTypesDTO = remittanceMtoTypesService.findOne(id);
        return ResponseUtil.wrapOrNotFound(remittanceMtoTypesDTO);
    }

    /**
     * {@code DELETE  /remittance-mto-types/:id} : delete the "id" remittanceMtoTypes.
     *
     * @param id the id of the remittanceMtoTypesDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/remittance-mto-types/{id}")
    public ResponseEntity<Void> deleteRemittanceMtoTypes(@PathVariable Long id) {
        log.debug("REST request to delete RemittanceMtoTypes : {}", id);
        remittanceMtoTypesService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
