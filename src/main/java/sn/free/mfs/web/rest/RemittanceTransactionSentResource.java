package sn.free.mfs.web.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import sn.free.mfs.service.RemittanceTransactionSentService;
import sn.free.mfs.web.rest.errors.BadRequestAlertException;
import sn.free.mfs.service.dto.RemittanceTransactionSentDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Random;

/**
 * REST controller for managing {@link sn.free.mfs.domain.RemittanceTransaction}.
 */
@RestController
@RequestMapping("/api")
public class RemittanceTransactionSentResource {

    private final Logger log = LoggerFactory.getLogger(RemittanceTransactionSentResource.class);

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RemittanceTransactionSentService remittanceTransactionSentService;

    public RemittanceTransactionSentResource(RemittanceTransactionSentService remittanceTransactionSentService) {
        this.remittanceTransactionSentService = remittanceTransactionSentService;
    }

    /**
     * {@code GET  /remittance-transactions} : get all the remittanceTransactions.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of remittanceTransactions in body.
     */
    @GetMapping("/remittance-transactions-sent")
    public ResponseEntity<List<RemittanceTransactionSentDTO>> getAllRemittanceTransactionsSent(Pageable pageable) {
        log.debug("REST request to get a page of RemittanceTransactionsSent");
        Page<RemittanceTransactionSentDTO> page = remittanceTransactionSentService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /remittance-transactions/:id} : get the "id" remittanceTransaction.
     *
     * @param id the id of the remittanceTransactionDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the remittanceTransactionDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/remittance-transactions-sent/{id}")
    public ResponseEntity<RemittanceTransactionSentDTO> getRemittanceTransactionSent(@PathVariable Long id) {
        log.debug("REST request to get RemittanceTransactionSent : {}", id);
        Optional<RemittanceTransactionSentDTO> remittanceTransactionSentDTO = remittanceTransactionSentService.findOne(id);
        return ResponseUtil.wrapOrNotFound(remittanceTransactionSentDTO);
    }


    @GetMapping("/remittance-transactions-sent/search")
    public ResponseEntity<List<RemittanceTransactionSentDTO>> getRemittanceTransactionSentBySenderIdNumberOrRecipientIdNumberOrSenderMsisdnOrRecipientMsisdnOrCodeMtoOrMobiquityTransactionIdOrMtoTransactionIdOrTransactionStatus(Pageable pageable,@RequestParam(required = false) String senderIdNumber,@RequestParam(required = false) String recipientIdNumber,@RequestParam(required = false) String senderMsisdn,@RequestParam(required = false) String recipientMsisdn,@RequestParam(required = false) String codeMto,@RequestParam(required = false) String mobiquityTransactionId,@RequestParam(required = false) String mtoTransactionId,@RequestParam(required = false) String transactionStatus) {
  
        log.debug("REST request to get RemittanceTransactionSent : {}", senderMsisdn + " or " + transactionStatus);
        // List<AccessList> accessList = accessListService.findByMsisdnOrCin(msisdn, cin);
        List<RemittanceTransactionSentDTO> all = remittanceTransactionSentService.findBySenderIdNumberOrRecipientIdNumberOrSenderMsisdnOrRecipientMsisdnOrCodeMtoOrMobiquityTransactionIdOrMtoTransactionIdOrTransactionStatus(pageable, senderIdNumber, recipientIdNumber, senderMsisdn, recipientMsisdn, codeMto, mobiquityTransactionId, mtoTransactionId, transactionStatus);
        return ResponseEntity.ok(all);
    }


}
