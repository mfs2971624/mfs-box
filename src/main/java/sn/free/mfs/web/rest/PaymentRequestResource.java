package sn.free.mfs.web.rest;

import org.springframework.data.domain.PageImpl;
import sn.free.mfs.service.PaymentRequestService;
import sn.free.mfs.web.rest.errors.BadRequestAlertException;
import sn.free.mfs.service.dto.PaymentRequestDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link sn.free.mfs.domain.PaymentRequest}.
 */
@RestController
@RequestMapping("/api")
public class PaymentRequestResource {

    private final Logger log = LoggerFactory.getLogger(PaymentRequestResource.class);

    private static final String ENTITY_NAME = "paymentRequest";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PaymentRequestService paymentRequestService;

    public PaymentRequestResource(PaymentRequestService paymentRequestService) {
        this.paymentRequestService = paymentRequestService;
    }

    /**
     * {@code POST  /payment-requests} : Create a new paymentRequest.
     *
     * @param paymentRequestDTO the paymentRequestDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new paymentRequestDTO, or with status {@code 400 (Bad Request)} if the paymentRequest has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/payment-requests")
    public ResponseEntity<PaymentRequestDTO> createPaymentRequest(@Valid @RequestBody PaymentRequestDTO paymentRequestDTO) throws URISyntaxException {
        log.debug("REST request to save PaymentRequest : {}", paymentRequestDTO);
        if (paymentRequestDTO.getId() != null) {
            throw new BadRequestAlertException("A new paymentRequest cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PaymentRequestDTO result = paymentRequestService.save(paymentRequestDTO);
        return ResponseEntity.created(new URI("/api/payment-requests/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /payment-requests} : Updates an existing paymentRequest.
     *
     * @param paymentRequestDTO the paymentRequestDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated paymentRequestDTO,
     * or with status {@code 400 (Bad Request)} if the paymentRequestDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the paymentRequestDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/payment-requests")
    public ResponseEntity<PaymentRequestDTO> updatePaymentRequest(@Valid @RequestBody PaymentRequestDTO paymentRequestDTO) throws URISyntaxException {
        log.debug("REST request to update PaymentRequest : {}", paymentRequestDTO);
        if (paymentRequestDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        PaymentRequestDTO result = paymentRequestService.save(paymentRequestDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, paymentRequestDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /payment-requests} : get all the paymentRequests.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of paymentRequests in body.
     */
    @GetMapping("/payment-requests")
    public ResponseEntity<List<PaymentRequestDTO>> getAllPaymentRequests(Pageable pageable) {
        log.debug("REST request to get a page of PaymentRequests");
        Page<PaymentRequestDTO> page = paymentRequestService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /payment-requests/:id} : get the "id" paymentRequest.
     *
     * @param id the id of the paymentRequestDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the paymentRequestDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/payment-requests/{id}")
    public ResponseEntity<PaymentRequestDTO> getPaymentRequest(@PathVariable Long id) {
        log.debug("REST request to get PaymentRequest : {}", id);
        Optional<PaymentRequestDTO> paymentRequestDTO = paymentRequestService.findOne(id);
        return ResponseUtil.wrapOrNotFound(paymentRequestDTO);
    }

    @GetMapping("/payment-requests/bulk/{bulkId}")
    public ResponseEntity<List<PaymentRequestDTO>> getPaymentRequestByBulk(@PathVariable String bulkId, Pageable pageable) {
        log.debug("REST request to get PaymentRequest by BulkId: {}", bulkId);
        Page<PaymentRequestDTO> page = paymentRequestService.findByBulkId(bulkId, pageable);
        if (page == null) page = new PageImpl<>(new ArrayList<>());
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code DELETE  /payment-requests/:id} : delete the "id" paymentRequest.
     *
     * @param id the id of the paymentRequestDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/payment-requests/{id}")
    public ResponseEntity<Void> deletePaymentRequest(@PathVariable Long id) {
        log.debug("REST request to delete PaymentRequest : {}", id);
        paymentRequestService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
