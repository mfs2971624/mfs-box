package sn.free.mfs.web.rest.vm;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @project: mfs
 * @author: psow on 23/03/2020
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PinVM {
    private String pin;
}
