package sn.free.mfs.web.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import sn.free.mfs.service.RemittanceTransactionService;
import sn.free.mfs.service.dto.OrderDTO;
import sn.free.mfs.service.dto.OrderDTOResponse;
import sn.free.mfs.web.rest.errors.BadRequestAlertException;
import sn.free.mfs.service.dto.RemittanceTransactionDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Random;

/**
 * REST controller for managing {@link sn.free.mfs.domain.RemittanceTransaction}.
 */
@RestController
@RequestMapping("/api")
public class RemittanceTransactionResource {

    private final Logger log = LoggerFactory.getLogger(RemittanceTransactionResource.class);

    private static final String ENTITY_NAME = "remittanceTransaction";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RemittanceTransactionService remittanceTransactionService;

    public RemittanceTransactionResource(RemittanceTransactionService remittanceTransactionService) {
        this.remittanceTransactionService = remittanceTransactionService;
    }

    /**
     * {@code POST  /remittance-transactions} : Create a new remittanceTransaction.
     *
     * @param remittanceTransactionDTO the remittanceTransactionDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new remittanceTransactionDTO, or with status {@code 400 (Bad Request)} if the remittanceTransaction has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/remittance-transactions")
    public ResponseEntity<RemittanceTransactionDTO> createRemittanceTransaction(@Valid @RequestBody RemittanceTransactionDTO remittanceTransactionDTO) throws URISyntaxException {
        log.debug("REST request to save RemittanceTransaction : {}", remittanceTransactionDTO);
        if (remittanceTransactionDTO.getId() != null) {
            throw new BadRequestAlertException("A new remittanceTransaction cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RemittanceTransactionDTO result = remittanceTransactionService.save(remittanceTransactionDTO);
        return ResponseEntity.created(new URI("/api/remittance-transactions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /remittance-transactions} : Updates an existing remittanceTransaction.
     *
     * @param remittanceTransactionDTO the remittanceTransactionDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated remittanceTransactionDTO,
     * or with status {@code 400 (Bad Request)} if the remittanceTransactionDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the remittanceTransactionDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/remittance-transactions")
    public ResponseEntity<RemittanceTransactionDTO> updateRemittanceTransaction(@Valid @RequestBody RemittanceTransactionDTO remittanceTransactionDTO) throws URISyntaxException {
        log.debug("REST request to update RemittanceTransaction : {}", remittanceTransactionDTO);
        if (remittanceTransactionDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        RemittanceTransactionDTO result = remittanceTransactionService.save(remittanceTransactionDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, remittanceTransactionDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /remittance-transactions} : get all the remittanceTransactions.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of remittanceTransactions in body.
     */
    @GetMapping("/remittance-transactions")
    public ResponseEntity<List<RemittanceTransactionDTO>> getAllRemittanceTransactions(Pageable pageable) {
        log.debug("REST request to get a page of RemittanceTransactions");
        Page<RemittanceTransactionDTO> page = remittanceTransactionService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /remittance-transactions/:id} : get the "id" remittanceTransaction.
     *
     * @param id the id of the remittanceTransactionDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the remittanceTransactionDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/remittance-transactions/{id}")
    public ResponseEntity<RemittanceTransactionDTO> getRemittanceTransaction(@PathVariable Long id) {
        log.debug("REST request to get RemittanceTransaction : {}", id);
        Optional<RemittanceTransactionDTO> remittanceTransactionDTO = remittanceTransactionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(remittanceTransactionDTO);
    }

    /**
     * {@code DELETE  /remittance-transactions/:id} : delete the "id" remittanceTransaction.
     *
     * @param id the id of the remittanceTransactionDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/remittance-transactions/{id}")
    public ResponseEntity<Void> deleteRemittanceTransaction(@PathVariable Long id) {
        log.debug("REST request to delete RemittanceTransaction : {}", id);
        remittanceTransactionService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

    @GetMapping("/remittance-transactions/search")
    public ResponseEntity<List<RemittanceTransactionDTO>> getRemittanceTransactionByMtoTypeOrStatusOrMsisdnOrExternalCodeOrExternalId(Pageable pageable, @RequestParam(required = false) String mtoType, @RequestParam(required = false) String status, @RequestParam(required = false) String msisdn, @RequestParam(required = false) String externalCode, @RequestParam(required = false) String externalId) {
        /*AccessStatus statusEnum = null;
        try {

            statusEnum = AccessStatus.valueOf(status);
        }catch (IllegalArgumentException | NullPointerException  iae){
            log.debug(iae.getMessage());
        }*/
        log.debug("REST request to get RemittanceTransaction : {}", msisdn + " or " + status);
        // List<AccessList> accessList = accessListService.findByMsisdnOrCin(msisdn, cin);
        List<RemittanceTransactionDTO> all = remittanceTransactionService.findByMtoTypeOrStatusOrMsisdnOrExternalCodeOrExternalId(pageable, mtoType, status, msisdn,externalCode,externalId);
        return ResponseEntity.ok(all);
    }

    @PostMapping("/remittance-transactions/cancel")
    public ResponseEntity<OrderDTOResponse> cancelRemittanceTransaction(@Valid @RequestBody OrderDTO orderDTO) {
        log.debug("REST request to cancel RemittanceTransaction : {}", orderDTO);
        OrderDTOResponse response = remittanceTransactionService.cancelRemittanceTransaction(orderDTO);
        log.debug("REST response after cancel RemittanceTransaction : {}", response);
        //return ResponseEntity.ok(response);
        return ResponseEntity.noContent().headers(HeaderUtil.createAlert(applicationName, "Annulation en cours...", "")).build();
    }

}
