package sn.free.mfs.web.rest;

import com.opencsv.CSVWriter;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.web.multipart.MultipartFile;
import sn.free.mfs.domain.PaymentRequest;
import sn.free.mfs.domain.enumeration.BulkType;
import sn.free.mfs.domain.enumeration.ProcessingStatus;
import sn.free.mfs.service.BulkDisbursementService;
import sn.free.mfs.web.rest.errors.BadRequestAlertException;
import sn.free.mfs.service.dto.BulkDisbursementDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sn.free.mfs.web.rest.vm.PinVM;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * REST controller for managing {@link sn.free.mfs.domain.BulkDisbursement}.
 */
@RestController
@RequestMapping("/api")
public class BulkDisbursementResource {

    private final Logger log = LoggerFactory.getLogger(BulkDisbursementResource.class);

    private static final String ENTITY_NAME = "bulkDisbursement";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BulkDisbursementService bulkDisbursementService;

    public BulkDisbursementResource(BulkDisbursementService bulkDisbursementService) {
        this.bulkDisbursementService = bulkDisbursementService;
    }

    /**
     * {@code POST  /bulk-disbursements} : Create a new bulkDisbursement by uploading file.
     *
     * @param bulkFile    the file containing payments
     * @param source      the msisdn fm account that will do the txn
     * @param type        the type of BD
     * @param description some comments
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new bulkDisbursementDTO, or with status {@code 400 (Bad Request)} if the bulkDisbursement has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/bulk-disbursements")
    public ResponseEntity<BulkDisbursementDTO> createBulkDisbursement(@RequestParam("bulkFile") MultipartFile bulkFile,
                                                                      @RequestParam String source,
                                                                      @RequestParam String type,
                                                                      @RequestParam String description) throws URISyntaxException, IOException {

        String bulkID = UUID.randomUUID().toString();
        BulkDisbursementDTO bulkDisbursementDTO = BulkDisbursementDTO.builder()
            .bulkId(bulkID)
            .msisdnSource(source)
            .type(BulkType.valueOf(type))
            .description(description)
            .processingStatus(ProcessingStatus.UPLOADING)
            .build();
        BulkDisbursementDTO result = bulkDisbursementService.save(bulkDisbursementDTO, bulkFile.getInputStream());

        return ResponseEntity.created(new URI("/api/bulk-disbursements/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /bulk-disbursements} : Updates an existing bulkDisbursement.
     *
     * @param bulkDisbursementDTO the bulkDisbursementDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated bulkDisbursementDTO,
     * or with status {@code 400 (Bad Request)} if the bulkDisbursementDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the bulkDisbursementDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/bulk-disbursements")
    public ResponseEntity<BulkDisbursementDTO> updateBulkDisbursement(@Valid @RequestBody BulkDisbursementDTO bulkDisbursementDTO) throws URISyntaxException, IOException {
        log.debug("REST request to update BulkDisbursement : {}", bulkDisbursementDTO);
        if (bulkDisbursementDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        BulkDisbursementDTO result = bulkDisbursementService.save(bulkDisbursementDTO, null);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, bulkDisbursementDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /bulk-disbursements} : get all the bulkDisbursements.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of bulkDisbursements in body.
     */
    @GetMapping("/bulk-disbursements")
    public ResponseEntity<List<BulkDisbursementDTO>> getAllBulkDisbursements(Pageable pageable) {
        log.debug("REST request to get a page of BulkDisbursements");
        Page<BulkDisbursementDTO> page = bulkDisbursementService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /bulk-disbursements/:id} : get the "id" bulkDisbursement.
     *
     * @param id the id of the bulkDisbursementDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the bulkDisbursementDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/bulk-disbursements/{id}")
    public ResponseEntity<BulkDisbursementDTO> getBulkDisbursement(@PathVariable Long id) {
        log.debug("REST request to get BulkDisbursement : {}", id);
        Optional<BulkDisbursementDTO> bulkDisbursementDTO = bulkDisbursementService.findOne(id);
        return ResponseUtil.wrapOrNotFound(bulkDisbursementDTO);
    }

    /**
     * {@code DELETE  /bulk-disbursements/:id} : delete the "id" bulkDisbursement.
     *
     * @param id the id of the bulkDisbursementDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/bulk-disbursements/{id}")
    public ResponseEntity<Void> deleteBulkDisbursement(@PathVariable Long id) {
        log.debug("REST request to delete BulkDisbursement : {}", id);
        bulkDisbursementService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

    @PostMapping("/bulk-disbursements/{id}/payment")
    public ResponseEntity<BulkDisbursementDTO> payment(@PathVariable Long id, @Valid @RequestBody PinVM pinVM) {
        log.debug("REST request to process Bulk payment : {}", id);
        BulkDisbursementDTO payment;
        try {
            payment = bulkDisbursementService.payment(id, pinVM.getPin());
        } catch (Exception e) {
            return ResponseEntity.badRequest()
                .headers(HeaderUtil.createFailureAlert(applicationName, false, "bulkdisbursement", null, "Error: " + e.getMessage()))
                .build();
        }
        return ResponseEntity.ok().headers(HeaderUtil.createAlert(applicationName, "Payment is ongoing", payment.getId().toString())).body(payment);
    }

    @GetMapping("/bulk-disbursements/{bulkId}/report")
    public void report(@PathVariable String bulkId, HttpServletResponse response) throws IOException {
        log.debug("REST request to dowload report for Bulk payment : {}", bulkId);
        String fileName = "bulk-disbusement-" + "1" + ".csv";

        response.setContentType("application/octet-stream");
        response.addHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fileName + "\"");

        bulkDisbursementService.findByBulkId(bulkId)
            .stream()
            .map(pr -> new String[]{pr.getId().toString(), pr.getMsisdn(), pr.getAmount().toString(), pr.getPaymentTxId(), pr.getPaymentMessage(), pr.getPaymentState().name()})
            .forEach(pr -> {
                try {
                    response.getOutputStream().write(toCsv(pr).getBytes());
                    response.getOutputStream().write("\n".getBytes());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        response.getOutputStream().flush();
        response.getOutputStream().close();


    }

    private String toCsv(String[] strings) {
        return String.join(";", strings);
    }
}
