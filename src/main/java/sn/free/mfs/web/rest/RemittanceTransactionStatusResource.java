package sn.free.mfs.web.rest;

import sn.free.mfs.service.RemittanceTransactionStatusService;
import sn.free.mfs.web.rest.errors.BadRequestAlertException;
import sn.free.mfs.service.dto.RemittanceTransactionStatusDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link sn.free.mfs.domain.RemittanceTransactionStatus}.
 */
@RestController
@RequestMapping("/api")
public class RemittanceTransactionStatusResource {

    private final Logger log = LoggerFactory.getLogger(RemittanceTransactionStatusResource.class);

    private static final String ENTITY_NAME = "remittanceTransactionStatus";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RemittanceTransactionStatusService remittanceTransactionStatusService;

    public RemittanceTransactionStatusResource(RemittanceTransactionStatusService remittanceTransactionStatusService) {
        this.remittanceTransactionStatusService = remittanceTransactionStatusService;
    }

    /**
     * {@code POST  /remittance-transaction-statuses} : Create a new remittanceTransactionStatus.
     *
     * @param remittanceTransactionStatusDTO the remittanceTransactionStatusDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new remittanceTransactionStatusDTO, or with status {@code 400 (Bad Request)} if the remittanceTransactionStatus has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/remittance-transaction-statuses")
    public ResponseEntity<RemittanceTransactionStatusDTO> createRemittanceTransactionStatus(@Valid @RequestBody RemittanceTransactionStatusDTO remittanceTransactionStatusDTO) throws URISyntaxException {
        log.debug("REST request to save RemittanceTransactionStatus : {}", remittanceTransactionStatusDTO);
        if (remittanceTransactionStatusDTO.getId() != null) {
            throw new BadRequestAlertException("A new remittanceTransactionStatus cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RemittanceTransactionStatusDTO result = remittanceTransactionStatusService.save(remittanceTransactionStatusDTO);
        return ResponseEntity.created(new URI("/api/remittance-transaction-statuses/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /remittance-transaction-statuses} : Updates an existing remittanceTransactionStatus.
     *
     * @param remittanceTransactionStatusDTO the remittanceTransactionStatusDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated remittanceTransactionStatusDTO,
     * or with status {@code 400 (Bad Request)} if the remittanceTransactionStatusDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the remittanceTransactionStatusDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/remittance-transaction-statuses")
    public ResponseEntity<RemittanceTransactionStatusDTO> updateRemittanceTransactionStatus(@Valid @RequestBody RemittanceTransactionStatusDTO remittanceTransactionStatusDTO) throws URISyntaxException {
        log.debug("REST request to update RemittanceTransactionStatus : {}", remittanceTransactionStatusDTO);
        if (remittanceTransactionStatusDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        RemittanceTransactionStatusDTO result = remittanceTransactionStatusService.save(remittanceTransactionStatusDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, remittanceTransactionStatusDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /remittance-transaction-statuses} : get all the remittanceTransactionStatuses.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of remittanceTransactionStatuses in body.
     */
    @GetMapping("/remittance-transaction-statuses")
    public ResponseEntity<List<RemittanceTransactionStatusDTO>> getAllRemittanceTransactionStatuses(Pageable pageable) {
        log.debug("REST request to get a page of RemittanceTransactionStatuses");
        Page<RemittanceTransactionStatusDTO> page = remittanceTransactionStatusService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /remittance-transaction-statuses/:id} : get the "id" remittanceTransactionStatus.
     *
     * @param id the id of the remittanceTransactionStatusDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the remittanceTransactionStatusDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/remittance-transaction-statuses/{id}")
    public ResponseEntity<RemittanceTransactionStatusDTO> getRemittanceTransactionStatus(@PathVariable Long id) {
        log.debug("REST request to get RemittanceTransactionStatus : {}", id);
        Optional<RemittanceTransactionStatusDTO> remittanceTransactionStatusDTO = remittanceTransactionStatusService.findOne(id);
        return ResponseUtil.wrapOrNotFound(remittanceTransactionStatusDTO);
    }

    /**
     * {@code DELETE  /remittance-transaction-statuses/:id} : delete the "id" remittanceTransactionStatus.
     *
     * @param id the id of the remittanceTransactionStatusDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/remittance-transaction-statuses/{id}")
    public ResponseEntity<Void> deleteRemittanceTransactionStatus(@PathVariable Long id) {
        log.debug("REST request to delete RemittanceTransactionStatus : {}", id);
        remittanceTransactionStatusService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
