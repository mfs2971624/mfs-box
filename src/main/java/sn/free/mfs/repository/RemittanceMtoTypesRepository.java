package sn.free.mfs.repository;

import sn.free.mfs.domain.RemittanceMtoTypes;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the RemittanceMtoTypes entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RemittanceMtoTypesRepository extends JpaRepository<RemittanceMtoTypes, Long> {

}
