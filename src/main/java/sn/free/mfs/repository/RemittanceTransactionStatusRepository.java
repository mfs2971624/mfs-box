package sn.free.mfs.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import sn.free.mfs.domain.RemittanceTransactionStatus;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import sn.free.mfs.service.dto.RemittanceTransactionStatusDTO;

/**
 * Spring Data  repository for the RemittanceTransactionStatus entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RemittanceTransactionStatusRepository extends JpaRepository<RemittanceTransactionStatus, Long> {

    //@Query(value = "From remittance_transaction_status_vw")
    //Page<RemittanceTransactionStatus> findAllRemittanceTransactionStatus(Pageable pageable);
}
