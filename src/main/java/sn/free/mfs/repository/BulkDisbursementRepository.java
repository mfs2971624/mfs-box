package sn.free.mfs.repository;

import sn.free.mfs.domain.BulkDisbursement;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the BulkDisbursement entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BulkDisbursementRepository extends JpaRepository<BulkDisbursement, Long> {

}
