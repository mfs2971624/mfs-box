package sn.free.mfs.repository;

import sn.free.mfs.domain.RemittanceTransaction;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the RemittanceTransaction entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RemittanceTransactionRepository extends JpaRepository<RemittanceTransaction, Long> {
    List<RemittanceTransaction> findByMtoType(String mtoType);
    List<RemittanceTransaction> findByStatus(String status);
    List<RemittanceTransaction> findByBeneficiaryMsisdn(String msisdn);
    List<RemittanceTransaction> findByExternalCode(String externalCode);
    List<RemittanceTransaction> findByExternalId(String externalId);
}
