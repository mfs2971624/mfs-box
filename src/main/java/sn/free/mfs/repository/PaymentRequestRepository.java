package sn.free.mfs.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import sn.free.mfs.domain.PaymentRequest;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import sn.free.mfs.domain.enumeration.PaymentState;

import java.util.stream.DoubleStream;
import java.util.stream.Stream;

/**
 * Spring Data  repository for the PaymentRequest entity.
 */
@Repository
public interface PaymentRequestRepository extends JpaRepository<PaymentRequest, Long> {

    Page<PaymentRequest> findAllByBulkDisbursement_BulkId(String bulkId, Pageable pageable);

    Stream<PaymentRequest> findAllByBulkDisbursement_BulkIdAndPaymentStateIsNot(String bulkId, PaymentState state);

    @Query(value = "SELECT sum(amount) from PaymentRequest where bulk_disbursement_id = ?1")
    Double getTotalAmountForBulk(Long bulkId);

    Stream<PaymentRequest> findAllByBulkDisbursement_BulkId(String bulkId);
}
