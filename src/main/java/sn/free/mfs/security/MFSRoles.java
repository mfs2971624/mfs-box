package sn.free.mfs.security;


import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @project: etms
 * @author: psow on 07/02/2020
 */

public enum MFSRoles {

    ADMIN("G_TERTIO_ADMIN", "ROLE_ADMIN"),
    MFS_ADMIN("G_MFS_ADMIN", "ROLE_MFS_ADMIN"),
    MFS_AGENT("G_MFS_AGENT", "ROLE_MFS_AGENT"),
    USER("G_MFSBOX_USER", "ROLE_USER"),
    INTERN_SYSTEM("INTERNAL", "ROLE_INTERNAL");

    public static final List<MFSRoles> allRoles;

    static {
        List<MFSRoles> roles = new ArrayList<>(Arrays.asList(MFSRoles.values()));
        allRoles = Collections.unmodifiableList(roles);
    }

    @Getter
    private final String groupAD;
    @Getter
    private final String role;
    @Getter
    private final GrantedAuthority authority;

    MFSRoles(String groupAD, String role) {
        this.groupAD = groupAD;
        this.role = role;
        this.authority = new SimpleGrantedAuthority(groupAD);
    }
}
