export interface IOrder {
  PCOrderNo?: string;
  SCOrderNo?: string;
  OrderStatus?: string;
  StatusDate?: string;
  StatusTime?: string;
  Reason?: string;
}

export class Order implements IOrder {
  constructor(
    public PCOrderNo?: string,
    public SCOrderNo?: string,
    public OrderStatus?: string,
    public StatusDate?: string,
    public StatusTime?: string,
    public Reason?: string
  ) {}
}
