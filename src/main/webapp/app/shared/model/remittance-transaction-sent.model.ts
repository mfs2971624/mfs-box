export interface IRemittanceTransactionSent {
  id?: number;
  mobiquityTransactionId?: string;
  mtoTransactionId?: string;
  createdAt?: string;
  updatedAt?: string;
  recipientSurname?: string;
  recipientLastName?: string;
  recipientPrename?: string;
  recipientMiddleName?: String;
  recipientPostalCode?: String;
  recipientState?: String;
  recipientToCountry?: String;
  recipientMsisdn?: String;
  recipientAddress?: String;
  recipientCity?: String;
  recipientDateOfBirth?: String;
  recipientIdNumber?: String;
  recipientIdType?: String;
  recipientIdCountry?: String;
  recipientExpirationDate?: String;
  recipientEmail?: String;
  senderFirstName?: String;
  senderLastName?: String;
  senderMiddleName?: String;
  senderAddress?: String;
  senderCity?: String;
  senderDateOfBirth?: String;
  senderIdNumber?: String;
  senderIdType?: String;
  senderIdCountry?: String;
  senderEmail?: String;
  senderFromCountry?: String;
  senderMsisdn?: String;
  senderIdExpirationDate?: String;
  senderSurname?: String;
  senderPostalCode?: String;
  sentAmount?: number;
  sentCurrencyCode?: String;
  sentFeeAmount?: number;
  sentFeeCurrencyCode?: String;
  codeMto?: String;
  transactionStatus?: String;
  transactionStatusMessage?: String;
  transactionAttemptsNumber?: String
}

export class RemittanceTransactionSent implements IRemittanceTransactionSent {
  constructor(
    public id?: number,
    public mobiquityTransactionId?: string,
    public mtoTransactionId?: string,
    public createdAt?: string,
    public updatedAt?: string,
    public recipientSurname?: string,
    public recipientLastName?: string,
    public recipientPrename?: string,
    public recipientMiddleName?: String,
    public recipientPostalCode?: String,
    public recipientState?: String,
    public recipientToCountry?: String,
    public recipientMsisdn?: String,
    public recipientAddress?: String,
    public recipientCity?: String,
    public recipientDateOfBirth?: String,
    public recipientIdNumber?: String,
    public recipientIdType?: String,
    public recipientIdCountry?: String,
    public recipientExpirationDate?: String,
    public recipientEmail?: String,
    public senderFirstName?: String,
    public senderLastName?: String,
    public senderMiddleName?: String,
    public senderAddress?: String,
    public senderCity?: String,
    public senderDateOfBirth?: String,
    public senderIdNumber?: String,
    public senderIdType?: String,
    public senderIdCountry?: String,
    public senderEmail?: String,
    public senderFromCountry?: String,
    public senderMsisdn?: String,
    public senderIdExpirationDate?: String,
    public senderSurname?: String,
    public senderPostalCode?: String,
    public sentAmount?: number,
    public sentCurrencyCode?: String,
    public sentFeeAmount?: number,
    public sentFeeCurrencyCode?: String,
    public codeMto?: String,
    public transactionStatus?: String,
    public transactionStatusMessage?: String,
    public transactionAttemptsNumber?: String
  ) {}
}

