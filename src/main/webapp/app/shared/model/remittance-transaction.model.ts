export interface IRemittanceTransaction {
  id?: number;
  externalId?: string;
  externalCode?: string;
  beneficiaryFirstName?: string;
  beneficiaryLastName?: string;
  beneficiaryMsisdn?: string;
  senderFirstName?: string;
  senderLastName?: string;
  sourceCountryIsoCode?: string;
  sentAmount?: number;
  sentAmountCurrency?: string;
  destinationAmount?: number;
  destinationAmountCurrency?: string;
  creationDate?: string;
  statusMessage?: string;
  mtoType?: string;
  status?: string;
}

export class RemittanceTransaction implements IRemittanceTransaction {
  constructor(
    public id?: number,
    public externalId?: string,
    public externalCode?: string,
    public beneficiaryFirstName?: string,
    public beneficiaryLastName?: string,
    public beneficiaryMsisdn?: string,
    public senderFirstName?: string,
    public senderLastName?: string,
    public sourceCountryIsoCode?: string,
    public sentAmount?: number,
    public sentAmountCurrency?: string,
    public destinationAmount?: number,
    public destinationAmountCurrency?: string,
    public creationDate?: string,
    public statusMessage?: string,
    public mtoType?: string,
    public status?: string
  ) {}
}
