export const enum PaymentState {
  PENDING,
  SUCCESS,
  FAILED
}
