export const enum ProcessingStatus {
  INITIATED,
  VALIDATED,
  PROCESSING,
  PROCESSED
}
