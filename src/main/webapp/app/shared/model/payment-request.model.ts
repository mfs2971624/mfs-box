import { PaymentState } from 'app/shared/model/enumerations/payment-state.model';

export interface IPaymentRequest {
  id?: number;
  msisdn?: string;
  amount?: string;
  identificationNo?: string;
  firstName?: string;
  lastName?: string;
  comment?: string;
  paymentState?: PaymentState;
  paymentTxId?: string;
  paymentMessage?: string;
  bulkDisbursementBulkId?: string;
  bulkDisbursementId?: number;
}

export class PaymentRequest implements IPaymentRequest {
  constructor(
    public id?: number,
    public msisdn?: string,
    public amount?: string,
    public identificationNo?: string,
    public firstName?: string,
    public lastName?: string,
    public comment?: string,
    public paymentState?: PaymentState,
    public paymentTxId?: string,
    public paymentMessage?: string,
    public bulkDisbursementBulkId?: string,
    public bulkDisbursementId?: number
  ) {}
}
