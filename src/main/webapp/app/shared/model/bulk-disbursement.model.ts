import { Moment } from 'moment';
import { ProcessingStatus } from 'app/shared/model/enumerations/processing-status.model';

export interface IBulkDisbursement {
  id?: number;
  bulkId?: string;
  msisdnSource?: string;
  type?: string;
  description?: string;
  processingStatus?: ProcessingStatus;
  uploadStart?: Moment;
  uploadEnd?: Moment;
  paymentStart?: Moment;
  paymentEnd?: Moment;
}

export class BulkDisbursement implements IBulkDisbursement {
  constructor(
    public id?: number,
    public bulkId?: string,
    public msisdnSource?: string,
    public type?: string,
    public description?: string,
    public processingStatus?: ProcessingStatus,
    public uploadStart?: Moment,
    public uploadEnd?: Moment,
    public paymentStart?: Moment,
    public paymentEnd?: Moment
  ) {}
}
