export interface IRemittanceTransactionStatus {
  id?: number;
  domaineId?: number;
  type?: string;
  label?: string;
  description?: string;
}

export class RemittanceTransactionStatus implements IRemittanceTransactionStatus {
  constructor(public id?: number, public domaineId?: number, public type?: string, public label?: string, public description?: string) {}
}
