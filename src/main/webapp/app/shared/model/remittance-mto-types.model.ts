export interface IRemittanceMtoTypes {
  id?: number;
  domaineId?: number;
  type?: string;
  label?: string;
  description?: string;
}

export class RemittanceMtoTypes implements IRemittanceMtoTypes {
  constructor(public id?: number, public domaineId?: number, public type?: string, public label?: string, public description?: string) {}
}
