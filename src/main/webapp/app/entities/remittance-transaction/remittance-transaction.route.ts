import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IRemittanceTransaction, RemittanceTransaction } from 'app/shared/model/remittance-transaction.model';
import { RemittanceTransactionService } from './remittance-transaction.service';
import { RemittanceTransactionComponent } from './remittance-transaction.component';
import { RemittanceTransactionDetailComponent } from './remittance-transaction-detail.component';
import { RemittanceTransactionUpdateComponent } from './remittance-transaction-update.component';

@Injectable({ providedIn: 'root' })
export class RemittanceTransactionResolve implements Resolve<IRemittanceTransaction> {
  constructor(private service: RemittanceTransactionService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IRemittanceTransaction> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((remittanceTransaction: HttpResponse<RemittanceTransaction>) => {
          if (remittanceTransaction.body) {
            return of(remittanceTransaction.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new RemittanceTransaction());
  }
}

export const remittanceTransactionRoute: Routes = [
  {
    path: '',
    component: RemittanceTransactionComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'RemittanceTransactions'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: RemittanceTransactionDetailComponent,
    resolve: {
      remittanceTransaction: RemittanceTransactionResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'RemittanceTransactions'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: RemittanceTransactionUpdateComponent,
    resolve: {
      remittanceTransaction: RemittanceTransactionResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'RemittanceTransactions'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: RemittanceTransactionUpdateComponent,
    resolve: {
      remittanceTransaction: RemittanceTransactionResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'RemittanceTransactions'
    },
    canActivate: [UserRouteAccessService]
  }
];
