import { HttpResponse } from '@angular/common/http';
import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IRemittanceTransaction } from 'app/shared/model/remittance-transaction.model';
import { IOrder, Order } from 'app/shared/model/order.model';
import { RemittanceTransactionService } from './remittance-transaction.service';
import { Observable } from 'rxjs';

@Component({
  templateUrl: './remittance-transaction-delete-dialog.component.html'
})
export class RemittanceTransactionDeleteDialogComponent {
  remittanceTransaction?: IRemittanceTransaction;

  remittanceTransactionsCancel?: IRemittanceTransaction[];

  comments?: string;

  isCancelling = false;

  constructor(
    protected remittanceTransactionService: RemittanceTransactionService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmCancel(): void {
    this.isCancelling = true;

    if (this.remittanceTransactionsCancel) {
      for (const item of this.remittanceTransactionsCancel) {
        // alert('Hello '+item.externalId);
        const instanceOrder = this.createFromForm(item.externalId);
        if (instanceOrder.OrderStatus !== undefined) {
          this.subscribeToSaveResponse(this.remittanceTransactionService.cancel(instanceOrder));
        }
      }
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IOrder>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isCancelling = false;
    this.activeModal.close();
  }

  protected onSaveError(): void {
    this.isCancelling = false;
  }

  private createFromForm(pco: any): IOrder {
    const date = new Date().toISOString().slice(0, 10);
    const dateok = date.replace('-', '');
    const stDate = dateok.replace('-', '');

    const time = new Date().toLocaleString().slice(12, 25);
    const timeok = time.replace(':', '');
    const stTime = timeok.replace(':', '');
    return {
      ...new Order(),
      PCOrderNo: pco || '',
      SCOrderNo: pco || '',
      OrderStatus: 'REJECTED' || '',
      StatusDate: stDate || '',
      StatusTime: stTime || '',
      Reason: 'Commentaires' || ''
    };
  }
}
