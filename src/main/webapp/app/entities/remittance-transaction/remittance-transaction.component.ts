/* eslint no-use-before-define: 0 */ // --> OFF
import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IRemittanceTransaction } from 'app/shared/model/remittance-transaction.model';
import { IRemittanceMtoTypes } from 'app/shared/model/remittance-mto-types.model';
import { IRemittanceTransactionStatus } from 'app/shared/model/remittance-transaction-status.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { RemittanceTransactionService } from './remittance-transaction.service';
import { RemittanceMtoTypesService } from '../remittance-mto-types/remittance-mto-types.service';
import { RemittanceTransactionStatusService } from '../remittance-transaction-status/remittance-transaction-status.service';
import { RemittanceTransactionDeleteDialogComponent } from './remittance-transaction-delete-dialog.component';

@Component({
  selector: 'jhi-remittance-transaction',
  templateUrl: './remittance-transaction.component.html'
})
export class RemittanceTransactionComponent implements OnInit, OnDestroy {
  remittanceTransactions?: IRemittanceTransaction[];

  remittanceTransactionsToCancel: IRemittanceTransaction[] = [];

  remittanceMtoTypes?: IRemittanceMtoTypes[];

  remittanceStatus?: IRemittanceTransactionStatus[];

  eventSubscriber?: Subscription;
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page!: number;
  predicate!: string;
  ascending!: boolean;
  ngbPaginationPage = 1;
  operateur?: string;
  status?: string;
  msisdn?: string;
  codeRefMto?: string;
  idOperateur?: string;
  etat?: string;
  selectionEmpty?: boolean = false;

  constructor(
    protected remittanceTransactionService: RemittanceTransactionService,
    protected remittanceMtoTypesService: RemittanceMtoTypesService,
    protected remittanceTransactionStatusService: RemittanceTransactionStatusService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadPage(page?: number): void {
    const pageToLoad: number = page || this.page;

    this.remittanceTransactionService
      .query({
        page: pageToLoad - 1,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe(
        (res: HttpResponse<IRemittanceTransaction[]>) => this.onSuccess(res.body, res.headers, pageToLoad),
        () => this.onError()
      );

    // Appel remittanceMtoTypesService

    this.remittanceMtoTypesService
      .query({
        page: pageToLoad - 1,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe(
        (res: HttpResponse<IRemittanceMtoTypes[]>) => this.onSuccessRMto(res.body, res.headers, pageToLoad),
        () => this.onError()
      );

    // Appel remittanceTransactionStatus

    this.remittanceTransactionStatusService
      .query({
        page: pageToLoad - 1,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe(
        (res: HttpResponse<IRemittanceTransactionStatus[]>) => this.onSuccessRTS(res.body, res.headers, pageToLoad),
        () => this.onError()
      );
  }

  loadPageSearch(): void {
    this.remittanceTransactionService
      .search({
        mtoType: this.operateur || '',
        status: this.status || '',
        msisdn: this.msisdn || '',
        externalCode: this.codeRefMto || '',
        externalId: this.idOperateur || ''
      })
      .subscribe(
        (res: HttpResponse<IRemittanceTransaction[]>) => this.onSuccess(res.body, res.headers, 1),
        () => this.onError()
      );
  }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(data => {
      this.page = data.pagingParams.page;
      this.ascending = data.pagingParams.ascending;
      this.predicate = data.pagingParams.predicate;
      this.ngbPaginationPage = data.pagingParams.page;
      this.loadPage();
      this.remittanceTransactionsToCancel = [];
    });
    this.registerChangeInRemittanceTransactions();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: any): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInRemittanceTransactions(): void {
    this.eventSubscriber = this.eventManager.subscribe('remittanceTransactionListModification', () => this.loadPage());
  }

  delete(remittanceTransaction: IRemittanceTransaction): void {
    const modalRef = this.modalService.open(RemittanceTransactionDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.remittanceTransaction = remittanceTransaction;
  }

  annuler(remittanceTransactionsCancel: IRemittanceTransaction[]): void {
    const modalRef = this.modalService.open(RemittanceTransactionDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.remittanceTransactionsCancel = remittanceTransactionsCancel;
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected onSuccess(data: IRemittanceTransaction[] | null, headers: HttpHeaders, page: number): void {
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.page = page;
    this.router.navigate(['/remittance-transaction'], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage,
        sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc')
      }
    });
    this.remittanceTransactions = data || [];
  }

  protected onSuccessRMto(data: IRemittanceMtoTypes[] | null, headers: HttpHeaders, page: number): void {
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.page = page;
    this.remittanceMtoTypes = data || [];
  }

  protected onSuccessRTS(data: IRemittanceTransactionStatus[] | null, headers: HttpHeaders, page: number): void {
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.page = page;
    this.remittanceStatus = data || [];
  }

  protected onError(): void {
    this.ngbPaginationPage = this.page;
  }

  //  isFailed(stat: string): boolean {
  //    const sp = stat.split(' - ');
  //    if (sp[1] === 'PAID' || sp[1] === 'CANCELED') {
  //      return true;
  //    }
  //    return false;
  //  }

  isFailed(stat: string): boolean {
    const sp = stat.split(' - ');
    if (this.equalsWithIgnoreCase(sp[1], 'PAID') || this.equalsWithIgnoreCase(sp[1], 'CANCELED')) {
      return true;
    }
    return false;
  }

  equalsWithIgnoreCase(a: string, b: string): boolean {
    return typeof a === 'string' && typeof b === 'string' ? a.localeCompare(b, undefined, { sensitivity: 'accent' }) === 0 : a === b;
  }

  onSelect(event: any, data: any): void {
    if (this.remittanceTransactionsToCancel) {
      if (event.target.checked) {
        this.selectionEmpty = true;
        this.remittanceTransactionsToCancel.push(data) || [];
      } else {
        const removeIndex = this.remittanceTransactionsToCancel.indexOf(data);
        if (removeIndex !== -1) this.remittanceTransactionsToCancel.splice(removeIndex, 1) || [];
      }
      if (this.remittanceTransactionsToCancel.length === 0) {
        this.selectionEmpty = false;
      }
    }
  }
}
