import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IRemittanceTransaction } from 'app/shared/model/remittance-transaction.model';
import { IOrder } from 'app/shared/model/order.model';

type EntityResponseType = HttpResponse<IRemittanceTransaction>;
type EntityArrayResponseType = HttpResponse<IRemittanceTransaction[]>;
type EntityResponseTypeOrder = HttpResponse<IOrder>;

@Injectable({ providedIn: 'root' })
export class RemittanceTransactionService {
  public resourceUrl = SERVER_API_URL + 'api/remittance-transactions';

  constructor(protected http: HttpClient) {}

  create(remittanceTransaction: IRemittanceTransaction): Observable<EntityResponseType> {
    return this.http.post<IRemittanceTransaction>(this.resourceUrl, remittanceTransaction, { observe: 'response' });
  }

  update(remittanceTransaction: IRemittanceTransaction): Observable<EntityResponseType> {
    return this.http.put<IRemittanceTransaction>(this.resourceUrl, remittanceTransaction, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IRemittanceTransaction>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IRemittanceTransaction[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IRemittanceTransaction[]>(`${this.resourceUrl}/search`, { params: options, observe: 'response' });
  }

  // cancel(req: any): Observable<EntityResponseTypeOrder> {
  //  const options = createRequestOption(req);
  //   return this.http.post<EntityResponseTypeOrder>(`${this.resourceUrl}/cancel`, { params: options, observe: 'response' });
  // }
  // cancel(order: IOrder): Observable<EntityResponseTypeOrder> {
  //  return this.http.post<IOrder>(this.resourceUrl, order, { observe: 'response' });
  // }

  cancel(order: IOrder): Observable<EntityResponseTypeOrder> {
    return this.http.post<IOrder>(`${this.resourceUrl}/cancel`, order, { observe: 'response' });
  }
}
