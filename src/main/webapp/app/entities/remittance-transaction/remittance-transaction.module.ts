import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MfsboxSharedModule } from 'app/shared/shared.module';
import { RemittanceTransactionComponent } from './remittance-transaction.component';
import { RemittanceTransactionDetailComponent } from './remittance-transaction-detail.component';
import { RemittanceTransactionUpdateComponent } from './remittance-transaction-update.component';
import { RemittanceTransactionDeleteDialogComponent } from './remittance-transaction-delete-dialog.component';
import { remittanceTransactionRoute } from './remittance-transaction.route';

@NgModule({
  imports: [MfsboxSharedModule, RouterModule.forChild(remittanceTransactionRoute)],
  declarations: [
    RemittanceTransactionComponent,
    RemittanceTransactionDetailComponent,
    RemittanceTransactionUpdateComponent,
    RemittanceTransactionDeleteDialogComponent
  ],
  entryComponents: [RemittanceTransactionDeleteDialogComponent]
})
export class MfsboxRemittanceTransactionModule {}
