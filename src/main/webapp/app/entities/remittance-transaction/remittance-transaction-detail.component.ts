import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IRemittanceTransaction } from 'app/shared/model/remittance-transaction.model';

@Component({
  selector: 'jhi-remittance-transaction-detail',
  templateUrl: './remittance-transaction-detail.component.html'
})
export class RemittanceTransactionDetailComponent implements OnInit {
  remittanceTransaction: IRemittanceTransaction | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ remittanceTransaction }) => (this.remittanceTransaction = remittanceTransaction));
  }

  previousState(): void {
    window.history.back();
  }
}
