import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IRemittanceTransaction, RemittanceTransaction } from 'app/shared/model/remittance-transaction.model';
import { RemittanceTransactionService } from './remittance-transaction.service';
import { IRemittanceMtoTypes } from 'app/shared/model/remittance-mto-types.model';
import { RemittanceMtoTypesService } from 'app/entities/remittance-mto-types/remittance-mto-types.service';
import { IRemittanceTransactionStatus } from 'app/shared/model/remittance-transaction-status.model';
import { RemittanceTransactionStatusService } from 'app/entities/remittance-transaction-status/remittance-transaction-status.service';

type SelectableEntity = IRemittanceMtoTypes | IRemittanceTransactionStatus;

@Component({
  selector: 'jhi-remittance-transaction-update',
  templateUrl: './remittance-transaction-update.component.html'
})
export class RemittanceTransactionUpdateComponent implements OnInit {
  isSaving = false;
  remittancemtotypes: IRemittanceMtoTypes[] = [];
  remittancetransactionstatuses: IRemittanceTransactionStatus[] = [];

  editForm = this.fb.group({
    id: [],
    externalId: [null, [Validators.required]],
    externalCode: [null, [Validators.required]],
    beneficiaryFirstName: [],
    beneficiaryLastName: [],
    beneficiaryMsisdn: [],
    senderFirstName: [],
    senderLastName: [],
    sourceCountryIsoCode: [],
    sentAmount: [],
    sentAmountCurrency: [],
    destinationAmount: [],
    destinationAmountCurrency: [],
    creationDate: [],
    statusMessage: [],
    mtoType: [],
    status: []
  });

  constructor(
    protected remittanceTransactionService: RemittanceTransactionService,
    protected remittanceMtoTypesService: RemittanceMtoTypesService,
    protected remittanceTransactionStatusService: RemittanceTransactionStatusService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ remittanceTransaction }) => {
      this.updateForm(remittanceTransaction);

      this.remittanceMtoTypesService
        .query()
        .subscribe((res: HttpResponse<IRemittanceMtoTypes[]>) => (this.remittancemtotypes = res.body || []));

      this.remittanceTransactionStatusService
        .query()
        .subscribe((res: HttpResponse<IRemittanceTransactionStatus[]>) => (this.remittancetransactionstatuses = res.body || []));
    });
  }

  updateForm(remittanceTransaction: IRemittanceTransaction): void {
    this.editForm.patchValue({
      id: remittanceTransaction.id,
      externalId: remittanceTransaction.externalId,
      externalCode: remittanceTransaction.externalCode,
      beneficiaryFirstName: remittanceTransaction.beneficiaryFirstName,
      beneficiaryLastName: remittanceTransaction.beneficiaryLastName,
      beneficiaryMsisdn: remittanceTransaction.beneficiaryMsisdn,
      senderFirstName: remittanceTransaction.senderFirstName,
      senderLastName: remittanceTransaction.senderLastName,
      sourceCountryIsoCode: remittanceTransaction.sourceCountryIsoCode,
      sentAmount: remittanceTransaction.sentAmount,
      sentAmountCurrency: remittanceTransaction.sentAmountCurrency,
      destinationAmount: remittanceTransaction.destinationAmount,
      destinationAmountCurrency: remittanceTransaction.destinationAmountCurrency,
      creationDate: remittanceTransaction.creationDate,
      statusMessage: remittanceTransaction.statusMessage,
      mtoType: remittanceTransaction.mtoType,
      status: remittanceTransaction.status
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const remittanceTransaction = this.createFromForm();
    if (remittanceTransaction.id !== undefined) {
      this.subscribeToSaveResponse(this.remittanceTransactionService.update(remittanceTransaction));
    } else {
      this.subscribeToSaveResponse(this.remittanceTransactionService.create(remittanceTransaction));
    }
  }

  private createFromForm(): IRemittanceTransaction {
    return {
      ...new RemittanceTransaction(),
      id: this.editForm.get(['id'])!.value,
      externalId: this.editForm.get(['externalId'])!.value,
      externalCode: this.editForm.get(['externalCode'])!.value,
      beneficiaryFirstName: this.editForm.get(['beneficiaryFirstName'])!.value,
      beneficiaryLastName: this.editForm.get(['beneficiaryLastName'])!.value,
      beneficiaryMsisdn: this.editForm.get(['beneficiaryMsisdn'])!.value,
      senderFirstName: this.editForm.get(['senderFirstName'])!.value,
      senderLastName: this.editForm.get(['senderLastName'])!.value,
      sourceCountryIsoCode: this.editForm.get(['sourceCountryIsoCode'])!.value,
      sentAmount: this.editForm.get(['sentAmount'])!.value,
      sentAmountCurrency: this.editForm.get(['sentAmountCurrency'])!.value,
      destinationAmount: this.editForm.get(['destinationAmount'])!.value,
      destinationAmountCurrency: this.editForm.get(['destinationAmountCurrency'])!.value,
      creationDate: this.editForm.get(['creationDate'])!.value,
      statusMessage: this.editForm.get(['statusMessage'])!.value,
      mtoType: this.editForm.get(['mtoType'])!.value,
      status: this.editForm.get(['status'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IRemittanceTransaction>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
