import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'bulk-disbursement',
        loadChildren: () => import('./bulk-disbursement/bulk-disbursement.module').then(m => m.MfsboxBulkDisbursementModule)
      },
      {
        path: 'payment-request',
        loadChildren: () => import('./payment-request/payment-request.module').then(m => m.MfsboxPaymentRequestModule)
      },
      {
        path: 'remittance-transaction',
        loadChildren: () => import('./remittance-transaction/remittance-transaction.module').then(m => m.MfsboxRemittanceTransactionModule)
      },
      {
        path: 'remittance-transaction-sent',
        loadChildren: () =>
          import('./remittance-transaction-sent/remittance-transaction-sent.module').then(m => m.MfsboxRemittanceTransactionSentModule)
      },
      {
        path: 'remittance-mto-types',
        loadChildren: () => import('./remittance-mto-types/remittance-mto-types.module').then(m => m.MfsboxRemittanceMtoTypesModule)
      },
      {
        path: 'remittance-transaction-status',
        loadChildren: () =>
          import('./remittance-transaction-status/remittance-transaction-status.module').then(
            m => m.MfsboxRemittanceTransactionStatusModule
          )
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ]
})
export class MfsboxEntityModule {}
