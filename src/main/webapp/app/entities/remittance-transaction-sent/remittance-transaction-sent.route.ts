import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IRemittanceTransactionSent, RemittanceTransactionSent } from 'app/shared/model/remittance-transaction-sent.model';
import { RemittanceTransactionSentService } from './remittance-transaction-sent.service';
import { RemittanceTransactionSentComponent } from './remittance-transaction-sent.component';
import { RemittanceTransactionSentDetailComponent } from './remittance-transaction-sent-detail.component';

@Injectable({ providedIn: 'root' })
export class RemittanceTransactionSentResolve implements Resolve<IRemittanceTransactionSent> {
  constructor(private service: RemittanceTransactionSentService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IRemittanceTransactionSent> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((remittanceTransactionSent: HttpResponse<RemittanceTransactionSent>) => {
          if (remittanceTransactionSent.body) {
            return of(remittanceTransactionSent.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new RemittanceTransactionSent());
  }
}

export const remittanceTransactionSentRoute: Routes = [
  {
    path: '',
    component: RemittanceTransactionSentComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'RemittanceTransactionsSent'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: RemittanceTransactionSentDetailComponent,
    resolve: {
      remittanceTransactionSent: RemittanceTransactionSentResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'RemittanceTransactionsSent'
    },
    canActivate: [UserRouteAccessService]
  }
];
