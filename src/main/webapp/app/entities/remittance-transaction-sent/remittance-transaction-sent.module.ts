import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MfsboxSharedModule } from 'app/shared/shared.module';
import { RemittanceTransactionSentComponent } from './remittance-transaction-sent.component';
import { RemittanceTransactionSentDetailComponent } from './remittance-transaction-sent-detail.component';
import { remittanceTransactionSentRoute } from './remittance-transaction-sent.route';

@NgModule({
  imports: [MfsboxSharedModule, RouterModule.forChild(remittanceTransactionSentRoute)],
  declarations: [RemittanceTransactionSentComponent, RemittanceTransactionSentDetailComponent],
  entryComponents: []
})
export class MfsboxRemittanceTransactionSentModule {}
