/* eslint no-use-before-define: 0 */ // --> OFF
import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IRemittanceTransactionSent } from 'app/shared/model/remittance-transaction-sent.model';
import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { RemittanceTransactionSentService } from './remittance-transaction-sent.service';


@Component({
  selector: 'jhi-remittance-transaction-sent',
  templateUrl: './remittance-transaction-sent.component.html'
})
export class RemittanceTransactionSentComponent implements OnInit, OnDestroy {
  remittanceTransactions?: IRemittanceTransactionSent[];


  eventSubscriber?: Subscription;
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page!: number;
  predicate!: string;
  ascending!: boolean;
  ngbPaginationPage = 1;
  operateur?: string;
  status?: string;
  msisdn?: string;
  codeRefMto?: string;
  idOperateur?: string;
  etat?: string;
  selectionEmpty?: boolean = false;

  constructor(
    protected remittanceTransactionSentService: RemittanceTransactionSentService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadPage(page?: number): void {
    const pageToLoad: number = page || this.page;

    this.remittanceTransactionSentService
      .query({
        page: pageToLoad - 1,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe(
        (res: HttpResponse<IRemittanceTransactionSent[]>) => this.onSuccess(res.body, res.headers, pageToLoad),
        () => this.onError()
      );
  }

  loadPageSearch(): void {
    this.remittanceTransactionSentService
      .search({
        msisdn: this.msisdn || ''
      })
      .subscribe(
        (res: HttpResponse<IRemittanceTransactionSent[]>) => this.onSuccess(res.body, res.headers, 1),
        () => this.onError()
      );
  }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(data => {
      this.page = data.pagingParams.page;
      this.ascending = data.pagingParams.ascending;
      this.predicate = data.pagingParams.predicate;
      this.ngbPaginationPage = data.pagingParams.page;
      this.loadPage();
    });
    this.registerChangeInRemittanceTransactions();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: any): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInRemittanceTransactions(): void {
    this.eventSubscriber = this.eventManager.subscribe('remittanceTransactionListModification', () => this.loadPage());
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected onSuccess(data: IRemittanceTransactionSent[] | null, headers: HttpHeaders, page: number): void {
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.page = page;
    this.router.navigate(['/remittance-transaction-sent'], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage,
        sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc')
      }
    });
    this.remittanceTransactions = data || [];
  }

  protected onError(): void {
    this.ngbPaginationPage = this.page;
  }

  exportAsXLSX():void {
    this.remittanceTransactionSentService.exportAsExcelFile(this.remittanceTransactions || [] , 'data');
 }


  equalsWithIgnoreCase(a: string, b: string): boolean {
    return typeof a === 'string' && typeof b === 'string' ? a.localeCompare(b, undefined, { sensitivity: 'accent' }) === 0 : a === b;
  }

}
