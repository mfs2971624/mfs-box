import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IRemittanceTransactionSent } from 'app/shared/model/remittance-transaction-sent.model';

@Component({
  selector: 'jhi-remittance-transaction-sent-detail',
  templateUrl: './remittance-transaction-sent-detail.component.html'
})
export class RemittanceTransactionSentDetailComponent implements OnInit {
  remittanceTransactionSent: IRemittanceTransactionSent | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ remittanceTransactionSent }) => (this.remittanceTransactionSent = remittanceTransactionSent));
    // eslint-disable-next-line no-console
    console.log(this.remittanceTransactionSent);
  }

  previousState(): void {
    window.history.back();
  }
}
