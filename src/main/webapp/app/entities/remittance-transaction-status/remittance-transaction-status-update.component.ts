import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IRemittanceTransactionStatus, RemittanceTransactionStatus } from 'app/shared/model/remittance-transaction-status.model';
import { RemittanceTransactionStatusService } from './remittance-transaction-status.service';

@Component({
  selector: 'jhi-remittance-transaction-status-update',
  templateUrl: './remittance-transaction-status-update.component.html'
})
export class RemittanceTransactionStatusUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    domaineId: [null, [Validators.required]],
    type: [null, [Validators.required]],
    label: [null, [Validators.required]],
    description: []
  });

  constructor(
    protected remittanceTransactionStatusService: RemittanceTransactionStatusService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ remittanceTransactionStatus }) => {
      this.updateForm(remittanceTransactionStatus);
    });
  }

  updateForm(remittanceTransactionStatus: IRemittanceTransactionStatus): void {
    this.editForm.patchValue({
      id: remittanceTransactionStatus.id,
      domaineId: remittanceTransactionStatus.domaineId,
      type: remittanceTransactionStatus.type,
      label: remittanceTransactionStatus.label,
      description: remittanceTransactionStatus.description
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const remittanceTransactionStatus = this.createFromForm();
    if (remittanceTransactionStatus.id !== undefined) {
      this.subscribeToSaveResponse(this.remittanceTransactionStatusService.update(remittanceTransactionStatus));
    } else {
      this.subscribeToSaveResponse(this.remittanceTransactionStatusService.create(remittanceTransactionStatus));
    }
  }

  private createFromForm(): IRemittanceTransactionStatus {
    return {
      ...new RemittanceTransactionStatus(),
      id: this.editForm.get(['id'])!.value,
      domaineId: this.editForm.get(['domaineId'])!.value,
      type: this.editForm.get(['type'])!.value,
      label: this.editForm.get(['label'])!.value,
      description: this.editForm.get(['description'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IRemittanceTransactionStatus>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
