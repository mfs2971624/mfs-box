import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IRemittanceTransactionStatus } from 'app/shared/model/remittance-transaction-status.model';
import { RemittanceTransactionStatusService } from './remittance-transaction-status.service';

@Component({
  templateUrl: './remittance-transaction-status-delete-dialog.component.html'
})
export class RemittanceTransactionStatusDeleteDialogComponent {
  remittanceTransactionStatus?: IRemittanceTransactionStatus;

  constructor(
    protected remittanceTransactionStatusService: RemittanceTransactionStatusService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.remittanceTransactionStatusService.delete(id).subscribe(() => {
      this.eventManager.broadcast('remittanceTransactionStatusListModification');
      this.activeModal.close();
    });
  }
}
