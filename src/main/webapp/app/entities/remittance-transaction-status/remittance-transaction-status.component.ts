import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IRemittanceTransactionStatus } from 'app/shared/model/remittance-transaction-status.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { RemittanceTransactionStatusService } from './remittance-transaction-status.service';
import { RemittanceTransactionStatusDeleteDialogComponent } from './remittance-transaction-status-delete-dialog.component';

@Component({
  selector: 'jhi-remittance-transaction-status',
  templateUrl: './remittance-transaction-status.component.html'
})
export class RemittanceTransactionStatusComponent implements OnInit, OnDestroy {
  remittanceTransactionStatuses?: IRemittanceTransactionStatus[];
  eventSubscriber?: Subscription;
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page!: number;
  predicate!: string;
  ascending!: boolean;
  ngbPaginationPage = 1;

  constructor(
    protected remittanceTransactionStatusService: RemittanceTransactionStatusService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadPage(page?: number): void {
    const pageToLoad: number = page || this.page;

    this.remittanceTransactionStatusService
      .query({
        page: pageToLoad - 1,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe(
        (res: HttpResponse<IRemittanceTransactionStatus[]>) => this.onSuccess(res.body, res.headers, pageToLoad),
        () => this.onError()
      );
  }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(data => {
      this.page = data.pagingParams.page;
      this.ascending = data.pagingParams.ascending;
      this.predicate = data.pagingParams.predicate;
      this.ngbPaginationPage = data.pagingParams.page;
      this.loadPage();
    });
    this.registerChangeInRemittanceTransactionStatuses();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IRemittanceTransactionStatus): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInRemittanceTransactionStatuses(): void {
    this.eventSubscriber = this.eventManager.subscribe('remittanceTransactionStatusListModification', () => this.loadPage());
  }

  delete(remittanceTransactionStatus: IRemittanceTransactionStatus): void {
    const modalRef = this.modalService.open(RemittanceTransactionStatusDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.remittanceTransactionStatus = remittanceTransactionStatus;
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected onSuccess(data: IRemittanceTransactionStatus[] | null, headers: HttpHeaders, page: number): void {
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.page = page;
    this.router.navigate(['/remittance-transaction-status'], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage,
        sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc')
      }
    });
    this.remittanceTransactionStatuses = data || [];
  }

  protected onError(): void {
    this.ngbPaginationPage = this.page;
  }
}
