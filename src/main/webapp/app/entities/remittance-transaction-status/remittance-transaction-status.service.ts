import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IRemittanceTransactionStatus } from 'app/shared/model/remittance-transaction-status.model';

type EntityResponseType = HttpResponse<IRemittanceTransactionStatus>;
type EntityArrayResponseType = HttpResponse<IRemittanceTransactionStatus[]>;

@Injectable({ providedIn: 'root' })
export class RemittanceTransactionStatusService {
  public resourceUrl = SERVER_API_URL + 'api/remittance-transaction-statuses';

  constructor(protected http: HttpClient) {}

  create(remittanceTransactionStatus: IRemittanceTransactionStatus): Observable<EntityResponseType> {
    return this.http.post<IRemittanceTransactionStatus>(this.resourceUrl, remittanceTransactionStatus, { observe: 'response' });
  }

  update(remittanceTransactionStatus: IRemittanceTransactionStatus): Observable<EntityResponseType> {
    return this.http.put<IRemittanceTransactionStatus>(this.resourceUrl, remittanceTransactionStatus, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IRemittanceTransactionStatus>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IRemittanceTransactionStatus[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
