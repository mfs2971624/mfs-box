import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MfsboxSharedModule } from 'app/shared/shared.module';
import { RemittanceTransactionStatusComponent } from './remittance-transaction-status.component';
import { RemittanceTransactionStatusDetailComponent } from './remittance-transaction-status-detail.component';
import { RemittanceTransactionStatusUpdateComponent } from './remittance-transaction-status-update.component';
import { RemittanceTransactionStatusDeleteDialogComponent } from './remittance-transaction-status-delete-dialog.component';
import { remittanceTransactionStatusRoute } from './remittance-transaction-status.route';

@NgModule({
  imports: [MfsboxSharedModule, RouterModule.forChild(remittanceTransactionStatusRoute)],
  declarations: [
    RemittanceTransactionStatusComponent,
    RemittanceTransactionStatusDetailComponent,
    RemittanceTransactionStatusUpdateComponent,
    RemittanceTransactionStatusDeleteDialogComponent
  ],
  entryComponents: [RemittanceTransactionStatusDeleteDialogComponent]
})
export class MfsboxRemittanceTransactionStatusModule {}
