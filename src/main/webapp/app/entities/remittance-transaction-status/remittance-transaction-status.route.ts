import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IRemittanceTransactionStatus, RemittanceTransactionStatus } from 'app/shared/model/remittance-transaction-status.model';
import { RemittanceTransactionStatusService } from './remittance-transaction-status.service';
import { RemittanceTransactionStatusComponent } from './remittance-transaction-status.component';
import { RemittanceTransactionStatusDetailComponent } from './remittance-transaction-status-detail.component';
import { RemittanceTransactionStatusUpdateComponent } from './remittance-transaction-status-update.component';

@Injectable({ providedIn: 'root' })
export class RemittanceTransactionStatusResolve implements Resolve<IRemittanceTransactionStatus> {
  constructor(private service: RemittanceTransactionStatusService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IRemittanceTransactionStatus> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((remittanceTransactionStatus: HttpResponse<RemittanceTransactionStatus>) => {
          if (remittanceTransactionStatus.body) {
            return of(remittanceTransactionStatus.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new RemittanceTransactionStatus());
  }
}

export const remittanceTransactionStatusRoute: Routes = [
  {
    path: '',
    component: RemittanceTransactionStatusComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'RemittanceTransactionStatuses'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: RemittanceTransactionStatusDetailComponent,
    resolve: {
      remittanceTransactionStatus: RemittanceTransactionStatusResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'RemittanceTransactionStatuses'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: RemittanceTransactionStatusUpdateComponent,
    resolve: {
      remittanceTransactionStatus: RemittanceTransactionStatusResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'RemittanceTransactionStatuses'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: RemittanceTransactionStatusUpdateComponent,
    resolve: {
      remittanceTransactionStatus: RemittanceTransactionStatusResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'RemittanceTransactionStatuses'
    },
    canActivate: [UserRouteAccessService]
  }
];
