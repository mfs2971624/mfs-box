import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IRemittanceTransactionStatus } from 'app/shared/model/remittance-transaction-status.model';

@Component({
  selector: 'jhi-remittance-transaction-status-detail',
  templateUrl: './remittance-transaction-status-detail.component.html'
})
export class RemittanceTransactionStatusDetailComponent implements OnInit {
  remittanceTransactionStatus: IRemittanceTransactionStatus | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(
      ({ remittanceTransactionStatus }) => (this.remittanceTransactionStatus = remittanceTransactionStatus)
    );
  }

  previousState(): void {
    window.history.back();
  }
}
