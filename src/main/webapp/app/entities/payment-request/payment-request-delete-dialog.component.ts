import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IPaymentRequest } from 'app/shared/model/payment-request.model';
import { PaymentRequestService } from './payment-request.service';

@Component({
  templateUrl: './payment-request-delete-dialog.component.html'
})
export class PaymentRequestDeleteDialogComponent {
  paymentRequest?: IPaymentRequest;

  constructor(
    protected paymentRequestService: PaymentRequestService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.paymentRequestService.delete(id).subscribe(() => {
      this.eventManager.broadcast('paymentRequestListModification');
      this.activeModal.close();
    });
  }
}
