import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IPaymentRequest, PaymentRequest } from 'app/shared/model/payment-request.model';
import { PaymentRequestService } from './payment-request.service';
import { IBulkDisbursement } from 'app/shared/model/bulk-disbursement.model';
import { BulkDisbursementService } from 'app/entities/bulk-disbursement/bulk-disbursement.service';

@Component({
  selector: 'jhi-payment-request-update',
  templateUrl: './payment-request-update.component.html'
})
export class PaymentRequestUpdateComponent implements OnInit {
  isSaving = false;
  bulkdisbursements: IBulkDisbursement[] = [];

  editForm = this.fb.group({
    id: [],
    msisdn: [null, [Validators.required]],
    amount: [null, [Validators.required]],
    identificationNo: [],
    firstName: [],
    lastName: [],
    comment: [],
    paymentState: [],
    paymentTxId: [],
    paymentMessage: [],
    bulkDisbursementId: []
  });

  constructor(
    protected paymentRequestService: PaymentRequestService,
    protected bulkDisbursementService: BulkDisbursementService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ paymentRequest }) => {
      this.updateForm(paymentRequest);

      this.bulkDisbursementService.query().subscribe((res: HttpResponse<IBulkDisbursement[]>) => (this.bulkdisbursements = res.body || []));
    });
  }

  updateForm(paymentRequest: IPaymentRequest): void {
    this.editForm.patchValue({
      id: paymentRequest.id,
      msisdn: paymentRequest.msisdn,
      amount: paymentRequest.amount,
      identificationNo: paymentRequest.identificationNo,
      firstName: paymentRequest.firstName,
      lastName: paymentRequest.lastName,
      comment: paymentRequest.comment,
      paymentState: paymentRequest.paymentState,
      paymentTxId: paymentRequest.paymentTxId,
      paymentMessage: paymentRequest.paymentMessage,
      bulkDisbursementId: paymentRequest.bulkDisbursementId
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const paymentRequest = this.createFromForm();
    if (paymentRequest.id !== undefined) {
      this.subscribeToSaveResponse(this.paymentRequestService.update(paymentRequest));
    } else {
      this.subscribeToSaveResponse(this.paymentRequestService.create(paymentRequest));
    }
  }

  private createFromForm(): IPaymentRequest {
    return {
      ...new PaymentRequest(),
      id: this.editForm.get(['id'])!.value,
      msisdn: this.editForm.get(['msisdn'])!.value,
      amount: this.editForm.get(['amount'])!.value,
      identificationNo: this.editForm.get(['identificationNo'])!.value,
      firstName: this.editForm.get(['firstName'])!.value,
      lastName: this.editForm.get(['lastName'])!.value,
      comment: this.editForm.get(['comment'])!.value,
      paymentState: this.editForm.get(['paymentState'])!.value,
      paymentTxId: this.editForm.get(['paymentTxId'])!.value,
      paymentMessage: this.editForm.get(['paymentMessage'])!.value,
      bulkDisbursementId: this.editForm.get(['bulkDisbursementId'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPaymentRequest>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IBulkDisbursement): any {
    return item.id;
  }
}
