import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IPaymentRequest, PaymentRequest } from 'app/shared/model/payment-request.model';
import { PaymentRequestService } from './payment-request.service';
import { PaymentRequestComponent } from './payment-request.component';
import { PaymentRequestDetailComponent } from './payment-request-detail.component';
import { PaymentRequestUpdateComponent } from './payment-request-update.component';
import { PaymentRequestListComponent } from 'app/entities/payment-request/payment-request-list.component';

@Injectable({ providedIn: 'root' })
export class PaymentRequestResolve implements Resolve<IPaymentRequest> {
  constructor(private service: PaymentRequestService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IPaymentRequest> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((paymentRequest: HttpResponse<PaymentRequest>) => {
          if (paymentRequest.body) {
            return of(paymentRequest.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new PaymentRequest());
  }
}

export const paymentRequestRoute: Routes = [
  {
    path: '',
    component: PaymentRequestComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'PaymentRequests'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: PaymentRequestDetailComponent,
    resolve: {
      paymentRequest: PaymentRequestResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'PaymentRequests'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'bulk/:id',
    component: PaymentRequestListComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'PaymentRequests bulk disbursement'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: PaymentRequestUpdateComponent,
    resolve: {
      paymentRequest: PaymentRequestResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'PaymentRequests'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: PaymentRequestUpdateComponent,
    resolve: {
      paymentRequest: PaymentRequestResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'PaymentRequests'
    },
    canActivate: [UserRouteAccessService]
  }
];
