import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MfsboxSharedModule } from 'app/shared/shared.module';
import { PaymentRequestComponent } from './payment-request.component';
import { PaymentRequestDetailComponent } from './payment-request-detail.component';
import { PaymentRequestUpdateComponent } from './payment-request-update.component';
import { PaymentRequestDeleteDialogComponent } from './payment-request-delete-dialog.component';
import { paymentRequestRoute } from './payment-request.route';
import { PaymentRequestListComponent } from 'app/entities/payment-request/payment-request-list.component';

@NgModule({
  imports: [MfsboxSharedModule, RouterModule.forChild(paymentRequestRoute)],
  declarations: [
    PaymentRequestComponent,
    PaymentRequestDetailComponent,
    PaymentRequestUpdateComponent,
    PaymentRequestDeleteDialogComponent,
    PaymentRequestListComponent
  ],
  entryComponents: [PaymentRequestDeleteDialogComponent]
})
export class MfsboxPaymentRequestModule {}
