import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IPaymentRequest } from 'app/shared/model/payment-request.model';

@Component({
  selector: 'jhi-payment-request-detail',
  templateUrl: './payment-request-detail.component.html'
})
export class PaymentRequestDetailComponent implements OnInit {
  paymentRequest: IPaymentRequest | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ paymentRequest }) => (this.paymentRequest = paymentRequest));
  }

  previousState(): void {
    window.history.back();
  }
}
