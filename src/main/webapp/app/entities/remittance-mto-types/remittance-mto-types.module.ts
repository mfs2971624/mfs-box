import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MfsboxSharedModule } from 'app/shared/shared.module';
import { RemittanceMtoTypesComponent } from './remittance-mto-types.component';
import { RemittanceMtoTypesDetailComponent } from './remittance-mto-types-detail.component';
import { RemittanceMtoTypesUpdateComponent } from './remittance-mto-types-update.component';
import { RemittanceMtoTypesDeleteDialogComponent } from './remittance-mto-types-delete-dialog.component';
import { remittanceMtoTypesRoute } from './remittance-mto-types.route';

@NgModule({
  imports: [MfsboxSharedModule, RouterModule.forChild(remittanceMtoTypesRoute)],
  declarations: [
    RemittanceMtoTypesComponent,
    RemittanceMtoTypesDetailComponent,
    RemittanceMtoTypesUpdateComponent,
    RemittanceMtoTypesDeleteDialogComponent
  ],
  entryComponents: [RemittanceMtoTypesDeleteDialogComponent]
})
export class MfsboxRemittanceMtoTypesModule {}
