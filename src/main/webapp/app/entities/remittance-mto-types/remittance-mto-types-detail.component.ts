import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IRemittanceMtoTypes } from 'app/shared/model/remittance-mto-types.model';

@Component({
  selector: 'jhi-remittance-mto-types-detail',
  templateUrl: './remittance-mto-types-detail.component.html'
})
export class RemittanceMtoTypesDetailComponent implements OnInit {
  remittanceMtoTypes: IRemittanceMtoTypes | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ remittanceMtoTypes }) => (this.remittanceMtoTypes = remittanceMtoTypes));
  }

  previousState(): void {
    window.history.back();
  }
}
