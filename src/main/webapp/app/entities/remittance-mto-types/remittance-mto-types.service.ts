import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IRemittanceMtoTypes } from 'app/shared/model/remittance-mto-types.model';

type EntityResponseType = HttpResponse<IRemittanceMtoTypes>;
type EntityArrayResponseType = HttpResponse<IRemittanceMtoTypes[]>;

@Injectable({ providedIn: 'root' })
export class RemittanceMtoTypesService {
  public resourceUrl = SERVER_API_URL + 'api/remittance-mto-types';

  constructor(protected http: HttpClient) {}

  create(remittanceMtoTypes: IRemittanceMtoTypes): Observable<EntityResponseType> {
    return this.http.post<IRemittanceMtoTypes>(this.resourceUrl, remittanceMtoTypes, { observe: 'response' });
  }

  update(remittanceMtoTypes: IRemittanceMtoTypes): Observable<EntityResponseType> {
    return this.http.put<IRemittanceMtoTypes>(this.resourceUrl, remittanceMtoTypes, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IRemittanceMtoTypes>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IRemittanceMtoTypes[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
