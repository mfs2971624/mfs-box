import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IRemittanceMtoTypes, RemittanceMtoTypes } from 'app/shared/model/remittance-mto-types.model';
import { RemittanceMtoTypesService } from './remittance-mto-types.service';
import { RemittanceMtoTypesComponent } from './remittance-mto-types.component';
import { RemittanceMtoTypesDetailComponent } from './remittance-mto-types-detail.component';
import { RemittanceMtoTypesUpdateComponent } from './remittance-mto-types-update.component';

@Injectable({ providedIn: 'root' })
export class RemittanceMtoTypesResolve implements Resolve<IRemittanceMtoTypes> {
  constructor(private service: RemittanceMtoTypesService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IRemittanceMtoTypes> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((remittanceMtoTypes: HttpResponse<RemittanceMtoTypes>) => {
          if (remittanceMtoTypes.body) {
            return of(remittanceMtoTypes.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new RemittanceMtoTypes());
  }
}

export const remittanceMtoTypesRoute: Routes = [
  {
    path: '',
    component: RemittanceMtoTypesComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'RemittanceMtoTypes'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: RemittanceMtoTypesDetailComponent,
    resolve: {
      remittanceMtoTypes: RemittanceMtoTypesResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'RemittanceMtoTypes'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: RemittanceMtoTypesUpdateComponent,
    resolve: {
      remittanceMtoTypes: RemittanceMtoTypesResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'RemittanceMtoTypes'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: RemittanceMtoTypesUpdateComponent,
    resolve: {
      remittanceMtoTypes: RemittanceMtoTypesResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'RemittanceMtoTypes'
    },
    canActivate: [UserRouteAccessService]
  }
];
