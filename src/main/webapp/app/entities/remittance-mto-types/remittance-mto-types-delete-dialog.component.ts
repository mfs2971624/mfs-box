import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IRemittanceMtoTypes } from 'app/shared/model/remittance-mto-types.model';
import { RemittanceMtoTypesService } from './remittance-mto-types.service';

@Component({
  templateUrl: './remittance-mto-types-delete-dialog.component.html'
})
export class RemittanceMtoTypesDeleteDialogComponent {
  remittanceMtoTypes?: IRemittanceMtoTypes;

  constructor(
    protected remittanceMtoTypesService: RemittanceMtoTypesService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.remittanceMtoTypesService.delete(id).subscribe(() => {
      this.eventManager.broadcast('remittanceMtoTypesListModification');
      this.activeModal.close();
    });
  }
}
