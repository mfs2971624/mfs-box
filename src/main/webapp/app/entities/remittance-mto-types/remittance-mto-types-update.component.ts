import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IRemittanceMtoTypes, RemittanceMtoTypes } from 'app/shared/model/remittance-mto-types.model';
import { RemittanceMtoTypesService } from './remittance-mto-types.service';

@Component({
  selector: 'jhi-remittance-mto-types-update',
  templateUrl: './remittance-mto-types-update.component.html'
})
export class RemittanceMtoTypesUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    domaineId: [null, [Validators.required]],
    type: [null, [Validators.required]],
    label: [null, [Validators.required]],
    description: []
  });

  constructor(
    protected remittanceMtoTypesService: RemittanceMtoTypesService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ remittanceMtoTypes }) => {
      this.updateForm(remittanceMtoTypes);
    });
  }

  updateForm(remittanceMtoTypes: IRemittanceMtoTypes): void {
    this.editForm.patchValue({
      id: remittanceMtoTypes.id,
      domaineId: remittanceMtoTypes.domaineId,
      type: remittanceMtoTypes.type,
      label: remittanceMtoTypes.label,
      description: remittanceMtoTypes.description
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const remittanceMtoTypes = this.createFromForm();
    if (remittanceMtoTypes.id !== undefined) {
      this.subscribeToSaveResponse(this.remittanceMtoTypesService.update(remittanceMtoTypes));
    } else {
      this.subscribeToSaveResponse(this.remittanceMtoTypesService.create(remittanceMtoTypes));
    }
  }

  private createFromForm(): IRemittanceMtoTypes {
    return {
      ...new RemittanceMtoTypes(),
      id: this.editForm.get(['id'])!.value,
      domaineId: this.editForm.get(['domaineId'])!.value,
      type: this.editForm.get(['type'])!.value,
      label: this.editForm.get(['label'])!.value,
      description: this.editForm.get(['description'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IRemittanceMtoTypes>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
