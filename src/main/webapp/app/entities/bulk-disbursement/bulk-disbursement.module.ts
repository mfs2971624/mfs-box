import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MfsboxSharedModule } from 'app/shared/shared.module';
import { BulkDisbursementComponent } from './bulk-disbursement.component';
import { BulkDisbursementDetailComponent } from './bulk-disbursement-detail.component';
import { BulkDisbursementUpdateComponent } from './bulk-disbursement-update.component';
import { BulkDisbursementDeleteDialogComponent } from './bulk-disbursement-delete-dialog.component';
import { bulkDisbursementRoute } from './bulk-disbursement.route';

@NgModule({
  imports: [MfsboxSharedModule, RouterModule.forChild(bulkDisbursementRoute)],
  declarations: [
    BulkDisbursementComponent,
    BulkDisbursementDetailComponent,
    BulkDisbursementUpdateComponent,
    BulkDisbursementDeleteDialogComponent
  ],
  entryComponents: [BulkDisbursementDeleteDialogComponent]
})
export class MfsboxBulkDisbursementModule {}
