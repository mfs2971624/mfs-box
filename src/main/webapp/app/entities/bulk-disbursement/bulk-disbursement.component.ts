import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IBulkDisbursement } from 'app/shared/model/bulk-disbursement.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { BulkDisbursementService } from './bulk-disbursement.service';
import { BulkDisbursementDeleteDialogComponent } from './bulk-disbursement-delete-dialog.component';
import Swal from 'sweetalert2';

@Component({
  selector: 'jhi-bulk-disbursement',
  templateUrl: './bulk-disbursement.component.html'
})
export class BulkDisbursementComponent implements OnInit, OnDestroy {
  bulkDisbursements?: IBulkDisbursement[];
  eventSubscriber?: Subscription;
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page!: number;
  predicate!: string;
  ascending!: boolean;
  ngbPaginationPage = 1;

  constructor(
    protected bulkDisbursementService: BulkDisbursementService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadPage(page?: number): void {
    const pageToLoad: number = page || this.page;

    this.bulkDisbursementService
      .query({
        page: pageToLoad - 1,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe(
        (res: HttpResponse<IBulkDisbursement[]>) => this.onSuccess(res.body, res.headers, pageToLoad),
        () => this.onError()
      );
  }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(data => {
      this.page = data.pagingParams.page;
      this.ascending = data.pagingParams.ascending;
      this.predicate = data.pagingParams.predicate;
      this.ngbPaginationPage = data.pagingParams.page;
      this.loadPage();
    });
    this.registerChangeInBulkDisbursements();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IBulkDisbursement): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInBulkDisbursements(): void {
    this.eventSubscriber = this.eventManager.subscribe('bulkDisbursementListModification', () => this.loadPage());
  }

  delete(bulkDisbursement: IBulkDisbursement): void {
    const modalRef = this.modalService.open(BulkDisbursementDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.bulkDisbursement = bulkDisbursement;
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected onSuccess(data: IBulkDisbursement[] | null, headers: HttpHeaders, page: number): void {
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.page = page;
    this.router.navigate(['/bulk-disbursement'], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage,
        sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc')
      }
    });
    this.bulkDisbursements = data || [];
  }

  protected onError(): void {
    this.ngbPaginationPage = this.page;
  }

  askPin(id: string): void {
    Swal.fire({
      title: 'Renseigner le PIN pour paiement',
      input: 'password',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Payer'
    }).then(result => {
      if (result.value) {
      // eslint-disable-next-line no-unused-vars
        this.bulkDisbursementService.payment(id, result.value).subscribe(res => {
          this.loadPage();
        });
      }
    });
  }

  report(bulkId: any): void {
    this.bulkDisbursementService.report(bulkId).subscribe(res => {
      // eslint-disable-next-line no-console
      const blob = new Blob([res], { type: 'text/csv' });
      const data = window.URL.createObjectURL(blob);
      const link = document.createElement('a');
      link.href = data;
      link.download = 'rapport.csv';
      link.click();
    });
  }
}
