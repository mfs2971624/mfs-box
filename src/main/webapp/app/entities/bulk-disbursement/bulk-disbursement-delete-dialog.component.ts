import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IBulkDisbursement } from 'app/shared/model/bulk-disbursement.model';
import { BulkDisbursementService } from './bulk-disbursement.service';

@Component({
  templateUrl: './bulk-disbursement-delete-dialog.component.html'
})
export class BulkDisbursementDeleteDialogComponent {
  bulkDisbursement?: IBulkDisbursement;

  constructor(
    protected bulkDisbursementService: BulkDisbursementService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.bulkDisbursementService.delete(id).subscribe(() => {
      this.eventManager.broadcast('bulkDisbursementListModification');
      this.activeModal.close();
    });
  }
}
