/* eslint no-use-before-define: 0 */ // --> OFF
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IBulkDisbursement } from 'app/shared/model/bulk-disbursement.model';
import Swal from 'sweetalert2';
import { BulkDisbursementService } from 'app/entities/bulk-disbursement/bulk-disbursement.service';
import { ProcessingStatus } from 'app/shared/model/enumerations/processing-status.model';

@Component({
  selector: 'jhi-bulk-disbursement-detail',
  templateUrl: './bulk-disbursement-detail.component.html'
})
export class BulkDisbursementDetailComponent implements OnInit {
  bulkDisbursement: IBulkDisbursement | null = null;
  bulkDisbursementProcessingStatus = ProcessingStatus.INITIATED;

  constructor(protected bulkDisbursementService: BulkDisbursementService, protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ bulkDisbursement }) => {
      this.bulkDisbursement = bulkDisbursement;
    });
  }

  previousState(): void {
    window.history.back();
  }

  doPayment(id: any): void {
    Swal.fire({
      title: 'Renseigner le PIN pour paiement',
      input: 'password',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Payer'
    }).then(result => {
      if (result.value) {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        this.bulkDisbursementService.payment('' + id, result.value).subscribe(res => {
          this.previousState();
        });
      }
    });
  }
}
