import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IBulkDisbursement } from 'app/shared/model/bulk-disbursement.model';

type EntityResponseType = HttpResponse<IBulkDisbursement>;
type EntityArrayResponseType = HttpResponse<IBulkDisbursement[]>;

@Injectable({ providedIn: 'root' })
export class BulkDisbursementService {
  public resourceUrl = SERVER_API_URL + 'api/bulk-disbursements';

  constructor(protected http: HttpClient) {}

  create(bulkDisbursement: IBulkDisbursement, bulkFile: any): Observable<EntityResponseType> {
    // eslint-disable-next-line no-console
    console.log(bulkFile);
    const formData = new FormData();
    formData.append('bulkFile', bulkFile);
    formData.append('source', bulkDisbursement.msisdnSource as string);
    formData.append('type', bulkDisbursement.type as string);
    formData.append('description', bulkDisbursement.description as string);
    return this.http.post<IBulkDisbursement>(this.resourceUrl, formData, { observe: 'response' });
  }

  update(bulkDisbursement: IBulkDisbursement): Observable<EntityResponseType> {
    return this.http.put<IBulkDisbursement>(this.resourceUrl, bulkDisbursement, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IBulkDisbursement>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IBulkDisbursement[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  payment(id: string, value: string): Observable<EntityResponseType> {
    return this.http
      .post(`${this.resourceUrl}/${id}/payment`, { pin: value }, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.uploadStart = res.body.uploadStart ? moment(res.body.uploadStart) : undefined;
      res.body.uploadEnd = res.body.uploadEnd ? moment(res.body.uploadEnd) : undefined;
      res.body.paymentStart = res.body.paymentStart ? moment(res.body.paymentStart) : undefined;
      res.body.paymentEnd = res.body.paymentEnd ? moment(res.body.paymentEnd) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((bulkDisbursement: IBulkDisbursement) => {
        bulkDisbursement.uploadStart = bulkDisbursement.uploadStart ? moment(bulkDisbursement.uploadStart) : undefined;
        bulkDisbursement.uploadEnd = bulkDisbursement.uploadEnd ? moment(bulkDisbursement.uploadEnd) : undefined;
        bulkDisbursement.paymentStart = bulkDisbursement.paymentStart ? moment(bulkDisbursement.paymentStart) : undefined;
        bulkDisbursement.paymentEnd = bulkDisbursement.paymentEnd ? moment(bulkDisbursement.paymentEnd) : undefined;
      });
    }
    return res;
  }

  report(bulkId: any): Observable<Blob> {
    return this.http.get(`${this.resourceUrl}/${bulkId}/report`, { responseType: 'blob' });
  }
}
