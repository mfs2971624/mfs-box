import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IBulkDisbursement, BulkDisbursement } from 'app/shared/model/bulk-disbursement.model';
import { BulkDisbursementService } from './bulk-disbursement.service';
import { BulkDisbursementComponent } from './bulk-disbursement.component';
import { BulkDisbursementDetailComponent } from './bulk-disbursement-detail.component';
import { BulkDisbursementUpdateComponent } from './bulk-disbursement-update.component';

@Injectable({ providedIn: 'root' })
export class BulkDisbursementResolve implements Resolve<IBulkDisbursement> {
  constructor(private service: BulkDisbursementService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IBulkDisbursement> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((bulkDisbursement: HttpResponse<BulkDisbursement>) => {
          if (bulkDisbursement.body) {
            return of(bulkDisbursement.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new BulkDisbursement());
  }
}

export const bulkDisbursementRoute: Routes = [
  {
    path: '',
    component: BulkDisbursementComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,desc',
      pageTitle: 'BulkDisbursements'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: BulkDisbursementDetailComponent,
    resolve: {
      bulkDisbursement: BulkDisbursementResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'BulkDisbursements'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: BulkDisbursementUpdateComponent,
    resolve: {
      bulkDisbursement: BulkDisbursementResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'BulkDisbursements'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: BulkDisbursementUpdateComponent,
    resolve: {
      bulkDisbursement: BulkDisbursementResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'BulkDisbursements'
    },
    canActivate: [UserRouteAccessService]
  }
];
