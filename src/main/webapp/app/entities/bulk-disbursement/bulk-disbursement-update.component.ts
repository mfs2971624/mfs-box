import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import Swal from 'sweetalert2';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IBulkDisbursement, BulkDisbursement } from 'app/shared/model/bulk-disbursement.model';
import { BulkDisbursementService } from './bulk-disbursement.service';

@Component({
  selector: 'jhi-bulk-disbursement-update',
  templateUrl: './bulk-disbursement-update.component.html'
})
export class BulkDisbursementUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    bulkId: [],
    msisdnSource: [null, [Validators.required]],
    type: [null, [Validators.required]],
    description: [],
    processingStatus: []
  });
  bulkFile: any;

  constructor(
    protected bulkDisbursementService: BulkDisbursementService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ bulkDisbursement }) => {
      this.updateForm(bulkDisbursement);
    });
  }

  updateForm(bulkDisbursement: IBulkDisbursement): void {
    this.editForm.patchValue({
      id: bulkDisbursement.id,
      bulkId: bulkDisbursement.bulkId,
      msisdnSource: bulkDisbursement.msisdnSource,
      type: bulkDisbursement.type,
      description: bulkDisbursement.description,
      processingStatus: bulkDisbursement.processingStatus
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    Swal.showLoading();
    this.isSaving = true;
    if (!this.bulkFile) {
      Swal.fire({
        title: 'Error!',
        text: 'Merci de charger un fichier au bon format',
        icon: 'error',
        footer: '<a href="../../../content/samples/bulk_disbursement.csv">Exemple de fichier bulk</a>'
      });
      this.isSaving = false;
      Swal.hideLoading();
      return;
    }
    const bulkDisbursement = this.createFromForm();
    if (bulkDisbursement.id !== undefined) {
      this.subscribeToSaveResponse(this.bulkDisbursementService.update(bulkDisbursement));
    } else {
      this.subscribeToSaveResponse(this.bulkDisbursementService.create(bulkDisbursement, this.bulkFile));
    }
  }

  private createFromForm(): IBulkDisbursement {
    return {
      ...new BulkDisbursement(),
      id: this.editForm.get(['id'])!.value,
      bulkId: this.editForm.get(['bulkId'])!.value,
      msisdnSource: this.editForm.get(['msisdnSource'])!.value,
      type: this.editForm.get(['type'])!.value,
      description: this.editForm.get(['description'])!.value,
      processingStatus: this.editForm.get(['processingStatus'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBulkDisbursement>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
    Swal.hideLoading();
  }

  protected onSaveError(): void {
    this.isSaving = false;
    Swal.hideLoading();
  }

  onFileChange(event: any): void {
    this.bulkFile = event.target.files.length > 0 ? event.target.files.item(0) : null;
  }
}
